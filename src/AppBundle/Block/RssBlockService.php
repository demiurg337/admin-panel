<?php

namespace AppBundle\Block;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\CoreBundle\Form\Type\ImmutableArrayType;
use Sonata\CoreBundle\Model\Metadata;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Sonata\BlockBundle\Block\Service\AbstractAdminBlockService;

use AppBundle\Helper\FileGenerator;
/**
 * @author Thomas Rabaix <thomas.rabaix@sonata-project.org>
 */
class RssBlockService extends AbstractAdminBlockService
{
    private $doctrine;
    /**
     * @param string             $name
     * @param EngineInterface    $templating
     * @param ContainerInterface $container
     */
    public function __construct($name, $templating, $doctrine)
    {
        parent::__construct($name, $templating);
        $this->doctrine = $doctrine;
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'url' => false,
            'title' => null,
            'translation_domain' => null,
            'icon' => 'glyphicon glyphicon-stats',
            'class' => null,
            'template' => '@AppBundle/Block/views/block_core_rss.html.twig',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        $formMapper->add('settings', ImmutableArrayType::class, [
            'keys' => [
                ['url', UrlType::class, [
                    'required' => false,
                    'label' => 'form.label_url',
                ]],
                ['title', TextType::class, [
                    'label' => 'form.label_title',
                    'required' => false,
                ]],
                ['translation_domain', TextType::class, [
                    'label' => 'form.label_translation_domain',
                    'required' => false,
                ]],
                ['icon', TextType::class, [
                    'label' => 'form.label_icon',
                    'required' => false,
                ]],
                ['class', TextType::class, [
                    'label' => 'form.label_class',
                    'required' => false,
                ]],
            ],
            'translation_domain' => 'SonataBlockBundle',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        $errorElement
            ->with('settings[url]')
                ->assertNotNull([])
                ->assertNotBlank()
            ->end()
            ->with('settings[title]')
                ->assertNotNull([])
                ->assertNotBlank()
                ->assertLength(['max' => 50])
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $from = new \DateTime();
        $from->modify('-1 day');
        $from = new \DateTime($from->format('Y-m-d') . ' 00:00:00');

        $to = new \DateTime();
        $to = new \DateTime($to->format('Y-m-d') . ' 23:59:59');

        $generator = new FileGenerator($this->doctrine);
        $info = $generator->getInfoForStatisticByStores($from, $to);
        usort($info, function($a, $b) {
            return $a->quantityOfOrders > $b->quantityOfOrders ? -1 : 1;
        }); 

        $total = [
            'quantityOfOrders' => 0,
            'price' => 0,
        ];
        foreach ($info as $item) {
            $total['quantityOfOrders'] += (int) $item->quantityOfOrders;
            $total['price'] += (float) $item->price;
        }
        /////////////////////////////////////////
        /////////////////////////////////////////

        $infoOfProducts = $generator->getinfostatisticbyproducts($from, $to);
        $infoOfProducts = array_slice($infoOfProducts, 0, 5);

        //var_dump($infoOfProducts); die;
        // merge settings
        $settings = $blockContext->getSettings();


        return $this->renderResponse($blockContext->getTemplate(), [
            'info' => $info,
            'infoOfProducts' => $infoOfProducts,
            'total' => $total,
            'block' => $blockContext->getBlock(),
            'settings' => $settings,
        ], $response);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockMetadata($code = null)
    {
        return new Metadata($this->getName(), (null !== $code ? $code : $this->getName()), false, 'SonataBlockBundle', [
            'class' => 'fa fa-rss-square',
        ]);
    }
}
