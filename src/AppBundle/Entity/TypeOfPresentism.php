<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
 * @ORM\Table(name="type_of_presentism")
 */
class TypeOfPresentism
{
    /**
    * @ORM\Column(name="type_of_presentism_id",type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $typeOfPresentismId;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * Get fileTypeId
     *
     * @return \int
     */
    public function getTypeOfPresentismId()
    {
        return $this->typeOfPresentismId;
    }

    /**
     * Set name
     *
     * @param string $taxTicket
     *
     * @return Seller
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
