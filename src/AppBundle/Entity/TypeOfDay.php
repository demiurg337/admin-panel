<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
 * @ORM\Table(name="type_of_day")
 */
class TypeOfDay
{
    /**
     *
     * @ORM\Column(name="type_of_day_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $typeOfDayId;


     /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * Get typeOfDayId
     *
     * @return \int
     */
    public function getTypeOfDayId()
    {
        return $this->typeOfDayId;
    }

    /**
     * Set typeOfDayId
     *
     * @param string $externalStoreId
     *
     * @return Seller
     */
    public function setTypeOfDayId($typeOfDayId)
    {
        $this->externalStoreId = $typeOfDayId;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * set name
     *
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}

