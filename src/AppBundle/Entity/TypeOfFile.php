<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
 * @ORM\Table(name="types_of_file")
 */
class TypeOfFile
{
    /**
    * @ORM\Column(name="file_type_id",type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $fileTypeId;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * Get fileTypeId
     *
     * @return \int
     */
    public function getFileTypeId()
    {
        return $this->storeId;
    }

    /**
     * Set name
     *
     * @param string $taxTicket
     *
     * @return Seller
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
