<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
 * @ORM\Table(name="store_additional_ids")
 */
class StoreCode
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Store")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="store_id")
     */
    private $store;


     /**
     * @var string
     *
     * @ORM\Column(name="store_additional_id", type="string", length=255)
     * @ORM\Id
     */
    private $additionalId;

    /**
     * Set store     
     *
     * @param Store $store
     *
     * @return StoreOrder
     */
    public function setStore($store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }    
    
    /**
     * Set additionalId
     *
     * @param string $additionalId
     *
     * @return Seller
     */
    public function setAdditionalId($additionalId)
    {
        $this->additionalId = $additionalId;

        return $this;
    }

    /**
     * Get externalStoreId
     *
     * @return string
     */
    public function getAdditionalId()
    {
        return $this->additionalId;
    }
}

