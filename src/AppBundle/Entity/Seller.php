<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
 * @ORM\Table(name="seller")
 */
class Seller
{
    /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $seller_id;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Set sellerId
     *
     * @param \int $sellerId
     *
     * @return Seller
     */
    public function setSellerId($sellerId)
    {
        $this->seller_id = $sellerId;

        return $this;
    }

    /**
     * Get sellerId
     *
     * @return \int
     */
    public function getSellerId()
    {
        return $this->seller_id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Seller
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

