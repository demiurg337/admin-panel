<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
 * @ORM\Table(name="uploading_of_files_log")
 */
class FileLog
{
    /**
    * @ORM\Column(name="log_id",type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $logId;

     /**
     * @var timestamp
     *
     * @ORM\Column(name="time_of_starting_saving", type="datetime")
     */
    private $timeOfStarting;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypeOfFile")
     * @ORM\JoinColumn(name="file_type_id", referencedColumnName="file_type_id")
     */
    private $type;

     /**
     * @var timestamp
     *
     * @ORM\Column(name="time_of_finshing_saving", type="datetime")
     */
    private $timeOfSaving;

    /**
     * @var string
     *
     *  @ORM\Column(name="is_all_ok", type="boolean", nullable=false)
     */
    private $isAllOk;



    /**
     * Get logId
     *
     * @return \int
     */
    public function getLogId()
    {
        return $this->logId;
    }

    /**
     * Set timeOfStarting
     *
     * @param string $name
     *
     * @return Seller
     */
    public function setTimeOfStarting($timeOfStarting)
    {
        $this->timeOfStarting = $timeOfStarting;

        return $this;
    }

    /**
     * Get timeOfStarting
     *
     * @return string
     */
    public function getTimeOfStarting()
    {
        return $this->timeOfStarting;
    }

    /**
     * Set isAllOk
     *
     * @param string $isAllOk
     *
     * @return int
     */
    public function setIsAllOk($isAllOk)
    {
        $this->isAllOk = $isAllOk;

        return $this;
    }

    /**
     * Get isAllOk
     *
     * @return string
     */
    public function getIsAllOk()
    {
        return $this->isAllOk;
    }

    /**
     * Set timeOfSaving
     *
     * @param string $name
     *
     * @return Seller
     */
    public function setTimeOfSaving($timeOfSaving)
    {
        $this->timeOfSaving = $timeOfSaving;

        return $this;
    }

    /**
     * Get timeOfSaving
     *
     * @return string
     */
    public function getTimeOfSaving()
    {
        return $this->timeOfSaving;
    }

    /**
     * Set type     
     *
     * @param Tyoe $tyoe
     *
     * @return StoreOrder
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return Store
     */
    public function getType()
    {
        return $this->type;
    }
}

