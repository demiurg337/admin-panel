<?php
// src/AppBundle/Entity/Product.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="store_order_product")
*/
class StoreOrderProduct
{
    /**
     *
     * @ORM\Column(name="store_order_product_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $storeOrderProductId;

    /**
    * @ORM\Column(name="articul", type="string", length=255)
    */
    private $articul;

     /**
     * @var timestamp
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
    * @ORM\Column(name="code", type="string", length=255)
    */
    private $code;

    /**
    * @ORM\Column(name="color", type="string", length=255)
    */
    private $color;

    /**
    * @ORM\Column(name="size", type="string", length=255)
    */
    private $size;


    /**
     * @ORM\Column(name="price", type="decimal", precision=30, scale=2)
     */
    protected $price;

    /**
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\StoreOrder", inversedBy="store_order_product")
     * @ORM\JoinColumn(name="store_order_id", referencedColumnName="store_order_id", nullable=true)
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Store")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="store_id")
     */
    private $store;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Seller")
     * @ORM\JoinColumn(name="seller_id", referencedColumnName="seller_id")
     */
    private $seller;

    /**
     * Get id
     *
     * @return integer
     */
    public function getStoreOrderProductId()
    {
        return $this->storeOrderProductId;
    }

    /**
     * Set articul
     *
     * @param string $email
     *
     * @return User
     */
    public function setArticul($articul)
    {
        $this->articul = $articul;

        return $this;
    }

    /**
     * Get articul
     *
     * @return string
     */
    public function getArticul()
    {
        return $this->articul;
    }

    /**
     * Set color
     *
     * @param string $email
     *
     * @return User
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set size
     *
     * @param string $email
     *
     * @return User
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set size
     *
     * @param string $code
     *
     * @return StoreOrderProduct
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set price
     *
     * @param decimal $price
     *
     * @return decimal
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return decimal
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set quantity
     *
     * @param decimal $quantity
     *
     * @return decimal
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return decimal
     */
    public function getQuantity()
    {
        return $this->quantity;
    }


    /**
     * Set order
     *
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return decimal
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set store     
     *
     * @param Store $store
     *
     * @return StoreOrder
     */
    public function setStore($store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Get date
     *
     * @return \timestamp
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get date
     *
     * @return \timestamp
     */
    public function setDate($date)
    {
        $this->date = $date;
    }



    /**
     * Set seller     
     *
     * @param Seller $store
     *
     * @return StoreOrder
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * Get seller
     *
     * @return Store
     */
    public function getSeller()
    {
        return $this->seller;
    }
}
