<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
 * @ORM\Table(name="store")
 */
class Store
{
    /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $storeId;


     /**
     * @var string
     *
     * @ORM\Column(name="external_store_id", type="string", length=255)
     */
    private $externalStoreId;

    /**
     * Get storeId
     *
     * @return \int
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

    /**
     * Set externalStoreId
     *
     * @param string $externalStoreId
     *
     * @return Seller
     */
    public function setExternalStoreId($externalStoreId)
    {
        $this->externalStoreId = $externalStoreId;

        return $this;
    }

    /**
     * Get externalStoreId
     *
     * @return string
     */
    public function getExternalStoreId()
    {
        return $this->externalStoreId;
    }
}

