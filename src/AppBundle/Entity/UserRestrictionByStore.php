<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
/**
* @ORM\Entity
 * @ORM\Table(name="user_restriction_by_store")
 */
class UserRestrictionByStore
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\Id
     * @ORM\JoinColumn(name="app_user_id", referencedColumnName="app_user_id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Store")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="store_id")
     */
    private $store;

     /**
     * Set store     
     *
     * @param Store $store
     *
     * @return StoreOrder
     */
    public function setStore($store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

     /**
     * Set user     
     *
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     */
    public function getUser()
    {
        return $this->user;
    }
}

