<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
 * @ORM\Table(name="categories")
 */
class Category
{
    /**
    * @ORM\Column(type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $categoryId;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

     
    /**
     * Get storeId
     *
     * @return \int
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Set taxTicket
     *
     * @param string $taxTicket
     *
     * @return Seller
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get taxTicket
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
