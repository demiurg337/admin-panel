<?php
// src/AppBundle/Entity/Product.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
* @ORM\Entity
* @ORM\Table(name="time_of_seller")
* @ORM\Entity(repositoryClass="AppBundle\Repository\TimeOfSellerRepository")
*/
class TimeOfSeller
{
    /**
     *
     * @ORM\Column(name="seller_time_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $sellerTimeId;

    /**
     *
     * @ORM\Column(name="seller_id", type="integer")
     */
    private $sellerId;

     /**
     * @var timestamp
     *
     * @ORM\Column(name="start_early_shift", type="datetime")
     */
    private $startOfEarlyShift;

     /**
     * @var timestamp
     *
     * @ORM\Column(name="end_early_shift", type="datetime")
     */
    private $endOfEarlyShift;

     /**
     * @var timestamp
     *
     * @ORM\Column(name="start_late_shift", type="datetime")
     */
    private $startOfLateShift;

     /**
     * @var timestamp
     *
     * @ORM\Column(name="end_late_shift", type="datetime")
     */
    private $endOfLateShift;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Seller")
     * @ORM\JoinColumn(name="seller_id", referencedColumnName="seller_id")
     */
    private $seller;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypeOfDay")
     * @ORM\JoinColumn(name="type_of_day_id", referencedColumnName="type_of_day_id")
     */
    private $typeOfDay;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypeOfPresentism")
     * @ORM\JoinColumn(name="type_of_presentism_id", referencedColumnName="type_of_presentism_id")
     */
    private $typeOfPresentism;

    /**
     *
     * @ORM\Column(name="type_of_presentism_id", type="integer")
     */
    private $typeOfPresentismId;

    /**
     * @var string
     *
     *  @ORM\Column(name="is_with_certificate", type="boolean", nullable=false)
     */
    private $isWithSertificate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getSellerTimeId()
    {
        return $this->sellerTimeId;
    }

    /**
     * Set time
     *
     * @param string $time
     *
     * @return User
     */
    public function setStartOfEarlyShift($time)
    {
        $this->startOfEarlyShift = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getStartOfEarlyShift()
    {
        return $this->startOfEarlyShift;
    }

    /**
     * Set time
     *
     * @param string $time
     *
     * @return User
     */
    public function setEndOfEarlyShift($time)
    {
        $this->endOfEarlyShift = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getEndOfEarlyShift()
    {
        return $this->endOfEarlyShift;
    }

     /*
     * Set time
     *
     * @param string $time
     *
     * @return User
     */
    public function setStartOfLateShift($time)
    {
        $this->startOfLateShift = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getStartOfLateShift()
    {
        return $this->startOfLateShift;
    }

     /*
     * Set time
     *
     * @param string $time
     *
     * @return User
     */
    public function setEndOfLateShift($time)
    {
        $this->endOfLateShift = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getEndOfLateShift()
    {
        return $this->endOfLateShift;
    }

    /**
     * Set seller     
     *
     * @param Seller $store
     *
     * @return StoreOrder
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;

        return $this;
    }

    /**
     * Get seller
     *
     * @return Store
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * Set type     
     *
     * @param Seller $store
     *
     * @return StoreOrder
     */
    public function setTypeOfDay($type)
    {
        $this->typeOfDay = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return Store
     */
    public function getTypeOfDay()
    {
        return $this->typeOfDay;
    }
    
    /**
     * Set type     
     *
     * @param Seller $store
     *
     * @return StoreOrder
     */
    public function setTypeOfPresentism($type)
    {
        $this->typeOfPresentism = $type;

        return $this;
    }

    public function setTypeOfPresentismId($typeId)
    {
        $this->typeOfPresentismId = $typeId;

        return $this;
    }

    public function getTypeOfPresentismId()
    {
        return $this->typeOfPresentismId;
    }

    /**
     * Get type
     *
     * @return Store
     */
    public function getTypeOfPresentism()
    {
        return $this->typeOfPresentism;
    }

    /**
     * Set isWithSertificate
     *
     * @param string $isWithSertificate
     *
     * @return int
     */
    public function setIsWithSertificate($isWithSertificate)
    {
        $this->isWithSertificate = $isWithSertificate;

        return $this;
    }

    /**
     * Set isWithSertificate
     *
     * @return string
     */
    public function getIsWithSertificate()
    {
        return $this->isWithSertificate;
    }
}
