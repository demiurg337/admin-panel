<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Seller as Seller;
use AppBundle\Entity\Store as Store;

/**
 * StoreOrder
 *
* @ORM\Entity
 * @ORM\Table(name="store_order")
 */
class StoreOrder
{
    /**
     *
     * @ORM\Column(name="store_order_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $storeOrderId;

    /**
    * @ORM\Column(name="store_order_external_id", type="string", length=255)
    */
    private $storeOrderExternalId;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Store", inversedBy="store_order")
     * @ORM\JoinColumn(name="store_id", referencedColumnName="store_id", nullable=true)
     */
    private $store;

    /**
     * @var timestamp
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get storeOrderExternalId
     *
     * @return \string
     */
    public function getStoreOrderExternalId()
    {
        return $this->storeOrderExternalId;
    }

    /**
     * Get storeOrderExternalId
     *
     * @return \string
     */
    public function setStoreOrderExternalId($storeOrderExternalId)
    {
        $this->storeOrderExternalId = $storeOrderExternalId;

        return $this;
    }

    /**
     * Set storeOrderId
     *
     * @param \int $storeOrderId
     *
     * @return StoreOrder
     */
    public function setStoreOrderId($storeOrderId)
    {
        $this->storeOrderId = $storeOrderId;

        return $this;
    }

    /**
     * Get storeOrderId
     *
     * @return \int
     */
    public function getStoreOrderId()
    {
        return $this->storeOrderId;
    }


    /**
     * Set storeId
     *
     * @param Store $store
     *
     * @return StoreOrder
     */
    public function setStore(Store $store)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Get store
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set date
     *
     * @param \timestamp $date
     *
     * @return StoreOrder
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \timestamp
     */
    public function getDate()
    {
        return $this->date;
    }
}

