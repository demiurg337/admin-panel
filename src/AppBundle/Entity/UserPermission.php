<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
 * @ORM\Table(name="user_permissions")
 */
class UserPermission
{
    /**
    * @ORM\Column(type="integer", name="user_permission_id")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $userPermissionId;


     /**
     * @var string
     *
     * @ORM\Column(name="permission_key", type="string", length=500)
     */
    private $permissionKey;

     /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500)
     */
    private $description;

    /**
     * Get storeId
     *
     * @return \int
     */
    public function getUserPermissionId()
    {
        return $this->userPermissionId;
    }

    /**
     * Set permissionKey
     *
     * @param string $permissionKey
     *
     * @return Seller
     */
    public function setPremissionKey($permissionKey)
    {
        $this->permissionKey = $permissionKey;

        return $this;
    }

    /**
     * Get permissionKey
     *
     * @return string
     */
    public function getPremissionKey()
    {
        return $this->permissionKey;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Seller
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get permissionKey
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

}

