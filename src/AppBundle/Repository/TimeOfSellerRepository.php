<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class TimeOfSellerRepository extends EntityRepository
{
    private $presentStatuses = [
        'PRESENTE' => 1,
        'DOMINIGO' => 2,
        'LEGADA_TARDE' => 3,
        'AUSENCIA_JUSITIFICADA' => 4,
        'FALTA_JUSITIFICADA' => 5,
        'TRAMITA_OBLIGITORIO' => 6,
        'VACOCIONES' => 6,
    ];

    public function getTimeOfSellers(\DateTime $from, \DateTime $to)
    {
        $records = $this->getEntityManager()
           ->createQuery(
            'SELECT 
                p 
            FROM 
                AppBundle:TimeOfSeller p 
            WHERE  
                p.endOfEarlyShift <= :to 
                AND 
                p.endOfEarlyShift >= :from ORDER BY p.sellerTimeId ASC')
           ->setParameter('from', $from)
           ->setParameter('to', $to)
           ->getResult();

        return $records;
    }

    public function calculateSalaryInfoByTimeOfSeller($timesOfSellers, $typesOfPresentism)
    {
        $statistic = [];
        $typesOfPresentismIds = $this->presentStatuses;

        $emptyValues = [];
        foreach ($typesOfPresentism as $type) {
            $emptyValues[(int) $type->getTypeOfPresentismId()] = [
                'name' => $type->getName(),
                'quantity' => 0,
            ];
        }

        foreach ($timesOfSellers as $time) {
            $typeOfPresentismId = (int) $time->getTypeOfPresentismId();
            $seller =  $time->getSeller();
            $sellerId = $seller->getSellerId();
            
            if (!isset($statistic[$sellerId])) {
                $statistic[$sellerId] = [
                    'sellerId' => $sellerId,
                    'sellerName' => $seller->getName(),
                    'price' => 0,
                    'statisticByStatuses' => $emptyValues
                ];
            }
            /*
                    'PRESENTE' => 1,
        'DOMINIGO' => 2,
        'LEGADA_TARDE' => 3,
        'AUSENCIA_JUSITIFICADA' => 4,
        'FALTA_JUSITIFICADA' => 5,
        'TRAMITA_OBLIGITORIO' => 6,
        'VACOCIONES' => 6,
            */
            if (
                $typeOfPresentismId === $typesOfPresentismIds['PRESENTE']
                ||
                $typeOfPresentismId === $typesOfPresentismIds['DOMINIGO']
                ||
                $typeOfPresentismId === $typesOfPresentismIds['FALTA_JUSITIFICADA']
                ||
                $typeOfPresentismId === $typesOfPresentismIds['TRAMITA_OBLIGITORIO']
            ) {
                $statistic[$sellerId]['price'] += 640;
            } else if (
                $typeOfPresentismId === $typesOfPresentismIds['AUSENCIA_JUSITIFICADA']
                ||
                $typeOfPresentismId === $typesOfPresentismIds['LEGADA_TARDE']
            ) {
                $statistic[$sellerId]['price'] -= 640;
            } else if ($typeOfPresentismId === $typesOfPresentismIds['VACOCIONES']) {
                $statistic[$sellerId]['price'] += 768;
            }

            foreach ($statistic[$sellerId]['statisticByStatuses'] as $typeId => &$part) {
                if ((int) $typeId === (int) $typeOfPresentismId) {
                    $part['quantity'] ++;  
                }
            }
        }

        foreach ($statistic as &$statPart) {
            $minusPersents = 0;
            $quantityOfStatuses = $statPart['statisticByStatuses'][$typesOfPresentismIds['LEGADA_TARDE']]['quantity'];
            if ($quantityOfStatuses >= 10) {
                $minusPersents = 50;
            } elseif ($quantityOfStatuses >= 5) {
                $minusPersents = 25;
            }



            $quantityOfStatuses = $statPart['statisticByStatuses'][$typesOfPresentismIds['AUSENCIA_JUSITIFICADA']]['quantity'];
            if ($quantityOfStatuses >= 3) {
                $minusPersents = 70;
            } elseif ($minusPersents < 40 && $quantityOfStatuses >= 2) {
                $minusPersents = 40;
            } elseif ($minusPersents < 25 && $quantityOfStatuses >= 1) {
                $minusPersents = 25;
            }

            $statPart['minusPersents'] = $minusPersents;
            if ($minusPersents > 0 && $statPart['price'] > 0) {
                $minusPrice = round((float) $statPart['price'] / 100 * $minusPersents, 2);
                $statPart['minusPrice'] = $minusPrice;
                $statPart['price'] = $statPart['price'] - $minusPrice;
            }
        }

        /*
       5 llegadas tarde    -25%
10 llegadas tarde    -50%
1 ausencia injustificada    -25%
2 ausencia injustificada      -40%
3 ausencia injustificada      -70% 
        */

        return $statistic;
    }
}
