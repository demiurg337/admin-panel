<?php

namespace AppBundle\Helper;

use \Google_Client;
use \Google_Service_Gmail;
class Gmail {
    private $basePath;
    private $client;

    public function __construct()
    {
        $this->basePath = __DIR__. '/../../..';
        $this->client = $this->getClient();
    }
    
    public function getGmailService()
    {
        $service = new Google_Service_Gmail($this->getClient());
        return $service;
    }

    public function getClient()
    {
        if ($this->client) {
            return $this->client;
        }

        $scopes = implode(' ', array(
          \Google_Service_Gmail::GMAIL_READONLY)
        );


        $basePath = $this->basePath;
        $pathToKey = $basePath . '/additional_files/key_for_google.json';


          $client = new Google_Client();
          $client->setApplicationName('Znube');
          $client->setScopes($scopes);
          $client->setAuthConfig($pathToKey);
          $client->setAccessType('offline');

          // Load previously authorized credentials from a file.
          $credentialsPath = $basePath . '/.credentials/gmail-php-quickstart.json';
        $accessToken = json_decode(file_get_contents($credentialsPath), true);

          $client->setAccessToken($accessToken);

          // Refresh the token if it's expired.
          if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
          }
    
        return $client; 
    }

    public function getGmailMessagesWithInfo($captionOfLetter)
    {
        $userId = 'me';
        $service = $this->getGmailService();
        $pageToken = NULL;
        $messages = array();
        $opt_param = array();

          do {
            try {
              if ($pageToken) {
                $opt_param['pageToken'] = $pageToken;
              }
                
                $opt_param['q'] = 'has:attachment subject:' . $captionOfLetter;

              $messagesResponse = $service->users_messages->listUsersMessages($userId, $opt_param);
              if ($messagesResponse->getMessages()) {
                $messages = array_merge($messages, $messagesResponse->getMessages());
                $pageToken = $messagesResponse->getNextPageToken();
              }
            } catch (Exception $e) {
              print 'An error occurred: ' . $e->getMessage();
            }
          } while ($pageToken);

          return $messages;
    }

    public function getInfoFromMessage($messageId)
    {
        $service = $this->getGmailService();
        $infoABoutMessage =$service->users_messages->get('me', $messageId);
        $headers = $infoABoutMessage->payload->headers;
        
        $date = '';
        foreach ($headers as $head) {
            if ($head->name === 'Date') {
                $date = $head->value;
            }
        }

        $partsOfMessage = $infoABoutMessage->payload->parts;
        $attachmentId = null;
        foreach ($partsOfMessage as $part) {
            if ($part->body->attachmentId !== null) {
                $attachmentId = $part->body->attachmentId;
                break;
            }
        }


        if ($attachmentId !== null) {
            $infoAboutAttacmnent = $service->users_messages_attachments->get('me', $messageId, $attachmentId);
            $decodedFile = base64_decode($infoAboutAttacmnent->data);
            return array(
                'attachment' => array($decodedFile),
                'dateOfReceiving' => $date
            );
        }

        return null;
    }
}
