<?php

namespace AppBundle\Helper;


use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;

/*
This code doesn't use most part of functional Doctrine
for better perfomance
*/
class FileGenerator {
    private $basePath;
    private $dbConn;
    const SEARCH_DELIMETER = '|||||';

    static $TYPE_OF_FILE = [
        'ALL_INFO' => 1,
        'PAYMENT_INFO' => 2,
        'STOCK' => 3,
        'PRODUCT_PRICES' => 4,
        'NEED_PRODUCTS' => 5
    ];

    public static $typesOfExport = array(
        'BY_STORE' => 1,
        'BY_SELLER' => 2,
        'BY_PRODUCT' => 3,
        'BY_PROVIDER' => 4,
        'BY_CATEGORIES' => 5,
        'BY_PRODUCTS_QUANTITY_PER_STORE' => 6,
        'BY_PRODUCTS_PRICE_PER_STORE' => 7,
        'BY_PRICE_PER_STORE_AND_PAYMENT_TYPE' => 8,
        'BY_PRICE_PER_STORE_AND_PAYMENT_TYPE_BY_MONTS' => 9,
        'WITH_STOCK' => 10,
        'PRODUCTS_WHICH_NEED' => 11,
        'ALL_PRODUCTS_WITH_STOCK' => 12,
    );

    public $typesOfExportNames = array();

    public function __construct($doctrine)
    {
        $this->dbConn = $doctrine->getManager()->getConnection()->getWrappedConnection();
        $this->basePath = __DIR__. '/../../..';


        $this->typesOfExportNames = array(
            self::$typesOfExport['BY_STORE'] => 'Locales',
            self::$typesOfExport['BY_SELLER'] => 'Vendedores',
            self::$typesOfExport['BY_PRODUCT'] => 'Productos',
            self::$typesOfExport['BY_PROVIDER'] => 'Provedores',
            self::$typesOfExport['BY_CATEGORIES'] => 'Categorias',
            self::$typesOfExport['BY_PRODUCTS_QUANTITY_PER_STORE'] => 'Cantidad vendida por locales',
            self::$typesOfExport['BY_PRODUCTS_PRICE_PER_STORE'] => 'Precio del producto por locales',
            self::$typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE'] => 'Medio de pago',
            self::$typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE_BY_MONTS'] => 'Ventas comparativas',
            self::$typesOfExport['WITH_STOCK'] => 'Dias de stock',
            self::$typesOfExport['PRODUCTS_WHICH_NEED'] => 'Productos necesarios',
            self::$typesOfExport['PRODUCTS_WHICH_NEED'] => 'Productos necesarios',
            self::$typesOfExport['ALL_PRODUCTS_WITH_STOCK'] => 'All products with stock',
        );
    }
    
    public static function prepareDecimal($a) 
    {
        $a = trim($a);
        $a = str_replace(',', '.', $a);
        return (float) $a;
    }

    public static function getProviderCodeFromProductCode($productCode) 
    {
        return mb_substr($productCode, 0, 3, 'UTF-8');
    }

    public static function encodeValuesForSearching($values)
    {
        /*
        First should articul
        second - code
        */
        return implode(self::SEARCH_DELIMETER, $values);
    }

    public static function decodeValuesForSearching($values)
    {
        /*
        First should articul
        second - code
        */
        $res = array();
        foreach ($values as $str) {
            $item = explode(self::SEARCH_DELIMETER, $str);
            $res[] = [
                'articul' => $item[0],
                'code' => $item[1]
            ];
        }

        return $res;
    }
    
    public static function getFiltrationByIdsPartSql($dbColumnName, $ids)
    {
        if (count($ids) <= 0) {
            return '';
        }

        $filterSqlPart = '';
        $filterSqlPart .= ' AND (';
        $i = 0;
        foreach ($ids as $id) {
            if ($i > 0) {
                $filterSqlPart .= ' OR ';
            }

            /*
            (int) - Will do escape for string
            */
            $filterSqlPart .= ' ' . $dbColumnName . ' = ' . ((int) $id);
            $i++;
        }
        $filterSqlPart .= ' ) ';

        return $filterSqlPart;
    }

    public static function prepareString($str) 
    {
        return utf8_encode($str);
    }


    public static function generateFiltrationSqIfCan($additionalParams)
    {
        $filterSqlPart = '';
        if (isset($additionalParams['storeIds'])) {
            $filterSqlPart .= self::getFiltrationByIdsPartSql('store_order_product.store_id', $additionalParams['storeIds']);
        }

        if (isset($additionalParams['sellerIds'])) {
            $filterSqlPart .= self::getFiltrationByIdsPartSql('store_order_product.seller_id', $additionalParams['sellerIds']);
        }

        if (isset($additionalParams['categoryIds'])) {
            $filterSqlPart .= self::getFiltrationByIdsPartSql('store_order_product.category_id', $additionalParams['categoryIds']);
        }

        if (isset($additionalParams['productParams'])) {
            $quantity = count($additionalParams['productParams']);
            if ($quantity > 0) {
                $filterSqlPart .= ' AND (';
                for ($i = 1; $i <= $quantity; $i++) {
                    if ($i > 1) {
                        $filterSqlPart .= ' OR ';
                    }

                    $filterSqlPart .= " (store_order_product.code = :code$i AND store_order_product.articul = :articul$i) ";
                }            
                $filterSqlPart .= ' ) ';
            }
        }

        return $filterSqlPart;
    }
     
    public static function bindValuesIfNeed($st, $additionalParams)
    {
        if (isset($additionalParams['productParams'])) {
            $quantity = count($additionalParams['productParams']);
            $values = self::decodeValuesForSearching($additionalParams['productParams']);
            $i = 0;
            foreach ($values as $val) {
                $i++;
                $st->bindValue(':code' . $i, $val['code']);
                $st->bindValue(':articul' . $i, $val['articul']);
            }
        }
    }

    public function getAllStores($storeIds = array())
    {
        $sqlPart = '';
        if (count($storeIds) > 0) {
            $sqlPart = ' WHERE store_id in (?' .str_repeat(',?', count($storeIds) - 1) . ') ';
        }

        $st = $this->dbConn->prepare(
            'SELECT 
                *
            FROM
                store
            ' .$sqlPart
        );

        if (! $st->execute($storeIds)) {
            throw new \Exception('Cant get stores');
        }

        $stores = $st->fetchAll(\PDO::FETCH_OBJ);

        return $stores;
    }

     public function getInfoForProductsWhichWillNeed($from, $to, $additionalParams =array()) 
    {

        $dateForStock = $this->getActualDateForStock($from, $to);
        if ($dateForStock !== null) {
            $dateForStock = new \DateTime($dateForStock);
        } else {
            $dateForStock = new \DateTime();
        }

        $dateForNeedProducts = $this->getActualDateForNeedProducts($from, $to);
        if ($dateForNeedProducts !== null) {
            $dateForNeedProducts = new \DateTime($dateForNeedProducts);
        } else {
            $dateForNeedProducts = new \DateTime();
        }

        return $this->getStockAndHowNeedValues($dateForStock, $dateForNeedProducts);
    }

    public function getInfoForStatisticByStoresAndPaymentType($from, $to, $additionalParams =array()) 
    {
        $filterSqlPart = self::generateFiltrationSqIfCan($additionalParams);
        $st = $this->dbConn->prepare(
            'SELECT 
                store_order_product.price,
                store_order.type_of_money_id,
                store.external_store_id
             FROM 
                store_order_product
                    JOIN store_order ON store_order.store_order_id = store_order_product.store_order_id
                    JOIN store ON store_order_product.store_id = store.store_id
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
                '. $filterSqlPart .'
            ORDER BY store_order_product.code'
        );


        if (! $st->execute(array(
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s'),
        ))) {
            throw new \Exception('Cant get store orders');
        }
        
        $res = $st->fetchAll(\PDO::FETCH_OBJ);

        if (isset($additionalParams['storeIds'])) {
            $stores = $this->getAllStores($additionalParams['storeIds']);
        } else {
            $stores = $this->getAllStores();
        }

        $initilaValues = array();
        foreach ($stores as $store) {
            $initilaValues[$store->external_store_id] = array(
                'real_money' => 0,
                'other' => 0,
            );
        }
        
        $statistic = array();
        foreach ($res as $row) {
            if (!isset($statistic[$row->external_store_id])) {
                $statistic[$row->external_store_id] = array(
                    'store_code' => $row->external_store_id,
                    'real_money' => 0,
                    'other' => 0,
                    'total' => 0
                );
            }
            

            $statistic[$row->external_store_id]['total'] += (float) $row->price;        

            if ((int) $row->type_of_money_id === 1) {
                $statistic[$row->external_store_id]['real_money'] += (float) $row->price;
            } else {
                $statistic[$row->external_store_id]['other'] += (float) $row->price;
            }
        }


        return $statistic;
    }
    
    public function prepareRowsForFileWithStockInformation($preparedInfo, $additionalParams =array())
    {
        ////////////////////////////////////////////////////
        // Captions
        $stores = $this->getAllStores();
        $captions = [];
        if (isset($additionalParams['NameOfEntity'])) {
            if (is_array($additionalParams['NameOfEntity'])) {
                foreach ($additionalParams['NameOfEntity'] as $val) {
                    $captions[] = $val;
                }
            } else {
                $captions[] = $additionalParams['NameOfEntity'];
            }
        } else {
            $captions[] = '';
        }
        foreach ($stores as $store) {
            $captions[] = $store->external_store_id;
        }
        $captions[] = 'Total';
        ///////////////////////////////////////////
        $allCaptions = array_merge($captions, ['', ''], $captions, ['', ''], $captions);

        $emptyRow = [];
        foreach ($allCaptions as $cap) {
            $emptyRow[] = '';
        }
        ///////////////////////////////////////////


        //starting of row's creation
        $rows = [];
        $rows[] = $emptyRow;

        $tmpRow =  $emptyRow;
        $tmpRow[2] = 'Total Days ';
        $tmpRow[3] = $additionalParams['diffDays'];
        $rows[] = $tmpRow;

        $tmpRow[2] = 'Sales from ';
        $tmpRow[3] = $additionalParams['from']->format('Y-m-d') . ' up to ' . $additionalParams['to']->format('Y-m-d');
        $rows[] = $tmpRow;

        $rows[] = $emptyRow;



        ////////////////////////////////////////////////////
        // top row with names
        $lengthOfTable = count($captions);
        $indexOfCaption = round($lengthOfTable / 2);
        $spaceLength = 2;
        $longCaptions = $emptyRow;
        $longCaptions[$indexOfCaption] = 'STOCK';
        $longCaptions[$lengthOfTable + $spaceLength + $indexOfCaption] = 'VENTAS';
        $longCaptions[$lengthOfTable * 2 + $spaceLength * 2 + $indexOfCaption] = 'VENTAS';
        ////////////////////////////////////////////////////

        $rows[] = $longCaptions;

        $rows[] = $emptyRow;
        $rows[] = $allCaptions;
        foreach ($preparedInfo as $info) {
            $values = [];

            if (is_array($info['caption'])) {
                foreach ($info['caption'] as $caption) {
                    $values[] = $caption;
                }
            } else {
                $values[] = $info['caption'];
            }

            $total = 0;
            foreach ($stores as $store) {
                $total += (int) $info['stockIn'][$store->external_store_id];
                $values[] = $info['stockIn'][$store->external_store_id];
            }
            $values[] = $total;


            $values[] = '';
            $values[] = '';

            if (is_array($info['caption'])) {
                foreach ($info['caption'] as $caption) {
                    $values[] = $caption;
                }
            } else {
                $values[] = $info['caption'];
            }

            $total = 0;
            foreach ($stores as $store) {
                $total += (int) $info['priceIn'][$store->external_store_id];
                $values[] = $info['priceIn'][$store->external_store_id];
            }
            $values[] = $total;


            $values[] = '';
            $values[] = '';


            if (is_array($info['caption'])) {
                foreach ($info['caption'] as $caption) {
                    $values[] = $caption;
                }
            } else {
                $values[] = $info['caption'];
            }

            $total = 0;
            foreach ($stores as $store) {
                $total += (int) $info['diaOfStock'][$store->external_store_id];
                $values[] = $info['diaOfStock'][$store->external_store_id];
            }
            $values[] = $info['diaOfStock']['Total'];
            $rows[] = $values;

        }

        return $rows;
    }

    function infoWithStock($writer, $rows)
    {
        $styleCaption = (new StyleBuilder())
           ->setBackgroundColor(Color::DARK_RED)
           ->build();
        $i = 0;
        foreach ($rows as $row) {

            $i++;
            if ($i === 5) {
                $writer->addRowWithStyle($row, $styleCaption);
                continue;
            }

            $writer->addRow($row);
        }
    }
    
    public function informationWithStatisticPerStoreOnMonth($dateOfMonth, $additionalParams)
    {
        $date = $dateOfMonth;
        $monthName = $dateOfMonth->format('F');
        $qunatityOfDays = (int) $date->format('t');
        $year = $date->format('Y');
        $previousYear = $year - 1;
        if (isset($additionalParams['storeIds'])) {
            $stores = $this->getAllStores($additionalParams['storeIds']);
        } else {
            $stores = $this->getAllStores();
        }
        
        $preparedInfo = array();
        //set default values
        foreach ($stores as $store) {
            $preparedInfo[$store->external_store_id] = array();
            for ($i = 1; $i <= $qunatityOfDays; $i++) {
                $preparedInfo[$store->external_store_id][$i-1] = array(
                    'store_code' => $store->external_store_id,
                    'real_money' => 0,
                    'other' => 0,
                    'total' => 0,
                    'previousTotal' => 0,
                );
            }
        }

        //set real values if exist
        for ($i = 1; $i <= $qunatityOfDays; $i++) {
            $from = new \DateTime($date->format('Y-m') . '-' . $i . ' 00:00:00');
            $to = new \DateTime($date->format('Y-m') . '-' . $i . ' 23:59:59');
            $infoPeriod = $this->getInfoForStatisticByStoresAndPaymentType($from, $to);


            $fromPreviousYear = new \DateTime($previousYear . '-' .  $date->format('m') . '-' . $i . ' 00:00:00');
            $toPreviousYear = new \DateTime($previousYear . '-' .  $date->format('m') .  '-' . $i . ' 23:59:59');
            $infoPeriodPreviousYear = $this->getInfoForStatisticByStoresAndPaymentType($fromPreviousYear, $toPreviousYear);

            foreach ($stores as $store) {
                $storeId = $store->external_store_id;
                
                if (isset($infoPeriod[$storeId])) {
                    $preparedInfo[$storeId][$i-1] = $infoPeriod[$storeId];
                }

                $preparedInfo[$storeId][$i-1]['previousTotal'] = 0;

                if (isset($infoPeriodPreviousYear[$storeId]['total'])) {
                    $preparedInfo[$storeId][$i-1]['previousTotal'] = $infoPeriodPreviousYear[$storeId]['total'];
                }
            }
        }

        
        $cells = [];
        $emptyLine = [];

        foreach ($preparedInfo as $storeId => $val) {
            $cells[] = ['','','',$storeId, '','','',''];
            $cells[] = array('DIA', 'AC.' . $previousYear, 'DIA.' . $previousYear, 'DIA.' .  $year, 'DIA.' .  $year, 'DIA.' .  $year, 'ACU.' .  $year, 'DIF/ANO');

            $cells[] = ['','','',
                'EFECT.', 'TARJ.', 'TOTAL',
            '','',];

            $localTotal = 0;
            $acumulatePreviousTotal = 0;
            $acumulateTotal = 0;
            for ($i = 1; $i <= $qunatityOfDays; $i++) {
                $rowToFile = [];
                $acumulatePreviousTotal += $val[$i-1]['previousTotal'];
                $acumulateTotal += $val[$i-1]['total'];
                $diffAcumulations = $acumulateTotal - $acumulatePreviousTotal;

                $rowToFile[0] = $i;
                $rowToFile[1] = $acumulatePreviousTotal;
                $rowToFile[2] = $val[$i-1]['previousTotal'];
                $rowToFile[3] = $val[$i-1]['real_money'];
                $rowToFile[4] = $val[$i-1]['other'];
                $rowToFile[5] = $val[$i-1]['total'];
                $rowToFile[6] = $acumulateTotal;
                $rowToFile[7] = $diffAcumulations;

                $localTotal += (float) $val[$i-1]['total'];

                $cells[] = $rowToFile;

            }

            $cells[] = ['', '', 'TOTAL',   '', '', $localTotal,   '',''];

            //adding of empty lines
            if (count($emptyLine) <= 0) {
                for ($i = 0; $i < count($cells[0]); $i++) {
                    $emptyLine[] = '';
                }
            }

            $cells[] = $emptyLine;
            $cells[] = $emptyLine;

        }
       
        array_unshift(
            $cells, 
            $emptyLine, 
            $emptyLine,
            ['','','','VENTAS COMPARATIVAS', '', '', '', ''],
            ['','','', $monthName . ' ' . $year, '', '', '', ''],
            $emptyLine, 
            $emptyLine
            );
        
        return $cells;
    }

    function addInfoWithStatisticForStoreToSheet($writer, $cells, $nameOfSheet)
    {
        $border = (new BorderBuilder())
            ->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
            ->setBorderLeft(Color::BLACK, Border::WIDTH_THIN)
            ->setBorderRight(Color::BLACK, Border::WIDTH_THIN)
            ->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
            ->build();

        $defaultStyle = (new StyleBuilder())
           ->setBorder($border)
           ->build();

        $style = (new StyleBuilder())
           //->setFontBold()
           //->setFontSize(12)
           ->setBorder($border)
           ->setShouldWrapText()
           ->setBackgroundColor(Color::YELLOW)
           ->build();

        $styleCaption = (new StyleBuilder())
           ->setBackgroundColor(Color::DARK_RED)
           ->build();

         foreach($cells as $rowToFile) {
            $rowStyle = $defaultStyle;
            if ($rowToFile[3] === 'EFECT.') {
                $rowStyle = $style;
            } elseif ($rowToFile[2] === '' && $rowToFile[3] !== '') {
                $rowStyle = $styleCaption;
            } elseif ($rowToFile[0] === '' && $rowToFile[2] === '') {
                $writer->addRow($rowToFile);
                continue;
            }

            $writer->addRowWithStyle($rowToFile, $rowStyle);
        }
        $writer->getCurrentSheet()->setName($nameOfSheet); 

    }

    public static function calculateDiaOfStock($globalVals, $stock, $emptyValue, $diffDays) 
    {
        $diaOfStock = $emptyValue;
        
        $totalStock = 0;
        $totalGlobal = 0;
        foreach ($stock as $keyOfStore => $stockVal) {
            $totalStock += (float) $stock[$keyOfStore];

            if (isset($globalVals[$keyOfStore])) {
                $totalGlobal += (float) $globalVals[$keyOfStore];
            }
        }   
        $stock['Total'] = $totalStock;
        $globalVals['Total'] = $totalGlobal;

        foreach ($stock as $keyOfStore => $stockVal) {
            if (!isset($globalVals[$keyOfStore])) {
                $globalVals[$keyOfStore] = 0;
            }
            $quantityForAllPeriod = $globalVals[$keyOfStore];
            
            $sellPerDay = (int) $quantityForAllPeriod / (int) $diffDays;

            $diaOfStock[$keyOfStore] = 0;
            if ($sellPerDay !== 0) {
                $diaOfStock[$keyOfStore] = round((int) $stock[$keyOfStore] / $sellPerDay, 2);
            }
        }   

        return $diaOfStock;
    }

    public function getAllProductsWithStock($date)
    {

        $emptyValue = $this->getBaseEmptyArrayWithAllStores();
        $res = [];
        $limit = 20000;
        $offset = 0;
        //while (true) {
        $st = $this->dbConn->prepare(
            'SELECT 
                product.product_id,
                product.code,
                product.articul,
                product.size,
                product.color,
                IFNULL((SELECT categories.name FROM categories WHERE categories.category_id = product.category_id LIMIT 1), "") AS categoryName,
                (SELECT provider.code FROM provider WHERE provider.provider_id = product.provider_id LIMIT 1) AS providerCode
             FROM 
                product
            WHERE
                (
                    SELECT 
                        product_stock_situation.product_id 
                    FROM 
                        product_stock_situation 
                    WHERE 
                        product_stock_situation.date = :date 
                        AND 
                        product_stock_situation.product_id = product.product_id 
                    LIMIT 1
                ) IS NOT NULL'
        );
        
        $st->bindValue(':date', $date->format('Y-m-d H:i:s'));

        if (! $st->execute()) {
            throw new \Exception('Cant get products');
        }

        $info = $st->fetchAll(\PDO::FETCH_OBJ);

        foreach ($info as $row) {
            $res[$row->product_id] = [
                'articul' => $row->articul,
                'code' => $row->code,
                'category' => $row->categoryName,
                'size' => $row->size,
                'color' => $row->color,
                'providerCode' => $row->providerCode,
                'stock' => $this->getStockValuesForProductId($row->product_id, $date)
            ];
        }

        return $res;
    }

    public function getStatsticWithStockForProducts($from, $to, $diffDays, $actualDateForStock, $emptyValue)
    {
        $date = $actualDateForStock;
        //////////////////////////////////////////////
        //////////////////////////////////////////////
        //product

        ///////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////
        //Optimization for reducing calculations

        $st = $this->dbConn->prepare(
            'SELECT 
                product.code,
                product.articul,
                IFNULL((SELECT categories.name FROM categories WHERE categories.category_id = product.category_id LIMIT 1), "") AS categoryName,
                (SELECT provider.code FROM provider WHERE provider.provider_id = product.provider_id LIMIT 1) AS providerCode,
                (SELECT product_stock_situation.product_id FROM product_stock_situation WHERE product_stock_situation.product_id = product.product_id LIMIT 1) AS isWithSotck
             FROM 
                product
            GROUP BY product.code
            '
        );

        if (! $st->execute()) {
            throw new \Exception('Cant get products');
        }

        $allProducts = $st->fetchAll(\PDO::FETCH_OBJ);
        $allProductsPrepared = [];
        foreach ($allProducts as $row) {
                $code = $row->code;
                $row = array(
                    'code' => $row->code,
                    'articul' => $row->articul,
                    'categoryName' => $row->categoryName,
                    'providerCode' => $row->providerCode,
                    'needStock' => $row->isWithSotck !== null,
                    'quantityIn' => $emptyValue,
                    'priceIn' => $emptyValue,
                );

                $row['stockIn']  = $emptyValue;
                $row['diaOfStock'] = $emptyValue;

                
                $rowuctName =  $row['code'] . ' ';

                if (trim($row['articul']) !== '') {
                    $rowuctName .= $row['articul'];
                }
                $row['caption'] = [];
                $row['caption'][] = self::prepareString($rowuctName);
                $row['caption'][] = self::prepareString($row['categoryName']);
                $row['caption'][] = self::prepareString($row['providerCode']);

                $allProductsPrepared[$code] = $row;
        }
        $allProducts = null;
        ///////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////


        $products = $this->getInfoStatisticByProductsQuantityPerStore($from, $to);
        $goalProductIds = $this->getRealProductIdsWhichWereSoldAt($from, $to);
        foreach ($products as &$prod) {
            /**
            *@Todo need to optimize it, exist some products which cartent in stock bu was sold
            */

            $stock = $this->getStockValuesForProductCode($prod['code'], $date);
            if ($stock === null) {
                $stock = [
                    'fullPrice' => $emptyValue,
                    'quantities' => $emptyValue,
                    'productIds' => []
                ];
            } 
            
            $prod['stockIn'] = $stock['fullPrice'];
            
            $globalVals = $this->getGloabalValuesForProductCode($prod['code'], $from, $to);
            $diaOfStock = array();
            if ($globalVals === null) {
                $globalVals = $emptyValue;
            }
            $diaOfStock = self::calculateDiaOfStock($globalVals, $stock['fullPrice'], $emptyValue, $diffDays);

            $prod['diaOfStock'] = $diaOfStock;

            $productName =  $prod['code'] . ' ';

            if (trim($prod['articul']) !== '') {
                $productName .= $prod['articul'];
            }

            $prod['caption'] = [];
            $prod['caption'][] = self::prepareString($productName);
            $prod['caption'][] = self::prepareString($prod['categoryName']);
            $prod['caption'][] = self::prepareString($prod['providerCode']);

            $allProductsPrepared[$prod['code']] = $prod;
            $allProductsPrepared[$prod['code']]['needStock'] = false;
        }
        
        $products = $allProductsPrepared;
        $allProductsPrepared = null;
        foreach ($products as &$prod) {
            if ($prod['needStock']) {
                $stock = $this->getStockValuesForProductCode($prod['code'], $date);
                if ($stock === null) {
                    $stock = $emptyValue;
                } else {
                    $stock = $stock['fullPrice'];
                }
                $prod['stockIn'] = $stock;
            }
        }

        return $products;
    }

    public function getStatsticWithStockForProviders($from, $to, $diffDays, $actualDateForStock, $emptyValue)
    {
        $date = $actualDateForStock;
        //////////////////////////////////////////////
        //////////////////////////////////////////////
        //provider
        //Optimization for reducing calculations

        $st = $this->dbConn->prepare(
            'SELECT 
                product.provider_id,
                (SELECT provider.code FROM provider WHERE provider.provider_id = product.provider_id LIMIT 1) AS providerCode,
                (SELECT product_stock_situation.product_id FROM product_stock_situation WHERE product_stock_situation.product_id = product.product_id LIMIT 1) AS isWithSotck
             FROM 
                product
            GROUP BY product.provider_id
            '
        );

        if (! $st->execute()) {
            throw new \Exception('Cant get products');
        }

        $allProviders = $st->fetchAll(\PDO::FETCH_OBJ);
        $allProvidersPrepared = [];
        foreach ($allProviders as $row) {
            $code = $row->providerCode;
            $part = array(
                'provider_id' => $row->provider_id,
                'providerCode' => $row->providerCode,
                'needStock' => $row->isWithSotck !== null,
                'quantityIn' => $emptyValue,
                'priceIn' => $emptyValue,
            );

            $part['stockIn']  = $emptyValue;
            $part['diaOfStock'] = $emptyValue;

            $part['caption'] = $code;

            $allProvidersPrepared[$row->provider_id] = $part;
        }
        $allProviders = null;
        ///////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////

        $providers = $this->getInfoStatisticByProviderQuantityPerStore($from, $to);
        foreach ($providers as &$prov) {
            $stock = $this->getStockValuesForProvider($prov['provider_id'], $date);
            if ($stock === null) {
                $stock = [
                    'fullPrice' => $emptyValue,
                    'quantities' => $emptyValue
                ];
            }

            $prov['stockIn'] = $stock['fullPrice'];

            $globalVals = $this->getGloabalValuesForProvider($prov['provider_id'], $from, $to);
            $diaOfStock = array();
            if ($globalVals === null) {
                $globalVals = $emptyValue;
            }
            $diaOfStock = self::calculateDiaOfStock($globalVals, $stock['fullPrice'], $emptyValue, $diffDays);
            $prov['diaOfStock'] = $diaOfStock;

            $prov['caption'] = $prov['code'];
            $allProvidersPrepared[$prov['provider_id']] = $prov;
            $allProvidersPrepared[$prov['provider_id']]['needStock'] = false;
        }
        $providers = $allProvidersPrepared;
        $allProvidersPrepared = null;

        foreach ($providers as &$item) {
            if ($item['needStock']) {
                $stock = $this->getStockValuesForProvider($item['provider_id'], $date);
                if ($stock === null) {
                    $stock = $emptyValue;
                } else {
                    $stock = $stock['fullPrice'];
                }
                $item['stockIn'] = $stock;
            }
        }

        return $providers;
    }

    public function getStatsticWithStockForCategories($from, $to, $diffDays, $actualDateForStock, $emptyValue)
    {
        $date = $actualDateForStock;
        //////////////////////////////////////////////
        //////////////////////////////////////////////
        //category
        //Optimization for reducing calculations

        $st = $this->dbConn->prepare(
            'SELECT 
                product.category_id,
                IFNULL((SELECT categories.name FROM categories WHERE categories.category_id = product.category_id LIMIT 1), "") AS categoryName,
                (SELECT product_stock_situation.product_id FROM product_stock_situation WHERE product_stock_situation.product_id = product.product_id LIMIT 1) AS isWithSotck
             FROM 
                product
            GROUP BY product.category_id
            '
        );

        if (! $st->execute()) {
            throw new \Exception('Cant get products');
        }

        $allCategories = $st->fetchAll(\PDO::FETCH_OBJ);
        $allCategoriesPrepared = [];
        foreach ($allCategories as $row) {
                $code = $row->category_id;
                $row = array(
                    'categoryName' => $row->categoryName,
                    'category_id' => $row->category_id,
                    'needStock' => $row->isWithSotck !== null,
                    'quantityIn' => $emptyValue,
                    'priceIn' => $emptyValue,
                    'caption' =>$row->categoryName,
                );

                $row['stockIn']  = $emptyValue;
                $row['diaOfStock'] = $emptyValue;

                $allCategoriesPrepared[$code] = $row;
        }
        $allCategories = null;
        ///////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////

        $categories = $this->getInfoStatisticByCategoryQuantityPerStore($from, $to);
        foreach ($categories as &$cat) {
            $stock = $this->getStockValuesForCategory($cat['category_id'], $date);
            if ($stock === null) {
                $stock = [
                    'fullPrice' => $emptyValue,
                    'quantities' => $emptyValue
                ];
            }

            $cat['stockIn'] = $stock['fullPrice'];
            
            $globalVals = $this->getGloabalValuesForCategory($cat['category_id'], $from, $to);
            $diaOfStock = array();
            if ($globalVals === null) {
                $globalVals = $emptyValue;
            }
            $diaOfStock = self::calculateDiaOfStock($globalVals, $stock['fullPrice'], $emptyValue, $diffDays);
            $cat['diaOfStock'] = $diaOfStock;

            $cat['caption'] = $cat['categoryName'];
            $allCategoriesPrepared[$cat['category_id']] = $cat;
            $allCategoriesPrepared[$cat['category_id']]['needStock'] = false;
        }
        $categories = $allCategoriesPrepared;
        $allCategoriesPrepared = null;
        foreach ($categories as &$item) {
            if ($item['needStock']) {
                $stock = $this->getStockValuesForCategory($item['category_id'], $date);
                if ($stock === null) {
                    $stock = $emptyValue;
                } else {
                    $stock = $stock['fullPrice'];
                }
                $item['stockIn'] = $stock;
            }
        }
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        return $categories;
    }

    private function getBaseEmptyArrayWithAllStores()
    {
        $stores = $this->getAllStores();
        $emptyValue = array();
        foreach ($stores as $store) {
            $emptyValue[$store->external_store_id] = 0;
        }

        return $emptyValue;
    }

    public function getInfoWithStockStatistic($from, $to, $additionalParams =array())
    {
        $now = new \DateTime();
        $diffDays = $to->diff($from);
        $diffDays = (int) $diffDays->days;
        //plus current day
        $diffDays ++;

        $date = $this->getActualDateForStock($from, $to);
        if ($date !== null) {
            $date = new \DateTime($date);
        } else {
            $date = new \DateTime();
        }
        $actualDateForStock = $date;
        
        $emptyValue = $this->getBaseEmptyArrayWithAllStores();
        $emptyValue['Total'] = 0;

        $needPageIndex = null;
        if (isset($additionalParams['needOnlyPageId'])) {
            $needPageIndex = (int) $additionalParams['needOnlyPageId'];
        }
        ////////////////////////////////////////////////
        //////////////////////////////////////////////
        //////////////////////////////////////////////

        $sheets = [];

        $index = 0;
        $sheet = [];
        if ($needPageIndex === null || $needPageIndex === $index) {
            $products = $this->getStatsticWithStockForProducts($from, $to, $diffDays, $actualDateForStock, $emptyValue);
            $sheet['cells'] = $this->prepareRowsForFileWithStockInformation($products, array(
                'diffDays' => $diffDays,
                'from' => $from,
                'to' => $to,
                'NameOfEntity' => ['Product', 'Category', 'Provider']
            ));
        }

        $sheet['name'] = 'DS por producto';
        $sheets[$index] = $sheet;

        $products = null;
        
        //-------------
        $index = 1;

        $sheet = [];
        if ($needPageIndex === null || $needPageIndex === $index) {
            $providers = $this->getStatsticWithStockForProviders($from, $to, $diffDays, $actualDateForStock, $emptyValue);
            $sheet['cells'] = $this->prepareRowsForFileWithStockInformation($providers, array(
                'diffDays' => $diffDays,
                'from' => $from,
                'to' => $to,
                'NameOfEntity' => 'Provider'
            ));
        }

        $sheet['name'] = 'DS por proveedor';
        $sheets[$index] = $sheet;

        //-------------
        $index = 2;
        $sheet = [];
        if ($needPageIndex === null || $needPageIndex === $index) {
            $categories = $this->getStatsticWithStockForCategories($from, $to, $diffDays, $actualDateForStock, $emptyValue);
            $sheet['cells'] = $this->prepareRowsForFileWithStockInformation($categories, array(
                'diffDays' => $diffDays,
                'from' => $from,
                'to' => $to,
                'NameOfEntity' => 'Categoria'
            ));
        }
        $sheet['name'] = 'DS por Categoria';
        $sheets[$index] = $sheet;

        return $sheets;
    }

    public function getFileWithStockStatistic($from, $to, $additionalParams =array())
    {
        $sheets = $this->getInfoWithStockStatistic($from, $to, $additionalParams =array());

        $fileName = 'stock_statistic ' . $from->format('Y-m') . '-' . $from->format('Y-m') . '.xlsx';
        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser($fileName);

        
        foreach ($sheets as $sheet) {
            $writer->getCurrentSheet()->setName($sheet['name']);
            $this->infoWithStock($writer, $sheet['cells']);
            $newSheet = $writer->addNewSheetAndMakeItCurrent();
        }

        $writer->close();
    }

    public function getNameOfSheetByDate($date) 
    {
        $monthName = $date->format('F');
        $year = $date->format('Y');
        return $monthName . '-' . $year;
    }

    public function getDatesForGenerationsStatisticByMonhts($from, $to) 
    {
        $month = (int) $from->format('m');
        $year = (int) $from->format('Y');

        $dates = array();
        while (true) {
            $date = new \DateTime($from->format($year . '-' . $month .'-1 00:00:00'));
            if ($date > $to) {
                break;
            }

            $dates[] = $date;
            $month ++;

            if ($month > 12) {
                $year ++;
                $month = 1;
            }
        }

        return $dates;
    }

    public function getInformationForStatisticByMonths($from, $to, $additionalParams =array())
    {

        $dates = $this->getDatesForGenerationsStatisticByMonhts($from, $to);
        $sheets = [];
        foreach ($dates as $date) {
            $nameOfSheet = $this->getNameOfSheetByDate($date);

            $cells = $this->informationWithStatisticPerStoreOnMonth($date, $additionalParams);
            $sheets[] = [
                'cells' => $cells,
                'name' => $nameOfSheet,
                'date' => $date
            ];
        } 

        return $sheets;
    }

    public function getFileWithStatiscByMonnts($sheets)
    {
        $fileName = 'statistic_per_store.xlsx';
        $writer = WriterFactory::create(Type::XLSX);
        $writer->openToBrowser($fileName);

        foreach ($sheets as $sheet) {
            $this->addInfoWithStatisticForStoreToSheet($writer, $sheet['cells'], $sheet['name']);
            $newSheet = $writer->addNewSheetAndMakeItCurrent();
        }
        
        $writer->close();

    }

    public function getStoreIdByCode($codeOfStore)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                store_id 
            FROM
                store_additional_ids
            WHERE
                store_additional_id = :code
            LIMIT 1
            '
        );

        $st->bindValue(':code', $codeOfStore);
        
        if (!$st->execute()) {
            throw new \Exception('Error when gettin store !');
        }

        if ($res = $st->fetchObject()) {
            return $res->store_id;
        }

        return null;
    } 

    public function getOrder($orderExternalId)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                store_order_id 
            FROM
                store_order
            WHERE
                store_order_external_id = :orderExternalId
            LIMIT 1
            '
        );

        $st->bindValue(':orderExternalId', $orderExternalId);
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order !');
        }

        if ($res = $st->fetchObject()) {
            return $res->store_order_id;
        }

        return null;
    }

    public function getActualDateForStock($from, $to)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                max(date) AS maxDate
            FROM
               product_stock_situation 
            WHERE
                date >= :from
                AND
                date <= :to
            '
        );

        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting date !');
        }

        if ($res = $st->fetchObject()) {
            return $res->maxDate;
        }

        return null;
    }

    public function getActualDateForNeedProducts($from, $to)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                max(date) AS maxDate
            FROM
               product_which_need_situation
            WHERE
                date >= :from
                AND
                date <= :to
            '
        );

        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting date !');
        }

        if ($res = $st->fetchObject()) {
            return $res->maxDate;
        }

        return null;
    }

    public function getStockValuesForProductCode($productCode, $date, $forProdutIds = [])
    {
        //escape the strings
        foreach ($forProdutIds as &$id) {
            $id = (int) $id;
        }

        $st = $this->dbConn->prepare(
            'SELECT 
                product.product_id,
                product.price,
                quantity ,
                (SELECT store.external_store_id FROM store WHERE store.store_id = product_stock_situation.store_id LIMIT 1) AS external_store_id
            FROM
                product 
                JOIN product_stock_situation
                    ON product.product_id = product_stock_situation.product_id
            WHERE
                product.code = :productCode
                AND
                product_stock_situation.date = :date
                '. (count($forProdutIds) > 0 ? (' AND product.product_id IN  (' . implode(',', $forProdutIds) .')') : '')
        );

        $st->bindValue(':productCode', $productCode);
        $st->bindValue(':date', $date->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order !');
        }
        
        if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
            $quantities = array();
            $fullPrice = array();
            foreach ($res as $row) {
                if (! isset($quantities[$row->external_store_id])) {
                    $quantities[$row->external_store_id] = 0;
                    $fullPrice[$row->external_store_id] = 0;
                }

                $quantities[$row->external_store_id] += (int) $row->quantity;
                $fullPrice[$row->external_store_id] += (int) $row->quantity * (float) $row->price;
            }

            return array(
                'fullPrice' => $fullPrice,
                'quantities' => $quantities,
            );
        }

        return null;
    }

    public function getStockValuesForProductId($productId, $date)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                product_id,
                quantity ,
                (SELECT store.external_store_id FROM store WHERE store.store_id = product_stock_situation.store_id LIMIT 1) AS external_store_id
            FROM
                product_stock_situation
            WHERE
                product_stock_situation.product_id = :productId
                AND
                product_stock_situation.date = :date
                '
        );

        $st->bindValue(':productId', $productId);
        $st->bindValue(':date', $date->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order !');
        }
        
        if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
            $quantities = array();
            foreach ($res as $row) {
                if (! isset($quantities[$row->external_store_id])) {
                    $quantities[$row->external_store_id] = 0;
                }

                $quantities[$row->external_store_id] += (int) $row->quantity;
            }

            return $quantities;
        }

        return null;
    }

    public function getStockAndHowNeedValues($dateForStock, $dateForNeedProducts)
    {
        $forReturn = array();

        $limit = 20000;
        $offset = 0;
        while (true) { 
            $st = $this->dbConn->prepare(
                'SELECT 
                    product.code,
                    product.color,
                    product.size,
                    IFNULL(product_stock_situation.quantity, 0) as quantity,
                    product_which_need_situation.quantity AS needQuantity,
                    store.external_store_id
                FROM
                    product 
                    JOIN product_which_need_situation
                        ON product.product_id = product_which_need_situation.product_id
                    JOIN store 
                        ON store.store_id = product_which_need_situation.store_id
                    LEFT JOIN product_stock_situation
                        ON product.product_id = product_stock_situation.product_id and product_stock_situation.date = :dateForStock
                WHERE
                    product_which_need_situation.date = :dateForNeedProducts
                LIMIT ' . $offset . ',' . $limit 
            );

            $st->bindValue(':dateForStock', $dateForStock->format('Y-m-d H:i:s'));
            $st->bindValue(':dateForNeedProducts', $dateForNeedProducts->format('Y-m-d H:i:s'));
            
            if (!$st->execute()) {
                throw new \Exception('Error when getting order !');
            }

            $stores = $this->getAllStores();

            $initilaValues = array();
            foreach ($stores as $store) {
                $initilaValues[$store->external_store_id] = 0;
            }
            
            if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
                foreach ($res as $row) {
                    $key = $row->code . $row->color . $row->size;
                    if (! isset($forReturn[$key])) {
                        $forReturn[$key] = array();
                        $forReturn[$key]['code'] = $row->code;
                        $forReturn[$key]['color'] = $row->color;
                        $forReturn[$key]['size'] = $row->size;
                        $forReturn[$key]['stock'] = $initilaValues;
                        $forReturn[$key]['need'] = $initilaValues;
                    }

                    if (! isset($forReturn[$key]['stock'][$row->external_store_id])) {
                        $forReturn[$key]['stock'][$row->external_store_id] = 0;
                        $forReturn[$key]['need'][$row->external_store_id] = 0;
                    }

                    $forReturn[$key]['stock'][$row->external_store_id] += (int) $row->quantity;
                    $forReturn[$key]['need'][$row->external_store_id] += (int) $row->needQuantity;
                }

                $offset += $limit;
                
            } else {
                break;
            }
        }

        foreach ($forReturn as &$item) {
            $item['ask'] = [];

            foreach($item['need'] as $key => $howNeed) {
                $isInStock = $item['stock'][$key];
                $item['ask'][$key] =  $howNeed - $isInStock;
            }

        }

        return $forReturn;
    }

    public function getGloabalValuesForProductCode($productCode, $from, $to)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                price ,
                date,
                store.external_store_id
            FROM
                store_order_product 
                JOIN store 
                    ON store.store_id = store_order_product.store_id
            WHERE
                store_order_product.code = :productCode
                AND
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
            ORDER BY date
            '
        );

        $st->bindValue(':productCode', $productCode);
        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order !');
        }
        
        if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
            $total = 0;
            $forReturn = array();
            foreach ($res as $row) {
                if (! isset($forReturn[$row->external_store_id])) {
                    $forReturn[$row->external_store_id] = 0;
                }

                $forReturn[$row->external_store_id] += (float) $row->price;
            }
            
            return $forReturn;
        }

        return null;
    }

    public function getGloabalValuesForProvider($providerId, $from, $to)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                price ,
                date,
                store.external_store_id
            FROM
                store_order_product 
                JOIN store 
                    ON store.store_id = store_order_product.store_id
            WHERE
                store_order_product.provider_id = :providerId
                AND
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
            ORDER BY date
            '
        );

        $st->bindValue(':providerId', $providerId);
        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order !');
        }
        
        if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
            $total = 0;
            $forReturn = array();
            foreach ($res as $row) {
                if (! isset($forReturn[$row->external_store_id])) {
                    $forReturn[$row->external_store_id] = 0;
                }

                $forReturn[$row->external_store_id] += (float) $row->price;
            }
            return $forReturn;
        }

        return null;
    }

    public function getGloabalValuesForCategory($categoryId, $from, $to)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                price,
                date,
                store.external_store_id
            FROM
                store_order_product 
                JOIN store 
                    ON store.store_id = store_order_product.store_id
            WHERE
                store_order_product.category_id = :categoryId
                AND
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
            ORDER BY date
            '
        );

        $st->bindValue(':categoryId', $categoryId);
        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order !');
        }
        
        if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
            $total = 0;
            $forReturn = array();
            foreach ($res as $row) {
                if (! isset($forReturn[$row->external_store_id])) {
                    $forReturn[$row->external_store_id] = 0;
                }

                $forReturn[$row->external_store_id] += (float) $row->price;
            }
            
            return $forReturn;
        }

        return null;
    }

    public function getStockValuesForProvider($providerId, $date, $forProdutIds = [])
    {
        //escape the strings
        foreach ($forProdutIds as &$id) {
            $id = (int) $id;
        }

        $st = $this->dbConn->prepare(
            'SELECT 
                product.price,
                quantity,
                (SELECT store.external_store_id FROM store WHERE store.store_id = product_stock_situation.store_id LIMIT 1) AS external_store_id
            FROM
                product 
                JOIN product_stock_situation
                    ON product.product_id = product_stock_situation.product_id
            WHERE
                product.provider_id = :providerId
                AND
                product_stock_situation.date = :date
                '. (count($forProdutIds) > 0 ? (' AND product.product_id IN  (' . implode(',', $forProdutIds) .')') : '') .'
            '
        );

        $st->bindValue(':providerId', $providerId);
        $st->bindValue(':date', $date->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order !');
        }
        
        if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
            $quantities = array();
            $fullPrice = array();
            foreach ($res as $row) {
                if (! isset($quantities[$row->external_store_id])) {
                    $quantities[$row->external_store_id] = 0;
                    $fullPrice[$row->external_store_id] = 0;
                }

                $quantities[$row->external_store_id] += (int) $row->quantity;
                $fullPrice[$row->external_store_id] += (int) $row->quantity * (float) $row->price;
            }
            
            return array(
                'fullPrice' => $fullPrice,
                'quantities' => $quantities,
            );
        }

        return null;
    }

    public function getStockValuesForCategory($categoryId, $date, $forProdutIds = [])
    {
        //escape the strings
        foreach ($forProdutIds as &$id) {
            $id = (int) $id;
        }

        $st = $this->dbConn->prepare(
            'SELECT 
                product.price,
                quantity,
                (SELECT store.external_store_id FROM store WHERE store.store_id = product_stock_situation.store_id LIMIT 1) AS external_store_id
            FROM
                product 
                JOIN product_stock_situation
                    ON product.product_id = product_stock_situation.product_id
            WHERE
                product.category_id = :categoryId
                AND
                product_stock_situation.date = :date
                '. (count($forProdutIds) > 0 ? (' AND product.product_id IN  (' . implode(',', $forProdutIds) .')') : '') .'
            '
        );

        $st->bindValue(':categoryId', $categoryId);
        $st->bindValue(':date', $date->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order !');
        }
        
        if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
            $quantities = array();
            $fullPrice = array();
            foreach ($res as $row) {
                if (! isset($quantities[$row->external_store_id])) {
                    $quantities[$row->external_store_id] = 0;
                    $fullPrice[$row->external_store_id] = 0;
                }

                $quantities[$row->external_store_id] += (int) $row->quantity;
                $fullPrice[$row->external_store_id] += (int) $row->quantity * (float) $row->price;
            }
            
            return array(
                'fullPrice' => $fullPrice,
                'quantities' => $quantities,
            );
        }

        return null;
    }

   
    public function createOrder($orderExternalId)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                store_order 
            (store_order_external_id)
            VALUES
            (:orderExternalId)
            '
        );


        $st->bindValue(':orderExternalId', $orderExternalId);

        if (!$st->execute()) {
            throw new \Exception('Error when creating order !');
        }

        return $this->dbConn->lastInsertId();
    }


    public function getCategory($categoryName)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                category_id 
            FROM
                categories
            WHERE
                name = :categoryName 
            LIMIT 1
            '
        );

        $st->bindValue(':categoryName', $categoryName);
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting category !');
        }

        if ($res = $st->fetchObject()) {
            return $res->category_id;
        }

        return null;
    }

    public function getSeller($sellerName)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                seller_id
            FROM
                seller
            WHERE
                name = :sellerName 
            LIMIT 1
            '
        );

        $st->bindValue(':sellerName', $sellerName);
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting category !');
        }

        if ($res = $st->fetchObject()) {
            return $res->seller_id;
        }

        return null;
    }

    public function createSeller($sellerName, $sellerExternalId)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                seller
            (name, external_id)
            VALUES
            (:sellerName, :sellerExternalId)
            '
        );

        $st->bindValue(':sellerName', $sellerName);
        $st->bindValue(':sellerExternalId', $sellerExternalId);
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting seller !');
        }

        return $this->dbConn->lastInsertId();
    }

    public function createCategory($categoryName)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                categories
            (name)
            VALUES 
            (:categoryName)
            '
        );

        $st->bindValue(':categoryName', $categoryName);
        
        if (!$st->execute()) {
            throw new \Exception('Error when creating category !');
        }

        return $this->dbConn->lastInsertId();
    }

    public function createProvider($code)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                provider
            (code)
            VALUES 
            (:code)
            '
        );

        $st->bindValue(':code', $code);
        
        if (!$st->execute()) {
            throw new \Exception('Error when creating provider !');
        }

        return $this->dbConn->lastInsertId();
    }

    public function getProvider($code)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                provider_id 
            FROM
                provider
            WHERE
                code = :code 
            LIMIT 1
            '
        );

        $st->bindValue(':code', $code);
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting product of order !');
        }

        if ($res = $st->fetchObject()) {
            return $res->provider_id;
        }

        return null;
    }

    public function getProduct($codeOfProduct, $articul, $size, $color)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                product_id,
                articul,
                code,
                color,
                size,
                category_id,
                provider_id
            FROM
                product
            WHERE
                articul = :articul
                AND
                code = :code 
                AND
                color = :color 
                AND
                size = :size
            LIMIT 1
            '
        );

        $st->bindValue(':articul', $articul);
        $st->bindValue(':code', $codeOfProduct);
        $st->bindValue(':color', $color);
        $st->bindValue(':size', $size);
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting product !');
        }

        if ($res = $st->fetchObject()) {
            return $res;
        }

        return null;
    }

    public function getOrderProduct($orderId, $codeOfProduct, $size, $color, $date)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
            store_order_product_id,
                    store_order_id,
                       provider_id,
                       category_id,
                              code,
                           articul,
                             color,
                              size,
                             price,
                    price_with_tax,
                 discount_with_tax,
                          quantity,
                              date,
                         seller_id,
                          store_id,
                        store_code
            FROM
                store_order_product
            WHERE
                store_order_id = :orderId
                AND
                code = :code 
                AND
                color = :color 
                AND
                size = :size
                AND
                date = :date
            LIMIT 1
            '
        );

        $st->bindValue(':orderId', $orderId);
        $st->bindValue(':code', $codeOfProduct);
        $st->bindValue(':color', $color);
        $st->bindValue(':size', $size);
        $st->bindValue(':date', $date->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting category !');
        }

        if ($res = $st->fetchObject()) {
            return $res;
        }

        return null;
    }

    public function getLastOrderProductByParams($codeOfProduct, $size, $color, $quantity = null)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
            store_order_product_id,
                    store_order_id,
                       provider_id,
                       category_id,
                              code,
                           articul,
                             color,
                              size,
                             price,
                    price_with_tax,
                 discount_with_tax,
                          quantity,
                              date,
                         seller_id,
                          store_id,
                        store_code
            FROM
                store_order_product
            WHERE
                code = :code 
                AND
                color = :color 
                AND
                size = :size
                '. ($quantity !== null ? ' AND
                quantity = :quantity ' : '') . '
            ORDER BY 
                date DESC
            LIMIT 1
            '
        );

        $st->bindValue(':code', $codeOfProduct);
        $st->bindValue(':color', $color);
        $st->bindValue(':size', $size);
        if ($quantity !== null) {
            $st->bindValue(':quantity', $quantity);
        }
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting category !');
        }

        if ($res = $st->fetchObject()) {
            return $res;
        }

        return null;
    }

    public function getOrderProductInfo($orderProductId)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                * 
            FROM
                store_order_product
            WHERE
                store_order_product_id = :orderProductId
            LIMIT 1
            '
        );

        $st->bindValue(':orderProductId', $orderProductId);
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting category !');
        }

        if ($res = $st->fetchObject()) {
            return $res;
        }

        return null;
    }

    private function getProductsForUpdating()
    {
        $now = new \DateTime();
        $now->modify('-3 day');

        $st = $this->dbConn->prepare(
            'SELECT 
                product_id,
                store_order_product.category_id
            FROM
                product
                JOIN
                    store_order_product ON 
                        product.code = store_order_product.code
                        AND
                        product.articul = store_order_product.articul
                        AND
                        product.color = store_order_product.color
                        AND
                        product.size = store_order_product.size
            WHERE
                store_order_product.date > "' . $now->format('Y-m-d') . ' 00:00:00"'
        );

        
        if (!$st->execute()) {
            throw new \Exception('Error when getting category !');
        }

        if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
            return $res;
        }

        return [];
    }

    public function autoUpdateCategoriesForProducts()
    {
        $res = $this->getProductsForUpdating();

        if (count($res) > 0) {
            foreach ($res as $row) {
                if (! $this->setCategoryForProduct($row->product_id, $row->category_id)) {
                    throw new \Exception('Cant update category of product !');
                }
            }
        }

        return true;
    }

    public function setCategoryForProduct($productId, $categoryId)
    {
        $st = $this->dbConn->prepare(
            'UPDATE
                product
            SET
                category_id = :categoryId
            WHERE
                product_id = :productId
            LIMIT 1 
            '
        );

        $st->bindValue(':productId', $productId);
        $st->bindValue(':categoryId', $categoryId);

        return $st->execute();
    }

    public function setPriceForProduct($productId, $price)
    {
        $st = $this->dbConn->prepare(
            'UPDATE
                product
            SET
                price = :price
            WHERE
                product_id = :productId
            LIMIT 1 
            '
        );

        $st->bindValue(':productId', $productId);
        $st->bindValue(':price', $price);

        return $st->execute();
    }

    public function createOrderProduct($orderId, $params)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                store_order_product
            (store_order_id, code, color, size, price, price_with_tax, 
                discount_with_tax, quantity, articul, category_id, provider_id, date, seller_id, store_id, store_code)
            VALUES 
            (:orderId, :code, :color, :size, :price, :price_with_tax, :discount_with_tax, 
                :quantity, :articul, :categoryId, :providerId, :date, :sellerId, :storeId, :codeOfStore)
            '
        );

        $st->bindValue(':orderId', $orderId);
        $st->bindValue(':code', $params['code']);
        $st->bindValue(':color', $params['color']);
        $st->bindValue(':size', $params['size']);
        $st->bindValue(':providerId', $params['providerId']);
        $st->bindValue(':categoryId', $params['categoryId']);
        $st->bindValue(':articul', $params['articul']);
        $st->bindValue(':price', $params['price']);
        $st->bindValue(':price_with_tax', $params['priceWithTax']);
        $st->bindValue(':discount_with_tax', $params['discountWithTax']);
        $st->bindValue(':quantity', $params['quantity']);
        $st->bindValue(':date', $params['date']->format('Y-m-d H:i:s'));
        $st->bindValue(':sellerId', $params['sellerId']);
        $st->bindValue(':storeId', $params['storeId']);
        $st->bindValue(':codeOfStore', $params['codeOfStore']);
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order product !');
        }

        return $this->dbConn->lastInsertId();
    }

    public function createProduct($params)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                product
            (code, color, size, articul, provider_id)
            VALUES 
            (:code, :color, :size, :articul, :providerId)
            '
        );

        $st->bindValue(':code', $params['code']);
        $st->bindValue(':color', $params['color']);
        $st->bindValue(':size', $params['size']);
        $st->bindValue(':providerId', $params['providerId']);
        $st->bindValue(':articul', $params['articul']);
        
        if (!$st->execute()) {
            throw new \Exception('Error when occurs creation of product !');
        }

        return $this->dbConn->lastInsertId();
    }

    public function saveProductStockSituation($productId, $storeId, $quantity, $date)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                product_stock_situation
            (product_id, store_id, quantity, date)
            VALUES 
            (:productId, :storeId, :quantity, :date)
            '
        );

        $st->bindValue(':productId', $productId);
        $st->bindValue(':storeId', $storeId);
        $st->bindValue(':quantity', $quantity);
        $st->bindValue(':date', $date->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when occurs creation of stock Info !');
        }

        return true;
    }

    public function saveProductNeedSituation($productId, $storeId, $quantity, $date)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                product_which_need_situation
            (product_id, store_id, quantity, date)
            VALUES 
            (:productId, :storeId, :quantity, :date)
            '
        );

        $st->bindValue(':productId', $productId);
        $st->bindValue(':storeId', $storeId);
        $st->bindValue(':quantity', $quantity);
        $st->bindValue(':date', $date->format('Y-m-d H:i:s'));
        
        if (!$st->execute()) {
            throw new \Exception('Error when occurs creation of stock Info !');
        }

        return true;
    }

    public function updateOrderProduct($storeOrderProductId, $params)
    {
        $st = $this->dbConn->prepare(
            'UPDATE
                store_order_product
            SET
                price = :price,                
                price_with_tax = :price_with_tax, 
                discount_with_tax = :price_with_tax, 
                quantity = :quantity
            WHERE
                store_order_product_id = :id
            LIMIT 1 
            '
        );

        $st->bindValue(':price', $params['price']);
        $st->bindValue(':price_with_tax', $params['priceWithTax']);
        $st->bindValue(':discount_with_tax', $params['discountWithTax']);
        $st->bindValue(':quantity', $params['quantity']);
        $st->bindValue(':id', $storeOrderProductId);

        return $st->execute();
    }
    /************************************************/
    /************************************************/
    /************************************************/

    public function saveOrderProduct($params, $withUpdatingProduct = false)
    {
        $codeOfStore = $params['codeOfStore'];
        $articul = $params['articul'];
        $date = $params['date'];
        $price = $params['price'];
        $quantity = $params['quantity'];
        $orderExternalId = $params['orderId'];
        $categoryName = $params['categoryName'];
        
        $selleName = $params['selleName'];
        $sellerExternalId = $params['selleExternalId'];
        $providerCode = $params['providerCode'];
        $productCode = $params['productCode'];
        $color = $params['color'];
        $size = $params['size'];
        $priceWithTax = $params['priceWithTax'];
        $discountWithTax = $params['discountWithTax'];
        
        //temporary fix for skipping
        if ($codeOfStore === 'QUILMES1') {
            return true;
        }

        $storeId = $this->getStoreIdByCode($codeOfStore);
        if ($storeId === null) {
            throw new \Exception('Cant find store !');
        }

        $sellerId = $this->getSeller($selleName);
        if ($sellerId === null) {
            $sellerId = $this->createSeller($selleName, $sellerExternalId);
        }

        $orderId = $this->getOrder($orderExternalId);
        if ($orderId === null) {
            $orderId = $this->createOrder($orderExternalId);
        }

        
        $categoryId = $this->getCategory($categoryName);
        if ($categoryId == null) {
            $categoryId = $this->createCategory($categoryName);
        }


        $providerId = $this->getProvider($providerCode);
        if ($providerId === null) {
            $providerId = $this->createProvider($providerCode);
        }

        $orderProductInfo = $this->getOrderProduct($orderId, $productCode, $size, $color, $date);
        if ($orderProductInfo === null) {
            if (!$this->createOrderProduct($orderId, array(
                'code' => $productCode,
                'articul' => $articul,
                'providerId' => $providerId,
                'categoryId' => $categoryId,
                'color' => $color,
                'size' => $size,
                'price' => $price,
                'priceWithTax' => $priceWithTax,
                'discountWithTax' => $discountWithTax,
                'sellerId' => $sellerId,
                'storeId' => $storeId,
                'quantity' => $quantity,
                'codeOfStore' => $codeOfStore,
                'date' => $date,
            ))) {
                return false;
            }
        } else if ($withUpdatingProduct) {

            $price += (float) $orderProductInfo->price;
            $priceWithTax += (float) $orderProductInfo->price_with_tax;
            $discountWithTax += (float) $orderProductInfo->discount_with_tax;
            $quantity += (int) $orderProductInfo->quantity;

            if (! $this->updateOrderProduct($orderProductInfo->store_order_product_id, array(
                'price' => $price,
                'priceWithTax' => $priceWithTax,
                'discountWithTax' => $discountWithTax,
                'quantity' => $quantity,
            ))) {
                throw new \Exception('Cant update product !');
            }
        }
        
        return true;
    }

    public function saveProduct($params)
    {
        $articul = $params['articul'];
        $providerCode = $params['providerCode'];
        $productCode = $params['productCode'];
        $color = $params['color'];
        $size = $params['size'];
        
        $providerId = $this->getProvider($providerCode);
        if ($providerId === null) {
            $providerId = $this->createProvider($providerCode);
        }
        
        $productInfo = $this->getProduct($productCode, $articul, $size, $color);
        if ($productInfo === null) {
            $productId = $this->createProduct(array(
                'code' => $productCode,
                'articul' => $articul,
                'providerId' => $providerId,
                'color' => $color,
                'size' => $size,
            ));

            if (!$productId) {
                return false;
            }

            return $productId;
        } 
        
        return $productInfo->product_id;
    }

    public function checkForFilaWithAllInfoAndDB($fileName)
    {
        $rowNum = 0;
        $info = array();
        $file = array();
        $priceAll = 0;
        $priceAll2 = 0;
        if (($handle = fopen($fileName, 'r')) !== false) {
            while($data = fgetcsv($handle, 0, ',')) {
                $rowNum++;
                if ($rowNum > 1) {
                    $orderId = trim($data[1]);

                    if ($orderId === '') {
                        continue;
                    }

                    $codeOfStore = trim($data[13]);
                    /*
                    $storeId = explode('-' ,$orderId);
                    if (count($storeId) === 2) {
                        $orderPartRealId = trim($storeId[1]);
                        //getting part which need to converting to real code of store
                        $storeId = trim($storeId[0]);
                        preg_match("/[^\d]*?(\d+)$/", $storeId, $storeIdRes);
                        $storeId = $storeIdRes[1];
                        $code = $storeId;
                    } else {
                        continue;
                    }
                    */
                    
                    $date = trim($data[0]);
                    $selleName = trim($data[2]);
                    $selleExternalId = trim($data[3]);
                    $productCode = trim($data[4]);
                    $providerCode = mb_substr($productCode, 0, 3, 'UTF-8');
                    $articul = trim($data[5]);
                    $category = trim($data[6]);
                    $quantity = (int) $data[7];
                    $priceWithTax = (float) $data[8];
                    $discountWithTax = (float) $data[9];

                    $price = self::prepareDecimal($data[10]);

                    $color = trim($data[11]);
                    $size = trim($data[12]);
                    
                    $orderExternalId = $orderId;
                    $orderId = $this->getOrder($orderExternalId);
                    
                    if ($orderId === null) {
                        continue;
                    }
                    
                    $date = \DateTime::createFromFormat('d/m/Y H:i:s', $date. ' 15:00:00');
                    $orderProductId = $this->getOrderProduct($orderId, $productCode, $size, $color, $date);
                    
                    if ($orderProductId !== null) {
                        $info = $this->getOrderProductInfo($orderProductId);
                        $priceAll += (float) $info->price;
                        $priceAll2 += (float) $price;
                        if ((float) $info->price !== (float) $price) {
                            echo '<pre>';
                            var_dump($info);
                            echo '</pre>';
                        }
                    } else {
                        echo '=================================';
                        var_dump( array($orderId, $productCode, $size, $color, $date));
                    }

                } else if (count($data) !== 14) {
                    return false;
                }
            }
        }
        

                            var_dump($priceAll);
                            var_dump($priceAll2);
        return true;
    }

    public function deleteProducts($from, $to)
    {
        $st = $this->dbConn->prepare(
            'DELETE FROM 
                store_order_product
            WHERE
                date >= :from
                AND
                date <= :to
            '
        );

        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));
        
        return $st->execute();
    }    
    
    public function saveInforFromFileWithFullProductDescription($fileName, $withDeletionBefore = false)
    {
        $rowNum = 0;
        $info = array();
        $file = array();
        
        $from = null;
        $to = null;
        if ($withDeletionBefore) {
            //getting of time range for cleaning
            if (($handle = fopen($fileName, 'r')) !== false) {
                while($data = fgetcsv($handle, 0, ',')) {
                    $date = trim($data[0]);
                    $date = \DateTime::createFromFormat('d/m/Y H:i:s', $date. ' 00:00:00');

                    if (!$date) {
                        continue;
                    }

                    if ($from === null || $from > $date) {
                        $from = $date;
                    }


                    if ($to === null || $to < $date) {
                        $to = $date;
                    }


                }
            }

            $to = new \DateTime($to->format('Y-m-d') . ' 23:59:59');
            if (!$this->deleteProducts($from, $to)) {
                throw new \Exception('Cant delete products !');
            }
        }
        
        if (($handle = fopen($fileName, 'r')) !== false) {
            while($data = fgetcsv($handle, 0, ',')) {
                $rowNum++;
                if ($rowNum > 1) {
                    $orderId = trim($data[1]);

                    if ($orderId === '') {
                        continue;
                    }

                    $codeOfStore = trim($data[13]);
                    
                    $date = trim($data[0]);
                    $selleName = self::prepareString(trim($data[2]));
                    $selleExternalId = trim($data[3]);
                    $productCode = trim($data[4]);
                    $providerCode = self::getProviderCodeFromProductCode($productCode);
                    $articul = self::prepareString(trim($data[5]));
                    $category = self::prepareString(trim($data[6]));
                    $quantity = (int) $data[7];
                    $priceWithTax = self::prepareDecimal($data[8]);
                    $discountWithTax = self::prepareDecimal($data[9]);
                    $price = self::prepareDecimal($data[10]);

                    $color = trim($data[11]);
                    $size = trim($data[12]);
                    
                    $prodPerapared = array(
                        'orderId' => $orderId,
                        'codeOfStore' => $codeOfStore,
                        'articul' => $articul,
                        'quantity' => $quantity,
                        'price' => $price,
                        'categoryName' => $category,
                        'selleName' => $selleName,
                        'selleExternalId' => $selleExternalId,
                        'providerCode' => $providerCode,
                        'color' => $color,
                        'size' => $size,
                        'productCode' => $productCode,
                        'priceWithTax' => $priceWithTax,
                        'discountWithTax' => $discountWithTax,
                        'date' => \DateTime::createFromFormat('d/m/Y H:i:s', $date. ' 15:00:00'),
                    );
                    
                    if ($withDeletionBefore) {
                        if (!$this->saveOrderProduct($prodPerapared, true)) {
                            return false;
                        }
                    } else {
                        $info[] = $prodPerapared;
                    }

                    /*
                    Information for generation of fixed variant of file
                    $file[] = $data;
                    */


                } else if (count($data) !== 14) {
                    return false;
                }
            }
        }
        
        /*
        For generation of fixed variant of file 
        $preparedInfo = array();
        foreach ($file as $f) {
            $orderId = trim($f[1]);
            $color = trim($f[11]);
            $size = trim($f[12]);
            $productCode = trim($f[4]);
            $date = trim($f[0]);

            $key = $date . $orderId . $color . $size . $productCode;
            if (isset($preparedInfo[$key])) {
                $preparedInfo[$key][7] += (int) $f[7]; 
                $preparedInfo[$key][8] += self::prepareDecimal($f[8]); 
                $preparedInfo[$key][9] += self::prepareDecimal($f[9]); 
                $preparedInfo[$key][10] += self::prepareDecimal($f[10]); 
            } else {
                $f[7] = (int) $f[7];

                $f[8] = self::prepareDecimal($f[8]);
                $f[9] = self::prepareDecimal($f[9]);
                $f[10] = self::prepareDecimal($f[10]);

                $preparedInfo[$key] = $f; 
            }
        }

        $firstLine = array();
        for ($i = 0; $i < count($preparedInfo[$key]); $i++) {
            $firstLine[] = '-';
        }

        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="sample.csv"');
        $fp = fopen('php://output', 'w');

        
        fputcsv($fp, $firstLine, ',');
        foreach ($preparedInfo as $line) {
            // though CSV stands for "comma separated value"
            // in many countries (including France) separator is ";"
            fputcsv($fp, $line, ',');
        }
        fclose($fp);
        die;
        */


        if (!$withDeletionBefore) {
            $preparedInfo = array();
            foreach ($info as $prod) {
                $key = $prod['date']->format('Y-m-d H:i:s') . $prod['orderId'] . $prod['color'] . $prod['size'] . $prod['productCode'];
                
                if (isset($preparedInfo[$key])) {
                    $preparedInfo[$key]['quantity'] += $prod['quantity']; 
                    $preparedInfo[$key]['price'] += $prod['price']; 
                    $preparedInfo[$key]['discountWithTax'] += $prod['discountWithTax']; 
                    $preparedInfo[$key]['priceWithTax'] += $prod['priceWithTax']; 
                } else {
                    $preparedInfo[$key] = $prod; 
                }
            }
            
            $info = null;
            foreach ($preparedInfo as $prodPerapared) {
                if (!$this->saveOrderProduct($prodPerapared)) {
                    return false;
                }
            }
        }

        return true;
    }

    public function saveProductPricesFromFile($fileName)
    {
        $rowNum = 0;
        $captionsOfFile = array();
        if (($handle = fopen($fileName, 'r')) !== false) {
            while($data = fgetcsv($handle, 0, ',')) {
                $rowNum++;
                if ($rowNum > 1) {

                    $articul = self::prepareString(trim($data[2]));
                    $parts = explode(' ', $articul, 2);

                    $productCode = trim($parts[0]);
                    $articul = '';
                    if (isset($parts[1])) {
                        $articul = trim($parts[1]);
                    }

                    $color = self::prepareString(trim($data[3]));
                    $size = self::prepareString(trim($data[4]));
                    $price = (float) trim($data[6]);

                    $productInfo = $this->getProduct($productCode, $articul, $size, $color);

                    if ($productInfo !== null) {
                        if (!$this->setPriceForProduct($productInfo->product_id, $price)) {
                            return false;
                        }
                    }
                } else if (count($data) !== 7) {
                    return false;
                } else {
                    $captionsOfFile = $data;
                }
            }
        }
        

        return true;
    }

    public function saveProductNeedInformationFromFile($fileName, $withDeletionBefore = false)
    {
        $rowNum = 0;
        $captionsOfFile = array();
        $date = new \DateTime();
        if (($handle = fopen($fileName, 'r')) !== false) {
            while($data = fgetcsv($handle, 0, ',')) {
                $rowNum++;
                if ($rowNum > 1) {
                    $productCode = self::prepareString(trim($data[0]));
                    $color = self::prepareString(trim($data[2]));
                    $size = self::prepareString(trim($data[3]));
                    $providerCode = self::getProviderCodeFromProductCode($productCode);

                    $articul = self::prepareString(trim($data[1]));

                    
                    $prodPerapared = array(
                        'articul' => $articul,
                        'providerCode' => $providerCode,
                        'color' => $color,
                        'size' => $size,
                        'productCode' => $productCode,
                    );
                    
                    $productId = $this->saveProduct($prodPerapared);
                    if (!$productId) {
                        return false;
                    }
                    
                    $startColumnIwthStock = 4;
                    $needInfo = array();
                    for ($i = $startColumnIwthStock; $i < count($captionsOfFile); $i++) {
                        //temporary fix for skipping
                        if ($captionsOfFile[$i] === 'QUILMES1') {
                            continue;
                        }

                        $storeId = $this->getStoreIdByCode($captionsOfFile[$i]);
                        $quantity = trim($data[$i]);
                        if ($quantity === '') {
                            $quantity = 0;
                        }

                        $quantity = (int) $quantity;
                        
                        if (!isset($needInfo[$storeId])) {
                            $needInfo[$storeId] = array(
                                'storeId' => $storeId,
                                'quantity' => 0
                            );
                        }

                        $needInfo[$storeId]['quantity'] += $quantity;
                    } 
                    
                    foreach ($needInfo as $info) {
                        if (!$this->saveProductNeedSituation($productId, $info['storeId'], $info['quantity'], $date)) {
                            throw new \Exception('Cant save stock sitation');
                        }
                    }

                } else if (count($data) <= 4) {
                    return false;
                } else {
                    $captionsOfFile = $data;
                }
            }
        }
        

        return true;
    }


    public function saveStockInformationFromFile($fileName, $withDeletionBefore = false)
    {
        $rowNum = 0;
        $captionsOfFile = array();
        $date = new \DateTime();
        if (($handle = fopen($fileName, 'r')) !== false) {
            while($data = fgetcsv($handle, 0, ',')) {
                $rowNum++;
                if ($rowNum > 1) {
                    

                    $articul = self::prepareString(trim($data[2]));
                    $productCode = trim($data[1]);
                    $providerCode = self::getProviderCodeFromProductCode($productCode);

                    $colorId = trim($data[3]);
                    $colorName = self::prepareString(trim($data[4]));

                    $color = $colorId; 
                    if ($colorName !== '') {
                        $color .= ' ' . $colorName;
                    }

                    $sizeId = trim($data[5]);
                    $sizeName = self::prepareString(trim($data[6]));

                    $size = $sizeId;
                    if ($sizeName !== '') {
                        $size .= ' ' . $sizeName;
                    }
                    
                    $prodPerapared = array(
                        'articul' => $articul,
                        'providerCode' => $providerCode,
                        'color' => $color,
                        'size' => $size,
                        'productCode' => $productCode,
                    );
                    
                    $productId = $this->saveProduct($prodPerapared);
                    if (!$productId) {
                        return false;
                    }
                    
                    $startColumnIwthStock = 7;
                    $stockInfo = array();
                    for ($i = $startColumnIwthStock; $i < count($captionsOfFile); $i++) {
                        if ($captionsOfFile[$i] === 'QUILMES1') {
                            continue;
                        }

                        $storeId = $this->getStoreIdByCode($captionsOfFile[$i]);
                        $quantity = trim($data[$i]);
                        if ($quantity === '') {
                            $quantity = 0;
                        }

                        $quantity = (int) $quantity;
                        
                        if (!isset($stockInfo[$storeId])) {
                            $stockInfo[$storeId] = array(
                                'storeId' => $storeId,
                                'quantity' => 0
                            );
                        }

                        $stockInfo[$storeId]['quantity'] += $quantity;
                    } 
                    
                    foreach ($stockInfo as $info) {
                        if (!$this->saveProductStockSituation($productId, $info['storeId'], $info['quantity'], $date)) {
                            throw new \Exception('Cant save stock sitation');
                        }
                    }

                } else if (count($data) <= 7) {
                    return false;
                } else {
                    $captionsOfFile = $data;
                }
            }
        }
        

        return true;
    }
    
    public function getOrderForSettingPaymentType($orderExternalId)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                store_order_id 
            FROM
                store_order
            WHERE
                store_order_external_id = :orderExternalId
                AND
                type_of_money_id = 0 
            LIMIT 1
            '
        );

        $st->bindValue(':orderExternalId', $orderExternalId);
        
        if (!$st->execute()) {
            throw new \Exception('Error when getting order !');
        }

        if ($res = $st->fetchObject()) {
            return $res->store_order_id;
        }

        return null;
    }

    private function getTypeOfPaymentByName($nameOfPayment)
    {
        $st = $this->dbConn->prepare(
            'SELECT
                type_of_money_id
            FROM
                types_of_money
            WHERE
                name LIKE :name
            LIMIT 1'
        );

        if (!$st->execute(array(
            'name' => $nameOfPayment,
        ))) {
            throw new \Exception('Cant get type of money !');
        }

        if ($res = $st->fetchObject()) {
            return $res->type_of_money_id;
        }

        return null;

    }
    
    private function saveTypeOfPayment($nameOfPayment)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                types_of_money
            (name)
            VALUES
            (:nameOfPayment)'
        );


        if (! $st->execute(array(
            'nameOfPayment' => $nameOfPayment,
        ))) {
            throw new \Exception('Cant save type of payment !');
        }

        return $this->dbConn->lastInsertId();
    }

    private function saveTypeOfPaymentForOrder($orderId, $typeMoneyId)
    {
        $st = $this->dbConn->prepare(
            'UPDATE
                store_order
            SET
                type_of_money_id = :typeMoneyId
            WHERE
                store_order_id = :orderId
            LIMIT 1'
        );


        return $st->execute(array(
            'orderId' => $orderId,
            'typeMoneyId' => $typeMoneyId,
        ));
    }

    public function saveInfoFromFileWithTypeOfPayment($fileName)
    {
        $rowNum = 0;
        $res = array();
        if (($handle = fopen($fileName, 'r')) !== false) {
            while($data = fgetcsv($handle, 0, ',')) {
                $rowNum++;
                if ($rowNum > 1) {

                    $typeOfMoney = trim($data[0]);

                    if ($typeOfMoney === '') {
                        continue;
                    }

                    $orderExternalId = trim($data[3]);

                    if ($orderExternalId === '') {
                        continue;
                    }

                    $orderId = $this->getOrderForSettingPaymentType($orderExternalId);
                    if ($orderId === null) {
                        continue;
                    }

                    $typeMoneyId = $this->getTypeOfPaymentByName($typeOfMoney);
                    if ($typeMoneyId === null) {
                        $typeMoneyId = $this->saveTypeOfPayment($typeOfMoney);
                        if ($typeMoneyId === null) {
                            return false;
                        }
                    }
                    
                    if ($orderId !== null) {
                        if (!$this->saveTypeOfPaymentForOrder($orderId, $typeMoneyId)) {
                            return false;
                        }
                    }

                } else if (count($data) !== 5) {
                    return false;
                }
            }
        }

        return true;
    }
    


    public function getInfoForStatisticByStores($from, $to, $additionalParams =array()) 
    {        
        $filterSqlPart = self::generateFiltrationSqIfCan($additionalParams);

        $st = $this->dbConn->prepare(
            'SELECT 
                DISTINCT store_order_product.store_id,
                store.external_store_id AS storeCode
             FROM 
                store_order_product 
                JOIN
                    store ON store.store_id = store_order_product.store_id
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
            ' . $filterSqlPart
        );

        if (!$st->execute(array(
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s'),
        ))) {
            throw new \Exception('Can\'t get stores');
        }

        $stores = $st->fetchAll(\PDO::FETCH_OBJ);

        ////////////////////////////////////
        $st = $this->dbConn->prepare(
            'SELECT 
                COUNT(DISTINCT store_order_product.store_order_id) AS quantityOfOrders
             FROM 
                store_order_product
            WHERE 
                store_order_product.store_id = :storeId
                AND
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
                '
        );

        foreach ($stores as &$j) {
            if (!$st->execute(array(
                'storeId' => $j->store_id,
                'from' => $from->format('Y-m-d H:i:s'),
                'to' => $to->format('Y-m-d H:i:s'),
            ))) {
                throw new \Exception('Can\'t get stores qunatities !');
            }

            $res = $st->fetchObject();
            $j->quantityOfOrders = $res->quantityOfOrders;
        }


        ////////////////////////////////////
        $st = $this->dbConn->prepare(
            'SELECT 
                store_order_product.price,
                store_order_product.quantity
             FROM 
                store_order_product
            WHERE 
                store_order_product.store_id = :storeId
                AND
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
                '
        );

        foreach ($stores as &$s) {
            if (!$st->execute(array(
                'storeId' => $s->store_id,
                'from' => $from->format('Y-m-d H:i:s'),
                'to' => $to->format('Y-m-d H:i:s'),
            ))) {
                throw new \Exception('Can\'t get info !');
            }

            $info = $st->fetchAll(\PDO::FETCH_OBJ);
            
            $sumPrice = 0;
            $sumQuantity = 0;
            foreach ($info as $i) {
                $sumPrice += (float) $i->price;
                $sumQuantity += (int) $i->quantity;
            }

            $s->price = $sumPrice;
            $s->quantity = $sumQuantity;
            $s->quantity_div_quantityOfOrders = round((int) $s->quantity / (int) $s->quantityOfOrders, 2);
            $s->price_div_quantityOfOrders = round((float) $s->price / (int) $s->quantityOfOrders, 2);
        }

        return $stores;
    }

    public function getInfoForStatisticBySellers($from, $to, $additionalParams = array()) 
    {
        $filterSqlPart = self::generateFiltrationSqIfCan($additionalParams);

        $st = $this->dbConn->prepare(
            'SELECT 
                DISTINCT store_order_product.seller_id,
                seller.name AS seller
             FROM 
                store_order_product
                JOIN
                    seller ON seller.seller_id = store_order_product.seller_id
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
            ' . $filterSqlPart
        );

        if (!$st->execute(array(
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s'),
        ))) {
            throw new \Exception('Can\'t get sellers !');
        }

        $sellers = $st->fetchAll(\PDO::FETCH_OBJ);
        ////////////////////////////////////
        $st = $this->dbConn->prepare(
            'SELECT 
                COUNT(DISTINCT store_order_product.store_order_id) AS quantityOfOrders
             FROM 
                store_order_product
            WHERE 
                store_order_product.seller_id = :sellerId
                AND
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
                '
        );

        foreach ($sellers as &$j) {
            if (!$st->execute(array(
                'sellerId' => $j->seller_id,
                'from' => $from->format('Y-m-d H:i:s'),
                'to' => $to->format('Y-m-d H:i:s'),
            ))) {
                throw new \Exception('Can\'t get sellers qunatities !');
            }

            $res = $st->fetchObject();
            $j->quantityOfOrders = $res->quantityOfOrders;
        }

        ////////////////////////////////////
        $st = $this->dbConn->prepare(
            'SELECT 
                store_order_product.price,
                store_order_product.quantity
             FROM 
                store_order_product
            WHERE 
                store_order_product.seller_id = :sellerId
                AND
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
                '
        );
      
        foreach ($sellers as &$s) {
            if (!$st->execute(array(
                'sellerId' => $s->seller_id,
                'from' => $from->format('Y-m-d H:i:s'),
                'to' => $to->format('Y-m-d H:i:s'),
            ))) {
                throw new \Exception('Can\'t get info !');
            }

            $info = $st->fetchAll(\PDO::FETCH_OBJ);
            
            $sumPrice = 0;
            $sumQuantity = 0;
            foreach ($info as $i) {
                $sumPrice += (float) $i->price;
                $sumQuantity += (int) $i->quantity;
            }


            $s->price = $sumPrice;
            $s->quantity = $sumQuantity;
            $s->quantity_div_quantityOfOrders = round((int) $s->quantity / (int) $s->quantityOfOrders, 2);
            $s->price_div_quantityOfOrders = round((float) $s->price / (int) $s->quantityOfOrders, 2);
        }

        usort($sellers, function($a, $b) {
            return $a->quantity_div_quantityOfOrders > $b->quantity_div_quantityOfOrders ? -1 : 1;
        });   

        return $sellers;
    }

    public function getInfoStatisticByProducts($from, $to, $additionalParams = array())
    {
        $filterSqlPart = self::generateFiltrationSqIfCan($additionalParams);
        
        $st = $this->dbConn->prepare(
            'SELECT 
                store_order_product.code,
                store_order_product.articul AS description,
                (SELECT categories.name FROM categories WHERE categories.category_id = store_order_product.category_id LIMIT 1) AS category,
                (SELECT provider.code FROM provider WHERE provider.provider_id = store_order_product.provider_id LIMIT 1) AS provider,
                store_order_product.price,
                store_order_product.quantity
             FROM 
                store_order_product
                JOIN
                    store_order ON store_order.store_order_id = store_order_product.store_order_id
                JOIN
                    store ON store.store_id = store_order_product.store_id
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
            ' . $filterSqlPart
        );

        self::bindValuesIfNeed($st, $additionalParams);
        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));

        if (! $st->execute()) {
            throw new \Exception('Cant get store orders');
        }
        $products = $st->fetchAll(\PDO::FETCH_OBJ);

        $summPrice = 0;
        $summQuantity = 0;

        $statistic = array();
        foreach ($products as &$item) {
            $item->price = round($item->price, 2);
            
            if (!isset($statistic[$item->code])) {
                $statistic[$item->code] = array(
                    'code' => $item->code,
                    'description' => $item->description,
                    'category' => $item->category,
                    'provider' => $item->provider,
                    'price' => 0,
                    'quantity' => 0
                );
            }

            $statistic[$item->code]['price'] += (int) $item->price;
            $statistic[$item->code]['quantity'] += (float) $item->quantity;
            $summPrice += $item->price;
            $summQuantity += $item->quantity;
        }

        foreach ($statistic as &$i) {
            $price = $i['price'];
            $i['percents'] = round($price / $summPrice, 5) * 100;
        }
        
        usort($statistic, function($a, $b) {
            return $a['price'] > $b['price'] ? -1 : 1;
        });

        $statistic[] = array(
            'price' => $summPrice,
            'quantity' => $summQuantity,
            'description' => '',
            'category' => '',
            'provider' => '',
            'code' => 'TOTAL',
            'percents' => 100
        );
        
        return $statistic;
    }

    public function getInfoStatisticByProviders($from, $to)
    {
        $st =  $this->dbConn->prepare(
            'SELECT 
                (SELECT provider.code FROM provider WHERE provider.provider_id = store_order_product.provider_id LIMIT 1) AS provider,
                store_order_product.price,
                store_order_product.quantity
             FROM 
                store_order_product
                JOIN
                    store_order ON store_order.store_order_id = store_order_product.store_order_id
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to'
        );


        if (! $st->execute(array(
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s'),
        ))) {
            throw new \Exception('Cant get store orders');
        }
        $info = $st->fetchAll(\PDO::FETCH_OBJ);

        
        $summPrice = 0;
        $summQuantity = 0;
        $res = array();
        foreach ($info as $i) {
            if (! isset($res[$i->provider])) {
                $res[$i->provider] = array(
                    'provider' => $i->provider,
                    'price' => 0,
                    'quantity' => 0
                );
            }
            
            $res[$i->provider]['quantity'] += (int) $i->quantity;
            $res[$i->provider]['price'] += (float) $i->price;
            $summQuantity += (int) $i->quantity;
            $summPrice += (float) $i->price;
        }

        foreach ($res as &$item) {
            $item['price'] = round($item['price'], 2);
            $item['percents'] = round($item['price'] / $summPrice, 5) * 100;
        }

        
        usort($res, function($a, $b) {
            return $a['price'] > $b['price'] ? -1 : 1;
        });

        $res[] = array(
            'price' => $summPrice,
            'quantity' => $summQuantity,
            'provider' => 'TOTAL',
            'percents' => 100
        );

        
        return $res;
    }

    /*
    @todo Need to fix it

    SUM(store_order_product.price) AS price,
    does - wrong price
    */
    public function getInfoStatisticByCategories($from, $to, $additionalParams = array())
    {
        $filterSqlPart = self::generateFiltrationSqIfCan($additionalParams);
        $st = $this->dbConn->prepare(
            'SELECT 
                (SELECT categories.name FROM categories WHERE categories.category_id = store_order_product.category_id LIMIT 1) AS category,
                store_order_product.price,
                store_order_product.quantity
             FROM 
                store_order_product
                JOIN
                    store_order ON store_order.store_order_id = store_order_product.store_order_id
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to' . $filterSqlPart
        );


        if (! $st->execute(array(
            'from' => $from->format('Y-m-d H:i:s'),
            'to' => $to->format('Y-m-d H:i:s'),
        ))) {
            throw new \Exception('Cant get store orders');
        }

        $info = $st->fetchAll(\PDO::FETCH_OBJ);
        
        $summPrice = 0;
        $summQuantity = 0;
        $res = array();
        foreach ($info as $i) {
            if (! isset($res[$i->category])) {
                $res[$i->category] = array(
                    'category' => $i->category,
                    'price' => 0,
                    'quantity' => 0
                );
            }
            
            $res[$i->category]['quantity'] += (int) $i->quantity;
            $res[$i->category]['price'] += (float) $i->price;
            $summQuantity += (int) $i->quantity;
            $summPrice += (float) $i->price;
        }

        foreach ($res as &$item) {
            $item['price'] = round($item['price'], 2);
            $item['percents'] = round($item['price'] / $summPrice, 5) * 100;
        }
        
        usort($res, function($a, $b) {
            return $a['price'] > $b['price'] ? -1 : 1;
        });

        $res[] = array(
            'price' => $summPrice,
            'quantity' => $summQuantity,
            'category' => 'TOTAL',
            'percents' => 100
        );
        
        return $res;
    }

    public function getRealProductIdsWhichWereSoldAt($from, $to)
    {
        $st = $this->dbConn->prepare(
            'SELECT 
                DISTINCT product.product_id
             FROM 
                store_order_product
                    JOIN product ON 
                        store_order_product.color = product.color
                        AND
                        store_order_product.size = product.size
                        AND
                        store_order_product.articul = product.articul
                        AND
                        store_order_product.code = product.code
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
            '
        );

        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));

        if (! $st->execute()) {
            throw new \Exception('Cant get store orders');
        }
        if ($res = $st->fetchAll(\PDO::FETCH_OBJ)) {
            $tmp = [];
            foreach ($res as $row) {
                $tmp[] = $row->product_id;
            }

            return $tmp;
        }
        
        return [];
    }

    public function getInfoStatisticByProductsQuantityPerStore($from, $to, $additionalParams = array())
    {
        $filterSqlPart = self::generateFiltrationSqIfCan($additionalParams);
        $st = $this->dbConn->prepare(
            'SELECT 
                store_order_product.code,
                store_order_product.articul,
                store_order_product.quantity,
                store_order_product.price,
                IFNULL((SELECT categories.name FROM categories WHERE categories.category_id = store_order_product.category_id LIMIT 1), "") AS categoryName,
                (SELECT provider.code FROM provider WHERE provider.provider_id = store_order_product.provider_id LIMIT 1) AS providerCode,
                (SELECT store.external_store_id FROM store WHERE store.store_id = store_order_product.store_id LIMIT 1) AS external_store_id
             FROM 
                store_order_product
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
                '. $filterSqlPart .'
            ORDER BY store_order_product.code'
        );

        self::bindValuesIfNeed($st, $additionalParams);
        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));

        if (! $st->execute()) {
            throw new \Exception('Cant get store orders');
        }
        $categories = $st->fetchAll(\PDO::FETCH_OBJ);
        $res = $categories;
        
        if (isset($additionalParams['storeIds'])) {
            $stores = $this->getAllStores($additionalParams['storeIds']);
        } else {
            $stores = $this->getAllStores();
        }

        $initilaValues = array();
        foreach ($stores as $store) {
            $initilaValues[$store->external_store_id] = 0;
        }

        $statistic = array();
        foreach ($res as $row) {
            if (!isset($statistic[$row->code])) {
                $statistic[$row->code] = array(
                    'code' => $row->code,
                    'articul' => $row->articul,
                    'categoryName' => $row->categoryName,
                    'providerCode' => $row->providerCode,
                    /*
                    @todo Bug for empty result
                    */
                    'quantityIn' => $initilaValues,
                    'priceIn' => $initilaValues,
                );

                $statistic[$row->code]['quantityIn']['Total'] = 0;
                $statistic[$row->code]['priceIn']['Total'] = 0;
            }
            

            $statistic[$row->code]['quantityIn'][$row->external_store_id] += (int) $row->quantity;
            $statistic[$row->code]['quantityIn']['Total'] += (int) $row->quantity;        

            $statistic[$row->code]['priceIn'][$row->external_store_id] += (float) $row->price;
            $statistic[$row->code]['priceIn']['Total'] += (float) $row->price;        
        }


        return $statistic;
    }

    public function getInfoStatisticByProviderQuantityPerStore($from, $to, $additionalParams = array())
    {
        $filterSqlPart = self::generateFiltrationSqIfCan($additionalParams);
        $st = $this->dbConn->prepare(
            'SELECT 
                provider.code,
                store_order_product.provider_id,
                store.external_store_id,
                store_order_product.quantity,
                store_order_product.price
             FROM 
                store_order_product
                    JOIN store ON store_order_product.store_id = store.store_id
                    JOIN provider ON provider.provider_id = store_order_product.provider_id
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
                '. $filterSqlPart .'
            ORDER BY provider.code'
        );

        self::bindValuesIfNeed($st, $additionalParams);
        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));

        if (! $st->execute()) {
            throw new \Exception('Cant get store orders');
        }
        $categories = $st->fetchAll(\PDO::FETCH_OBJ);
        $res = $categories;
        
        if (isset($additionalParams['storeIds'])) {
            $stores = $this->getAllStores($additionalParams['storeIds']);
        } else {
            $stores = $this->getAllStores();
        }

        $initilaValues = array();
        foreach ($stores as $store) {
            $initilaValues[$store->external_store_id] = 0;
        }

        $statistic = array();
        foreach ($res as $row) {
            if (!isset($statistic[$row->code])) {
                $statistic[$row->code] = array(
                    'code' => $row->code,
                    'provider_id' => $row->provider_id,
                    'quantityIn' => $initilaValues,
                    'priceIn' => $initilaValues,
                );

                $statistic[$row->code]['quantityIn']['Total'] = 0;
                $statistic[$row->code]['priceIn']['Total'] = 0;
            }
            

            $statistic[$row->code]['quantityIn'][$row->external_store_id] += (int) $row->quantity;
            $statistic[$row->code]['quantityIn']['Total'] += (int) $row->quantity;        

            $statistic[$row->code]['priceIn'][$row->external_store_id] += (float) $row->price;
            $statistic[$row->code]['priceIn']['Total'] += (float) $row->price;        
        }


        return $statistic;
    }

    public function getInfoStatisticByCategoryQuantityPerStore($from, $to, $additionalParams = array())
    {
        $filterSqlPart = self::generateFiltrationSqIfCan($additionalParams);
        $st = $this->dbConn->prepare(
            'SELECT 
                categories.name,
                store_order_product.category_id,
                store.external_store_id,
                store_order_product.quantity,
                store_order_product.price
             FROM 
                store_order_product
                    JOIN store ON store_order_product.store_id = store.store_id
                    JOIN categories ON categories.category_id = store_order_product.category_id
            WHERE 
                store_order_product.date >= :from
                AND
                store_order_product.date <= :to
                '. $filterSqlPart .'
            ORDER BY categories.name'
        );

        self::bindValuesIfNeed($st, $additionalParams);
        $st->bindValue(':from', $from->format('Y-m-d H:i:s'));
        $st->bindValue(':to', $to->format('Y-m-d H:i:s'));

        if (! $st->execute()) {
            throw new \Exception('Cant get store orders');
        }
        $categories = $st->fetchAll(\PDO::FETCH_OBJ);
        $res = $categories;
        
        if (isset($additionalParams['storeIds'])) {
            $stores = $this->getAllStores($additionalParams['storeIds']);
        } else {
            $stores = $this->getAllStores();
        }

        $initilaValues = array();
        foreach ($stores as $store) {
            $initilaValues[$store->external_store_id] = 0;
        }

        $statistic = array();
        foreach ($res as $row) {
            if (!isset($statistic[$row->category_id])) {
                $statistic[$row->category_id] = array(
                    'categoryName' => $row->name,
                    'category_id' => $row->category_id,
                    'quantityIn' => $initilaValues,
                    'priceIn' => $initilaValues,
                );

                $statistic[$row->category_id]['quantityIn']['Total'] = 0;
                $statistic[$row->category_id]['priceIn']['Total'] = 0;
            }
            

            $statistic[$row->category_id]['quantityIn'][$row->external_store_id] += (int) $row->quantity;
            $statistic[$row->category_id]['quantityIn']['Total'] += (int) $row->quantity;        

            $statistic[$row->category_id]['priceIn'][$row->external_store_id] += (float) $row->price;
            $statistic[$row->category_id]['priceIn']['Total'] += (float) $row->price;        
        }


        return $statistic;
    }
    
    public function uploadFile($fileName, $code)
    {
        $idOfLog = $this->saveFileLog($code);
        switch ($code) {
            case self::$TYPE_OF_FILE['ALL_INFO']:
                $res = (
                    $this->saveInforFromFileWithFullProductDescription($fileName, true) 
                    &&
                    $this->autoUpdateCategoriesForProducts()
                );
                break;
            case self::$TYPE_OF_FILE['PAYMENT_INFO']:
                $res = $this->saveInfoFromFileWithTypeOfPayment($fileName);
                break;
            case self::$TYPE_OF_FILE['STOCK']:
                $res = $this->saveStockInformationFromFile($fileName);
                break;
            case self::$TYPE_OF_FILE['PRODUCT_PRICES']:
                $res = $this->saveProductPricesFromFile($fileName);
                break;
            case self::$TYPE_OF_FILE['NEED_PRODUCTS']:
                $res = $this->saveProductNeedInformationFromFile($fileName);
                break;
        }

        if ($res) {
            $this->markAsSucessForLogOfFile($idOfLog);
        }
        
        return $res;
    }

    private function saveFileLog($typeIdOfFile)
    {
        $st = $this->dbConn->prepare(
            'INSERT INTO
                uploading_of_files_log
            (file_type_id)
            VALUES
           (:typeIdOfFile)'
        );


        $st->bindValue(':typeIdOfFile', $typeIdOfFile);

        if (!$st->execute()) {
            throw new \Exception('Error of writing log !');
        }

        return $this->dbConn->lastInsertId();
    }

    private function markAsSucessForLogOfFile($logId)
    {
        $st = $this->dbConn->prepare(
            'UPDATE
                uploading_of_files_log
            SET
                time_of_finshing_saving = NOW(),
                is_all_ok = 1
            WHERE
                log_id = :logId
            LIMIT 1'
        );


        $st->bindValue(':logId', $logId);
        return $st->execute();
    }

    public function getInformationForExport($typeId, $params = [])
    {
        $typesOfExport = self::$typesOfExport;
        $typeId = (int) $typeId;
        $from = $params['from'];
        $to = $params['to'];
        $additionalParams = $params['additionalParams'];
        $pages = [];
        $currentPageId = 0;

        $description = [];
        switch ($typeId) {
            case $typesOfExport['BY_STORE']:
                $info = $this->getInfoForStatisticByStores($from, $to, $additionalParams);
                $description[] = ['ID de tienda', 'Unidades', 'Monto', 'Comprabantes', 'Monto/Comprabantes', 'Unidades/Comprabantes'];
                foreach ($info as $key => $row) {
                   $row = (object) $row;
                   $rowFile = array();
                   $rowFile[] = $row->storeCode;
                   $rowFile[] = $row->quantity;
                   $rowFile[] = '$' . $row->price;
                   $rowFile[] = $row->quantityOfOrders;
                   $rowFile[] = '$' . $row->price_div_quantityOfOrders;
                   $rowFile[] = $row->quantity_div_quantityOfOrders;
                   $description[] = $rowFile;
                }
            break;
            case $typesOfExport['BY_SELLER']:
                $info = $this->getInfoForStatisticBySellers($from, $to, $additionalParams);

                $description[] = [
                    'Vendedor',
                    'Unidades',
                    'Monto',
                    'Comprobantes',
                    'Monto/Comprobantes',
                    'Unidades/Comprobantes',
                ];

                foreach ($info as $key => $row) {
                   
                   $row = (object) $row;

                   $rowFile = array();
                   $rowFile[] = $row->seller;
                   $rowFile[] = $row->quantity;
                   $rowFile[] = round($row->price, 2);
                   $rowFile[] = $row->quantityOfOrders;
                   $rowFile[] = $row->price_div_quantityOfOrders;
                   $rowFile[] = $row->quantity_div_quantityOfOrders;

                   $description[] = $rowFile;
                }
            break;
            case $typesOfExport['BY_PRODUCT']:
                $info = $this->getinfostatisticbyproducts($from, $to, $additionalParams);
                $description[] = [
                    'Articulo',
                    'Descripcion',
                    'Categoria',
                    'Suma de Item - Candidat',
                    'Suma de Item - Monto Neto',
                    'Proveedor',
                    'Porcentaje',
                ];

                foreach ($info as $key => $row) {
                   
                   $row = (object) $row;

                   $rowFile = array();
                   $rowFile[] = $row->code;
                   $rowFile[] = $row->description;
                   $rowFile[] = $row->category;
                   $rowFile[] = $row->quantity;
                   $rowFile[] = '$' . $row->price;
                   $rowFile[] = $row->provider;
                   $rowFile[] = $row->percents . '%';

                   $description[] = $rowFile;
                }

            break;
            case $typesOfExport['BY_PROVIDER']:
                $info = $this->getInfoStatisticByProviders($from, $to, $additionalParams);

                $description[] = [
                    'Proveeedor',
                    'Cantidad',
                    'Total',
                    'Porcentaje',
                ];

                foreach ($info as $key => $row) {
                   
                   $row = (object) $row;

                   $rowFile = array();
                   $rowFile[] = $row->provider;
                   $rowFile[] = $row->quantity;
                   $rowFile[] ='$' . $row->price;
                   $rowFile[] = $row->percents . '%';

                   $description[] = $rowFile;
                }
            break;
            case $typesOfExport['BY_CATEGORIES']:
                $info = $this->getInfoStatisticByCategories($from, $to, $additionalParams);
                $description[] = [
                    'Categoria',
                    'Cantidad',
                    'Total',
                    'Porcentaje'
                ];

                foreach ($info as $key => $row) {
                   
                   $row = (object) $row;

                   $rowFile = array();
                   $rowFile[] = $row->category;
                   $rowFile[] = $row->quantity;
                   $rowFile[] = '$' . $row->price;
                   $rowFile[] = $row->percents . '%';

                   $description[] = $rowFile;
                }
            break;
            case $typesOfExport['BY_PRODUCTS_QUANTITY_PER_STORE']:
                $info = $this->getInfoStatisticByProductsQuantityPerStore($from, $to, $additionalParams);

                $el = reset($info);
                $storeNames = array();
                if (isset($el['quantityIn'])) {
                    foreach ($el['quantityIn'] as $keyStore => $store) {
                        $storeNames[] = $keyStore;
                    }
                }

                $fieldsNames[] = 'Producto';
                foreach ($storeNames as $storeName) {
                    $fieldsNames[] = $storeName;
                }

                $description[] = $fieldsNames;
                usort($info, function($a, $b) {
                    return $a['quantityIn']['Total'] > $b['quantityIn']['Total'] ? -1 : 1;
                });

                foreach ($info as $key => $row) {
                    $row = (object) $row;

                    $rowFile = array();
                    $rowFile[] = $row->code . ' ' . $row->articul;
                    foreach ($row->quantityIn as $val) {
                       $rowFile[] = $val;
                    }

                    $description[] = $rowFile;
                }

            break;
            case $typesOfExport['BY_PRODUCTS_PRICE_PER_STORE']:
                $info = $this->getInfoStatisticByProductsQuantityPerStore($from, $to, $additionalParams);

                $el = reset($info);
                $storeNames = array();
                if (isset($el['quantityIn'])) {
                    foreach ($el['quantityIn'] as $keyStore => $store) {
                        $storeNames[] = $keyStore;
                    }
                }

                $fieldsNames[] = 'Producto';
                foreach ($storeNames as $storeName) {
                    $fieldsNames[] = $storeName;
                }

                $description[] = $fieldsNames;

                usort($info, function($a, $b) {
                    return $a['priceIn']['Total'] > $b['priceIn']['Total'] ? -1 : 1;
                });

                foreach ($info as $key => $row) {
                    $row = (object) $row;

                    $rowFile = array();
                    $rowFile[] = $row->code . ' ' . $row->articul;
                    foreach ($row->priceIn as $val) {
                       $rowFile[] = $val;
                    }

                    $description[] = $rowFile;
                }
            break;
            case $typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE']:
                $info = $this->getInfoForStatisticByStoresAndPaymentType($from, $to, $additionalParams);

                $description[] = [
                    'Tienda',
                    'Efectivo',
                    'Otro',
                    'Total'
                ];

                foreach ($info as $key => $row) {
           
                   $row = (object) $row;

                   $rowFile = array();
                   $rowFile[] = $row->store_code;
                   $rowFile[] = '$' . $row->real_money;
                   $rowFile[] = '$' . $row->other;
                   $rowFile[] = '$' . $row->total;

                   $description[] = $rowFile;
                }
            break;
            case $typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE_BY_MONTS']:
                if ($params['idOfSheet']) {
                    $currentPageId = $params['idOfSheet'];
                }

                $sheets = $this->getInformationForStatisticByMonths($from, $to, $additionalParams);

                foreach ($sheets as $key => $sheet) {
                    $pages[] = [
                        'name' => $sheet['name'],
                        'id' => $key,
                    ];
                }

                $sheet = $sheets[$currentPageId];

                $description = $sheet['cells'];
            break;
            case $typesOfExport['WITH_STOCK']:
                if ($params['idOfSheet']) {
                    $currentPageId = $params['idOfSheet'];
                }

                $additionalParams['needOnlyPageId'] = $currentPageId;
                $sheets = $this->getInfoWithStockStatistic($from, $to, $additionalParams);
                
                foreach ($sheets as $key => $sheet) {
                    $pages[] = [
                        'name' => $sheet['name'],
                        'id' => $key,
                    ];
                }

                $sheet = $sheets[$currentPageId];
                $description = $sheet['cells'];
            break;
            case $typesOfExport['PRODUCTS_WHICH_NEED']:
            
                //return $this->prepareData($this->generator->getInfoForProductsWhichWillNeed($this->from, $this->to));
                $data = $this->getInfoForProductsWhichWillNeed($from, $to);
                $el = reset($data);

                $captions = ['Articulo', 'Color', 'Talle'];
                if (isset($el['stock'])) {
                    foreach ($el['stock'] as $key => $val) {
                        $captions[] = 'Repo ' . $key;
                        $captions[] = 'Stock ' . $key;
                        $captions[] = 'Pedido ' . $key;
                    }
                }

                $description[] = $captions;

                foreach ($data as $key => $row) {
                    $row = (object) $row;
                    $rowFile = array();
                    $rowFile[] = $row->code; 
                    $rowFile[] = $row->color;
                    $rowFile[] = $row->size;

                    foreach ($row->stock as $key => $val) {
                        $rowFile[] = $row->need[$key];
                        $rowFile[] = $val;
                        $rowFile[] = $row->ask[$key];
                    }

                    $description[] = $rowFile;
                }
            break;
            case $typesOfExport['ALL_PRODUCTS_WITH_STOCK']:
                $dateForStock = $this->getActualDateForStock($from, $to);
                $dateForStock = new \DateTime($dateForStock);
                $info = $this->getAllProductsWithStock($dateForStock);

                $description = [];
                $captions = [
                    'Name',
                    'Code',
                    'Color',
                    'Size',
                    'Category'
                ];

                if (count($info) > 0) {
                    $stores = [];
                    $firstEl = reset($info);
                    foreach ($firstEl['stock'] as $storeName => $val) {
                        $stores[] = $storeName;
                        $captions[] = $storeName;
                    }

                    foreach ($info as $key => $row) {
               
                        $row = (object) $row;

                        $rowFile = array();
                        $rowFile[] = $row->articul;
                        $rowFile[] = $row->code;
                        $rowFile[] = $row->color;
                        $rowFile[] = $row->size;
                        $rowFile[] = $row->category;

                        foreach ($stores as $store) {
                            $rowFile[] = $row->stock[$store];
                        }

                        $description[] = $rowFile;
                    }
                }

                array_unshift($description, $captions);

            break;

        }

        //additional cells for some exports
        if (
            $typeId !== $typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE_BY_MONTS']
            &&
            $typeId !== $typesOfExport['WITH_STOCK']
            &&
            count($description) > 0
            &&
            isset($description[0][1])
        ) {
            $countOfCells = count($description[0]);
            $emptyRow = [];
            for ($i = 0; $i < $countOfCells; $i++) {
                $emptyRow[] = '';
            }

            array_unshift($description, $emptyRow);
            $emptyRow[1] = 'Informe de "'.$this->typesOfExportNames[$typeId] . 
                '" desde "'. $from->format('Y-m-d H:i:s') .'" hasta "'. $to->format('Y-m-d H:i:s') .'"';
            array_unshift($description, $emptyRow);


            $stores = [];
            if (isset($additionalParams['storeIds'])) {
                $stores = $this->getAllStores($additionalParams['storeIds']);
            } else {
                $stores = $this->getAllStores();
            }

            $storeNames = [];
            foreach ($stores as $store) {
                $storeNames[] = $store->external_store_id;
            }

            $emptyRow[1] = '"Stores: ' . implode(', ', $storeNames) . '"';
            array_unshift($description, $emptyRow);
        }

        if (isset($params['searchBy'])) {

            $searchBy = $params['searchBy'];
            $isFoundCaptions = false;
            foreach($description as $key => $row) {
                $isFound = false;
                //get row with captions
                if (!$isFoundCaptions && $row[0] !== '') {
                    $isFoundCaptions = true;
                    continue;
                }

                foreach ($row as $cell) {
                    $cell = (string) $cell;

                    if (mb_strpos($cell , $searchBy, 0, 'UTF-8') !== false) {
                        $isFound = true;
                        break;
                    }
                }

                if (!$isFound) {
                    unset($description[$key]);
                }
            }

            $description = array_values($description);
        }

        if (isset($params['limit'])) {
            $params['limit'] = (int) $params['limit'];
            $fromRow = isset($params['offset']) ? (int) $params['offset'] : 0;

            $description = array_slice($description, $fromRow, $params['limit']);

            if (count($description) >= $params['limit']) {
                $description = array(
                    'description' => $description,
                    'lastRow' => $fromRow + $params['limit']
                );
            }
        }

        if (count($pages) > 1) {
            if (!isset($description['description'])) {
                $description['description'] = $description;
            }

            $description['pages'] = $pages;
            $description['currentPageId'] = $currentPageId;
        }

        return $description;
    }
}
