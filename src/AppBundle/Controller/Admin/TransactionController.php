<?php
namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Entity\Seller as Seller;
use AppBundle\Entity\Store as Store;
use AppBundle\Entity\Category as Category;
use AppBundle\Entity\StoreOrder as StoreOrder;
use AppBundle\Entity\StoreOrderProduct as StoreOrderProduct;

use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Sonata\CoreBundle\Form\Type\DateTimeRangePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Doctrine\ORM\Query\ResultSetMapping;

use AppBundle\Helper\Gmail;
use AppBundle\Helper\FileGenerator;

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;

use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
class TransactionController extends Controller
{

    public function listAction() 
    {   
        ini_set('memory_limit','100M');
        set_time_limit(3600 * 5);
        $fileName = '/home/demiurg337/test.csv';
        $generator = new FileGenerator($this->getDoctrine());
        //$generator->saveProductPricesFromFile('/home/demiurg337/price_test.csv');
        //$generator->autoUpdatePriceForProducts();
        //$generator->autoUpdateCategoriesForProducts();
        //$generator->saveStockInformationFromFile($fileName);
        //die;
        //$generator = new FileGenerator($this->getDoctrine());
        //$generator->saveInforFromFileWithFullProductDescription($fileName);
        /*
        $date = (new \DateTime())->modify('-4 day');
        $date->format('Y-m-d H:i:s');
        $from = new \DateTime($date->format('Y-m-d') . ' 00:00:00');
        $to = new \DateTime($date->format('Y-m-d') . ' 23:59:59');

        $from = new \DateTime('2018-03-15 00:00:00');
        $to = new \DateTime('2018-03-21 23:59:59');

        $generator = new FileGenerator($this->getDoctrine());
        $basePath = '/tmp';

        $stores = $generator->getAllStores();
        
        foreach ($stores as $store) {
            $storeName = str_replace(' ', '_', $store->external_store_id);
            $fileName = $from->format('Y-m-d') . '-' . $from->format('Y-m-d') . '_' . $storeName . '.csv';
            $toFile = $basePath . '/' . $fileName;
            
            $additionalParams = array(
                'storeIds' => [$store->store_id]
            );



            $stores = $generator->getInfoForStatisticByStores($from, $to);
            $sellers = $generator->getInfoForStatisticBySellers($from, $to, $additionalParams);
            
            $info = array(
                'sellers' => $sellers,
                'stores' => $stores,
                'timeRange' => array(
                    'from' => $from,
                    'to' => $to,
                )
            );

            $msg = 'Repoort por vendedores';
            if (count($sellers) > 0) {
                //////////////////////////
                //////////////////////////
                //////////////////////////
                $output_file = fopen($toFile, 'w');
                if ($output_file === false) {
                    // Handle error
                }

                $write_ob_to_file = function($buffer) use ($output_file) {
                    fwrite($output_file, $buffer);

                    // Output string as-is
                    return false;
                };

                ob_start($write_ob_to_file, 4096);
                
                ///////////////////////////////////
                //It willl ne to fix
                //1059 $exportHelper->getExport();
                ///////////////////////////////////
                $this->getFile($info, 'ExportForReports');
                ob_end_flush();

                fclose($output_file);
                //////////////////////////
                //////////////////////////
                //////////////////////////
                
                if (! file_exists($toFile)) {
                    continue;
                }
            } else {
                $toFile = false;
                $msg = 'Durante esta fecha no hubo compras para esta tienda';
            }

            $emailPortions = ['lfrontera@clubpoint.com.ar', 'jptorras@clubpoint.com.ar', 'spampuro@bomer.com.ar'];
            
            $storeName = trim($store->external_store_id);
          
            if ($storeName === 'MDQ2344') {
                $emailPortions[] = 'bomermdq@gmail.com';
            } elseif ($storeName === '8 775') {
                $emailPortions[] = 'bomerlp8@gmail.com';
            } elseif ($storeName === 'Quilmes 338') {
                $emailPortions[] = 'bomerquilmes338@gmail.com';
            } elseif ($storeName === '12 1290') {
                $emailPortions[] = 'bomerlp12@gmail.com';
            } elseif ($storeName === 'MRN - BM2916') {
                $emailPortions[] = 'bomermoreno@gmail.com';
            }

            //$emailPortions = ['dmitro.gedz@gmail.com', 'yodeco@gmail.com',];
            
            $message = \Swift_Message::newInstance()
                ->setSubject('Repoort por vendedores')
                ->setFrom('cpznubereports@gmail.com')
                ->setTo($emailPortions)
                ->setBody(
                    $msg . ', store ' . $store->external_store_id,
                    'text/html'
                );

            if ($toFile) {
                $message->attach(\Swift_Attachment::fromPath($toFile));
            }

            $this->get('mailer')->send($message);
        }
        die;
        */

        ///////////////////////////////////c/
        /////////////////////////////////////
        /////////////////////////////////////

        $request = $this->getRequest();

        $this->admin->checkAccess('list');

        $preResponse = $this->preList($request);
        if ($preResponse !== null) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }
        
        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        //$this->setFormTheme($formView, $this->admin->getFilterTheme());

        $customTemplate = $this->get('kernel')->getSrcDir() . '/AppBundle/Admin/views/list.html.twig';
        return $this->render($customTemplate, array(
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
            'export_formats' => $this->has('sonata.admin.admin_exporter') ?
                $this->get('sonata.admin.admin_exporter')->getAvailableFormats($this->admin) :
                $this->admin->getExportFormats(),
        ), null);
    }

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    // For saving files
    public function updateStatisticAction()
    {
        ini_set('memory_limit','100M');
        set_time_limit(3600);
        /*
        Tempratry disabling autopdating
        */
        $now = new \DateTime();
        $response = new JsonResponse(array(
            'lastSuccessUpdate' => $now->format('Y-m-d H:i:s')
        ));

        return $response;

        $gmailHelper = new Gmail();

        //list with short description of message
        $messages = $gmailHelper->getGmailMessagesWithInfo('Reporte zNube - For Dima 2 do not delete');
        
        $this->executeFilesFromGmail($messages);

        $messagesWithPyamentInfo = $gmailHelper->getGmailMessagesWithInfo('Reporte zNube - Comprobantes de venta por medio de pago');
        $this->executeFilesFromGmail($messagesWithPyamentInfo, 'with_pay_info_');
        
        $time = $this->getTimeOfLastSuccessSavingInformation();
        
        if (null === $time) {
            $time = '';
        }

        $response = new JsonResponse(array(
            'lastSuccessUpdate' => $time
        ));

        return $response;
    }

    public function executeFilesFromGmail($messages, $prefixForFile = '')
    {
        $gmailHelper = new Gmail();
        $now = new \DateTime;
        $generator = new FileGenerator($this->getDoctrine());
        foreach ($messages as $msg) {
            if ($this->isExistZnubeLogFor($msg->id)) {
                continue;
            }

            $this->saveZnubeStartFileLog($msg->id);

            $infoFromFile = $gmailHelper->getInfoFromMessage($msg->id);
            
            if ($infoFromFile !== null) {
                $nameOfFile = $prefixForFile . $msg->id .'_Received:' . $infoFromFile['dateOfReceiving'] . ' Saving_started_at:' . $now->format('Y-m-d H:i:s') . '.csv';
                $fullPath = $this->get('kernel')->getSrcDir(). '/../znube_files/' . $nameOfFile;
                file_put_contents($fullPath, $infoFromFile['attachment'][0]);

                if ($prefixForFile === '') {
                   $isOk = $generator->saveInforFromFileWithFullProductDescription($fullPath);
                } else {
                   $isOk = $generator->saveInfoFromFileWithTypeOfPayment($fullPath);
                }

                if ($isOk) {
                    $this->saveZnubeSuccessFileLog($msg->id);
                }
            }
        }
    }

    private function isExistZnubeLogFor($googleEmailId)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection()->getWrappedConnection();

        $st = $conn->prepare(
            'SELECT
                google_email_id
            FROM
                znube_email_files
            WHERE
                google_email_id = :googleEmailId
            LIMIT 1'
        );


        if (!$st->execute(array(
            'googleEmailId' => $googleEmailId,
        ))) {
            throw new \Exception('Cant get log for Znube !');
        }

        if ($res = $st->fetchObject()) {
            return true;
        }

        return false;
    }

    private function saveZnubeStartFileLog($googleEmailId)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $st = $conn->prepare(
            'INSERT INTO
                znube_email_files
            (google_email_id)
            VALUES
            (:googleEmailId)'
        );


        return $st->execute(array(
            'googleEmailId' => $googleEmailId,
        ));
    }
     
    private function saveZnubeSuccessFileLog($googleEmailId)
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $st = $conn->prepare(
            'UPDATE
                znube_email_files
            SET
                time_of_finshing_saving = NOW(),
                is_all_ok = 1
            WHERE
                google_email_id = :googleEmailId
            LIMIT 1'
        );


        return $st->execute(array(
            'googleEmailId' => $googleEmailId,
        ));
    }

    private function getTimeOfLastSuccessSavingInformation()
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection()->getWrappedConnection();

        $st = $conn->prepare(
            'SELECT
                time_of_finshing_saving
            FROM
                znube_email_files
            ORDER BY
                time_of_finshing_saving DESC
            LIMIT 1'
        );


        if (!$st->execute()) {
            throw new \Exception('Cant get log for Znube !');
        }

        if ($res = $st->fetchObject()) {
            return $res->time_of_finshing_saving;
        }

        return null;
    }
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    public function getProductForFilterAction(Request $request)
    {
        $q = $request->query->get('q'); // use "term" instead of "q" for jquery-ui

        $generator = new FileGenerator($this->getDoctrine());
        $conn = $this->getDoctrine()->getManager()->getConnection()->getWrappedConnection();
        $st = $conn->prepare(
            'SELECT 
                DISTINCT store_order_product.articul,
                store_order_product.code
             FROM 
                store_order_product
             WHERE 
                store_order_product.code LIKE :q
                OR
                store_order_product.articul LIKE :q
            LIMIT 10'
        );

        if (! $st->execute(array(
            'q' => '%' . $q . '%' ,
        ))) {
            throw new \Exception('Cant get store orders');
        }

        $res = $st->fetchAll(\PDO::FETCH_OBJ);

        $results = array();
        foreach ($res as $row){
            $results[] = array(
                'id' => $generator->encodeValuesForSearching([$row->articul, $row->code]),
                'text' => $row->articul . ' - ' . $row->code,
            );
        }
       
        $response = new JsonResponse($results);

        return $response;
    }

    public function getStatisticFromDatabaseAction(Request $request)
    {
        $forExport = new \DateTime();
        $forExport->modify('-1 day');
        $generator = new FileGenerator($this->getDoctrine());
        $exportNames = $generator->typesOfExportNames;
        $typesOfExport = $generator::$typesOfExport;

        $form = $this->get('form.factory')->createNamedBuilder('form_for_dates')
            ->add('date', DateTimeRangePickerType::class, array(
                    'label' => 'Elige las fechas', 
                    'field_options' =>array(
                        'format' => 'yyyy-MM-dd HH:mm',
                    ),
                    'field_options_start' => array(
                        'label' => 'Desde',
                        'data'=> new \DateTime($forExport->format('Y-m-d' . ' 00:00:00'))
                    ),
                    'field_options_end' => array(
                        'label' => 'A',
                        'data'=> new \DateTime($forExport->format('Y-m-d' . ' 23:59:59'))
                    )

                )
            )
            ->add('seller', EntityType::class, [
                'label' => 'Vendedor',
                'class' => Seller::class,
                'choice_label' => 'name',
                'multiple' => true,
                'required' => false
            ])
            ->add('store', EntityType::class, [
                'label' => 'Tienda',
                'class' => Store::class,
                'choice_label' => 'externalStoreId',
                'multiple' => true,
                'required' => false
            ])
            ->add('category', EntityType::class, [
                'label' => 'Categoría',
                'class' => Category::class,
                'choice_label' => 'name',
                'multiple' => true,
                'required' => false
            ])
           ->add('product', Select2EntityType::class, [
                'label' => 'Producto',
                'multiple' => true,
                'remote_route' => 'admin_app_storeorderproduct_getProductForFilter',
                'minimum_input_length' => 2,
                'page_limit' => 10,
                'allow_clear' => true,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => 'en',
                'placeholder' => 'Select a prodcut',
                // 'object_manager' => $objectManager, // inject a custom object / entity manager 
            ])



            ->add('type', ChoiceType::class, array(
                'label' => 'Tipo de estadística:',
                'choices' => array(
                    $exportNames[$typesOfExport['BY_STORE']] => $typesOfExport['BY_STORE'],
                    $exportNames[$typesOfExport['BY_SELLER']] => $typesOfExport['BY_SELLER'],
                    $exportNames[$typesOfExport['BY_PRODUCT']] => $typesOfExport['BY_PRODUCT'],
                    $exportNames[$typesOfExport['BY_PROVIDER']] => $typesOfExport['BY_PROVIDER'],
                    $exportNames[$typesOfExport['BY_CATEGORIES']] => $typesOfExport['BY_CATEGORIES'],
                    $exportNames[$typesOfExport['BY_PRODUCTS_QUANTITY_PER_STORE']] => $typesOfExport['BY_PRODUCTS_QUANTITY_PER_STORE'],
                    $exportNames[$typesOfExport['BY_PRODUCTS_PRICE_PER_STORE']] => $typesOfExport['BY_PRODUCTS_PRICE_PER_STORE'],
                    $exportNames[$typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE']] => $typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE'],
                    $exportNames[$typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE_BY_MONTS']] => $typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE_BY_MONTS'],
                    $exportNames[$typesOfExport['WITH_STOCK']] => $typesOfExport['WITH_STOCK'],
                    $exportNames[$typesOfExport['PRODUCTS_WHICH_NEED']] => $typesOfExport['PRODUCTS_WHICH_NEED'],
                    $exportNames[$typesOfExport['ALL_PRODUCTS_WITH_STOCK']] => $typesOfExport['ALL_PRODUCTS_WITH_STOCK'],
                )
            ))

            ->add('send', SubmitType::class, array('label' => 'Obtener exportación de la base de datos'))
            ->getForm();
        /////////////////////////////////////

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            ini_set('memory_limit','150M');
            set_time_limit(3600);

            $info = $request->request->all();
            $info = $info['form_for_dates'];
            $date = $info['date'];

            $additionalParams = array();
            if (isset($info['store'])) {
                $additionalParams['storeIds'] = $info['store'];
            }

            if (isset($info['seller'])) {
                $additionalParams['sellerIds'] = $info['seller'];
            }

            if (isset($info['category'])) {
                $additionalParams['categoryIds'] = $info['category'];
            }

            if (isset($info['product'])) {
                $additionalParams['productParams'] = $info['product'];
            }
            
            $date = $info['date'];
            $from = new \DateTime('0000-00-00 00:00:00');
            $to = new \DateTime('0000-00-00 00:00:00');

            if (isset($date['start'])) {
                $from = new \DateTime($date['start']);
            }

            if (isset($date['start'])) {
                $to = new \DateTime($date['end']);
            }

            $typeId = $info['type'];
            
            $generator = new FileGenerator($this->getDoctrine());

            switch ($typeId) {
                case $generator::$typesOfExport['BY_STORE']:
                case $generator::$typesOfExport['BY_SELLER']:
                case $generator::$typesOfExport['BY_PRODUCT']:
                case $generator::$typesOfExport['BY_PROVIDER']:
                case $generator::$typesOfExport['BY_CATEGORIES']:
                case $generator::$typesOfExport['BY_PRODUCTS_QUANTITY_PER_STORE']:
                case $generator::$typesOfExport['BY_PRODUCTS_PRICE_PER_STORE']:
                case $generator::$typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE']:
                case $generator::$typesOfExport['PRODUCTS_WHICH_NEED']:
                case $generator::$typesOfExport['ALL_PRODUCTS_WITH_STOCK']:
                    $description = $generator->getInformationForExport($typeId, [
                        'from' => $from,
                        'to' => $to,
                        'additionalParams' => $additionalParams
                    ]);
                    $this->getFile($description, 'file_by_description');
                    die;
                case $generator::$typesOfExport['BY_PRICE_PER_STORE_AND_PAYMENT_TYPE_BY_MONTS']:
                    $sheets = $generator->getInformationForStatisticByMonths($from, $to, $additionalParams);
                    $generator->getFileWithStatiscByMonnts($sheets);
                    die;
                case $generator::$typesOfExport['WITH_STOCK']:
                    $generator->getFileWithStockStatistic($from, $to, $additionalParams);
                    die;
                    break;
                /*
                case $typesOfExport['PRODUCTS_WHICH_NEED']:
                    require_once $this->get('kernel')->getSrcDir(). '/../export/export_load.php';
                    $params = [
                        'generator' => $generator,
                        'from' => $from,
                        'to' => $to,
                    ];
                    $exportHelper = new \Helper\Export\Export($params, 'expectedProducts', 0);
                    $exportHelper->getExport();
                    die;
                    */
            }
        }
        
        ///////////////////////////////////////////////////

        $customTemplate = $this->get('kernel')->getSrcDir() . '/AppBundle/Admin/views/export.from.db.html.twig';
        return $this->render($customTemplate, array(
            'action' => 'Donwload files with statistic',
            'form' => $form->createView(),
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ), null);
    }

    public function getPreviewOfExportAction(Request $request)
    {
        $info =$request->query->all();
        foreach ($info as $key => $item) {
            if (is_string($info[$key])) { 
                if (trim($info[$key]) == '') {
                    unset($info[$key]);
                } else {
                    $info[$key] = trim($info[$key]);
                }
            }
        }

            $additionalParams = array();
            if (isset($info['store'])) {
                $additionalParams['storeIds'] = $info['store'];
            }

            if (isset($info['seller'])) {
                $additionalParams['sellerIds'] = $info['seller'];
            }

            if (isset($info['category'])) {
                $additionalParams['categoryIds'] = $info['category'];
            }

            if (isset($info['product'])) {
                $additionalParams['productParams'] = $info['product'];
            }
            
            $date = $info['date'];
            $from = new \DateTime('0000-00-00 00:00:00');
            $to = new \DateTime('0000-00-00 00:00:00');

            if (isset($date['start'])) {
                $from = new \DateTime($date['start']);
            }

            if (isset($date['start'])) {
                $to = new \DateTime($date['end']);
            }

            
            $typeId = (int) $info['type'];
        

        $generator = new FileGenerator($this->getDoctrine());
        $description = $generator->getInformationForExport($typeId, [
            'from' => $from,
            'to' => $to,
            'additionalParams' => $additionalParams,
            'limit' => 50,
            'offset' => isset($info['nextPortionFrom']) ? $info['nextPortionFrom'] : null,
            'searchBy' => isset($info['searchBy']) ? $info['searchBy'] : null,
            'idOfSheet' => isset($info['idOfSheet']) ? $info['idOfSheet'] : null
        ]);
        
        $res = [
            'description' => isset($description['description']) ? $description['description'] : $description
        ];

        if (isset($description['lastRow'])) {
            $res['nextFromRow'] = $description['lastRow'];
        }

        if (isset($description['pages'])) {
            $res['pages'] = $description['pages'];
            $res['currentPageId'] = $description['currentPageId'];
        }
        
        if (isset($info['searchBy'])) {
            $res['resultBy'] = $info['searchBy'];
        }

        $response = new JsonResponse($res);
        return $response;
    }

    public function convertAction(Request $request)
    {
        $generator = new FileGenerator($this->getDoctrine());

        ini_set('memory_limit','100M');
        set_time_limit(3600 * 5);
        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////
        //File with all information
        $form6 = $this->get('form.factory')->createNamedBuilder('form_for_all_info')
            ->add('file_with_all_info', FileType::class, array('label' => 'File with all information'))
            ->add('send', SubmitType::class, array('label' => 'Update information'))
            ->getForm();        

        $form6->handleRequest($request);

        if ($form6->isSubmitted()) {

            $fileName = $form6->get('file_with_all_info')->getData();
            
            if ($generator->uploadFile($fileName, $generator::$TYPE_OF_FILE['ALL_INFO'])) {
                $generator->autoUpdateCategoriesForProducts();
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'File with all information was uploaded !');
            } else {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', 'Error has been occcured, might file has wrong format !');
            }
        }
        
        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////
        //File with payment info
        $form7 = $this->get('form.factory')->createNamedBuilder('form_for_payment_info')
            ->add('file_with_all_info', FileType::class, array('label' => 'File with payment information'))
            ->add('send', SubmitType::class, array('label' => 'Update information'))
            ->getForm();        

        $form7->handleRequest($request);

        if ($form7->isSubmitted()) {

            $fileName = $form7->get('file_with_all_info')->getData();
            
            if ($generator->uploadFile($fileName, $generator::$TYPE_OF_FILE['PAYMENT_INFO'])) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'File with payment information was uploaded !');
            } else {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', 'Error has been occcured, might file has wrong format !');
            }
        }
        
        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////
        //Stock information
        $form8 = $this->get('form.factory')->createNamedBuilder('form_for_stock_info')
            ->add('file_with_all_info', FileType::class, array('label' => 'File with stock information'))
            ->add('send', SubmitType::class, array('label' => 'Upload information'))
            ->getForm();        

        $form8->handleRequest($request);

        if ($form8->isSubmitted()) {
            $fileName = $form8->get('file_with_all_info')->getData();
            
            if ($generator->uploadFile($fileName, $generator::$TYPE_OF_FILE['STOCK'])) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'File with stock information was uploaded !');
            } else {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', 'Error has been occcured, might file has wrong format !');
            }
        }
        
        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////
        //prices for stock product
        $form9 = $this->get('form.factory')->createNamedBuilder('form_for_stock_prices_info')
            ->add('file_with_all_info', FileType::class, array('label' => 'File with Prices'))
            ->add('send', SubmitType::class, array('label' => 'Update information'))
            ->getForm();        

        $form9->handleRequest($request);

        if ($form9->isSubmitted()) {
            $fileName = $form9->get('file_with_all_info')->getData();
            
            if ($generator->uploadFile($fileName, $generator::$TYPE_OF_FILE['PRODUCT_PRICES'])) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'File with prices was uploaded !');
            } else {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', 'Error has been occcured, might file has wrong format !');
            }
        }
        
        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////
        //qunatity of products which need
        $form10 = $this->get('form.factory')->createNamedBuilder('form_for_need_product')
            ->add('file_with_all_info', FileType::class, array('label' => 'File with products which need'))
            ->add('send', SubmitType::class, array('label' => 'Upload information'))
            ->getForm();        

        $form10->handleRequest($request);

        if ($form10->isSubmitted()) {
            $fileName = $form10->get('file_with_all_info')->getData();
            
            if ($generator->uploadFile($fileName, $generator::$TYPE_OF_FILE['NEED_PRODUCTS'])) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'File with products which need was uploaded !');
            } else {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', 'Error has been occcured, might file has wrong format !');
            }
        }

        ///////////////////////////////////////////////////
        ///////////////////////////////////////////////////

        $customTemplate = $this->get('kernel')->getSrcDir() . '/AppBundle/Admin/views/export.converted.html.twig';
        return $this->render($customTemplate, array(
            'action' => 'convert',
            'form6' => $form6->createView(),
            'form7' => $form7->createView(),
            'form8' => $form8->createView(),
            'form9' => $form9->createView(),
            'form10' => $form10->createView(),
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ), null);
    }


    private function baseConvertationOfInformatioForProducts($fileName)
    {
        $rowNum = 0;
        $res = array();
        $z = 0;
        if (($handle = fopen($fileName, 'r')) !== false) {
            while($data = fgetcsv($handle, 0, ',')) {
                $rowNum++;

                if ($rowNum > 1) {
                    $code = trim($data[2]);

                    if (!isset($res[$code])) {
                        $res[$code] = array(
                            'price' => 0,
                            'quantity' => 0,
                            'description' => '',
                            'category' => '',
                            'provider' => '',
                            'code' => $code
                        );
                    }

                    $description = trim($data[3]);
                    $category = trim($data[4]);
                    $quantity = (int) $data[5];
                    $price = (float) str_replace(',', '.' ,$data[8]);
                    
                    /*
                    if ($price < 0) {
                        echo '|' . $res[$code]['price'] . '<==';
                    }
                    */
                    $before = $res[$code]['price'];
                    $res[$code]['price'] += $price;
                    /*
                    if ($price < 0) {
                        echo $price . ''. '==>'. $res[$code]['price'] . '|';
                    }
                    */
                    $res[$code]['quantity'] += $quantity;
                    $res[$code]['description'] = $description;
                    $res[$code]['category'] = $category;
                    $res[$code]['provider'] = mb_substr($code, 0, 3, 'UTF-8');
                    $z += $price;
                } elseif (count($data) !== 9) {
                    return false;
                }
            }
        } 

        
        return $res;
    }

    private function getFile($info, $key)
    {
        require_once $this->get('kernel')->getSrcDir(). '/../export/export_load.php';

        switch ($key) {
            case 'ExportForReports':
                $exportHelper = new \Helper\Export\Export($info, 'ExportForReports', 0);
            break;
            case 'file_by_description':
                /**
                * @todo Will need to change name of export
                */
                $exportHelper = new \Helper\Export\Export($info, 'convertedFileFromOfflineStores', 0);
            break;
        }

        $exportHelper->getExport();
    }

}
