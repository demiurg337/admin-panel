<?php

namespace AppBundle\Controller\Admin;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\TimeOfSeller;

class TimeOfSellerController extends Controller
{
    public function addedTimeAction(Request $request)
    {
        $doctrine = $this->getDoctrine();
        $mng = $doctrine->getRepository('AppBundle:TimeOfSeller');

        $time = new \DateTime();
        $from = new \DateTime($time->format('Y-m') . '-1 00:00:00');
        $to = new \DateTime($time->format('Y-m-t') . ' 00:00:00');

        $typesOfPresentism = $this->getDoctrine()
            ->getRepository('AppBundle:TypeOfPresentism')
            ->findAll();

        $timeOfSellerMng = $this->getDoctrine()
            ->getRepository('AppBundle:TimeOfSeller');
        $records = $timeOfSellerMng->getTimeOfSellers($from, $to);

        $statistic = $timeOfSellerMng->calculateSalaryInfoByTimeOfSeller($records, $typesOfPresentism);
        /*
        echo '<pre>';
        var_dump($statistic);
        echo '</pre>';
        die;
        */ 

        ////////////////////////


        $customTemplate = $this->get('kernel')->getSrcDir() . '/AppBundle/Admin/views/TimeOfSeller/addedTime.html.twig';
        return $this->render($customTemplate, array(
            'statistic' => $statistic,
            'typesOfPresentism' => $typesOfPresentism,
            'records' => $records,
            'action' => 'addedTime',
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
        ), null);
    }

    public function saveTypeOfPresentismAction(Request $request)
    {
        $info =$request->query->all();

        $timeId = $info['timeId'];
        $newPresentismId = $info['newPresentismId'];
        $em = $this->getDoctrine()->getManager();
        $timeOfSeller = $em->getRepository('AppBundle:TimeOfSeller')
            ->find($timeId);

        if (!$timeOfSeller) {
            throw $this->createNotFoundException(
                'No product found for id '.$timeId
            );
        }

        $timeOfSeller->setTypeOfPresentismId($newPresentismId);
        $em->flush(); 
        $response = new JsonResponse([
            'code' => 200
        ]);
        return $response;
    }
}
