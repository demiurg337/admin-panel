<?php 
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\UserPermission;
class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('login')
            ->add('password')
            ->add('userPermission', EntityType::class, [
                'class' => UserPermission::class,
                'choice_label' => 'description',
            ])
            ->add('isActive');

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('login')
            ->add('password')
            ->add('isActive');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('login')
            ->add('password')
            ->add('userPermission.description', null, ['label' => 'Type'])
            ->add('isActive')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ]
            ]);
    }
}

