<?php 
namespace AppBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Store;
use AppBundle\Entity\Seller;
use AppBundle\Entity\StoreOrder;

use Sonata\CoreBundle\Form\Type\DateTimeRangePickerType;
use Sonata\CoreBundle\Form\Type\ImmutableArrayType;


use Sonata\AdminBundle\Form\Type\ModelListType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
class StoreOrderProductAdmin extends AbstractAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('getStatisticFromDatabase')
            ->add('getPreviewOfExport')
            ->add('updateStatistic')
            ->add('getProductForFilter')
            ->add('convert');
    }


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('code')
            ->add('color')
            ->add('size')
            ->add('price')

            ->add('store', EntityType::class, [
                'class' => Store::class,
                'choice_label' => 'externalStoreId',
            ])
            ->add('userPermission', EntityType::class, [
                'class' => Seller::class,
                'choice_label' => 'description',
            ])
            /*
            ->add('order', ModelListType::class, [
                'class' => StoreOrder::class,
                'choice_label' => 'storeOrderExternalId',
            ])
            ->add('order', EntityType::class, [
                'class' => StoreOrder::class,
                'choice_label' => 'storeOrderExternalId',
            ])
            */

             ->add('order', ModelAutocompleteType::class, [
                'property' => 'storeOrderExternalId',
                'multiple' => true
            ])

            ->add('quantity');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('date', 'doctrine_orm_datetime_range', array('field_type'=>'sonata_type_datetime_range_picker',))
            ->add('store', null, array(
                'show_filter' => true
            ), EntityType::class, [
                'class' => Store::class,
                'choice_label' => 'externalStoreId',
                'multiple' => true,
            ])
            ->add('seller', null, array(
                'show_filter' => true
            ), EntityType::class, [
                'class' => Seller::class,
                'choice_label' => 'name',
                'multiple' => true,
            ])


            ->add('articul', null, [
            ]);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('store', null, array('associated_property' => 'externalStoreId'))
            ->add('store.externalStoreId', null, ['label' => 'Code of store'] )
            ->add('seller.name')
            ->add('order.storeOrderExternalId', null, ['label' => 'Id of order'])
            ->add('code')
            ->add('color')
            ->add('size')
            ->add('price')
            ->add('date', 'date')
            ->add('quantity');
    }
}

