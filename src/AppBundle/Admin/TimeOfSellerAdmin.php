<?php 
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Sonata\CoreBundle\Form\Type\DateTimeRangePickerType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use AppBundle\Entity\Seller;
use AppBundle\Entity\TypeOfDay;
use AppBundle\Entity\TypeOfPresentism;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


use Sonata\AdminBundle\Route\RouteCollection;
class TimeOfSellerAdmin extends AbstractAdmin
{

    protected $datagridValues = [
        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('saveTypeOfPresentism')
            ->add('addedTime');
    }

    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();

        $actions['import'] = [
            'label'              => 'Added time',
            'url'                => $this->generateUrl('addedTime'),
            'icon'               => 'list',
        ];

        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $config = [
            'format' => 'yyyy-MM-dd HH:mm:ss',
            'dp_default_date' => (new \DateTime())->format('Y-m-d') . ' 00:00:00',
            'dp_min_date' => (new \DateTime())->format('Y-m-d') . ' 00:00:00',
            'dp_max_date' => (new \DateTime())->format('Y-m-d') . ' 23:59:59',
            'dp_pick_time' => true,
            'dp_days_of_week_disabled' => true,
            'dp_disabled_dates' => true
        ];
        

        
        $customSellers = $this->getRestrictedSellers();

        $formMapper
            ->add(
                'startOfEarlyShift', 
                DatePickerType::class, 
                array_merge(['label' => 'Start of early shift'], $config)
            )
            ->add(
                'endOfEarlyShift', 
                DatePickerType::class, 
                array_merge(['label' => 'End early shift'], $config)
            )

            ->add(
                'startOfLateShift', 
                DatePickerType::class, 
                array_merge(['label' => 'Start early shift'], $config)
            )
            ->add(
                'endOfLateShift', 
                DatePickerType::class, 
                array_merge(['label' => 'End early shift'], $config)
            );

        if (count($customSellers) <= 0) {
            $formMapper->add('seller', EntityType::class, [
                    'label' => 'Seller',
                    'class' => Seller::class,
                    'choice_label' => 'name',
                    'required'   => true,
                ]);
        } else {
            $choices = [];
            foreach ($customSellers as $sell) {
                $choices[$sell->getName()] = $sell;
            }

            $formMapper->add('seller', ChoiceType::class, array(
                    'choices'  => $choices,
                    'required'   => true,
                )
            );
        }


        $formMapper
            ->add('typeOfDay', EntityType::class, [
                'label' => 'Type of day',
                'class' => TypeOfDay::class,
                'choice_label' => 'name',
            ])
            ->add('typeOfPresentism', EntityType::class, [
                'label' => 'Type of day',
                'class' => TypeOfPresentism::class,
                'choice_label' => 'name',
            ])
            ->add('isWithSertificate')
        ;
    }


    /**
    * @todo Need to move it to better place
    */
    public function getRestrictedSellers() {
        $currentUser = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $entityMng = $this->getConfigurationPool()->getContainer()->get('doctrine');
        $restrictionMng = $entityMng->getRepository('AppBundle:UserRestrictionByStore');
        $restrictions = $restrictionMng->findBy(['user' => $currentUser]);

        $stores = [];
        $storeIds = [];
        $customSellers = [];
        if (count($restrictions) > 0) {
            foreach ($restrictions as $restr) {
                $store = $restr->getStore();
                $stores[] = $store;
                $storeIds[] = $store->getStoreId();
            }

            /*
            @todo Need to find other way for getting sellers ! (easier then by Doctrine)
            */
            $dbConn = $entityMng->getManager()->getConnection()->getWrappedConnection();
            $st = $dbConn->prepare(
                'SELECT 
                    DISTINCT seller_id
                FROM
                    store_order_product
                WHERE
                    store_id IN ('. implode(',', $storeIds) .')'
            );

            if (!$st->execute()) {
                throw new \Exception('Cant get stores');
            }


            $sellers = $st->fetchAll(\PDO::FETCH_OBJ);
            $sellerIds = [];
            foreach ($sellers as $seller) {
                $sellerIds[] = $seller->seller_id;
            }

            
            $sellersMng = $entityMng->getRepository('AppBundle:Seller');
            $customSellers = $sellersMng->findBy(['seller_id' => $sellerIds]);
        }

        return $customSellers;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $customSellers = $this->getRestrictedSellers();
        $sellerIds = [];
        foreach ($customSellers as $sell) {
            $sellerIds[] = $sell->getSellerId();
        }

        if (count($sellerIds) > 0) {
            $query->andWhere($query->getRootAliases()[0].'.sellerId in (' . implode(',', $sellerIds) . ') ');
        }
        
        return $query;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('startOfEarlyShift')
            ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('seller.name')
            ->add('startOfEarlyShift')
            ->add('endOfEarlyShift')
            ->add('startOfLateShift')
            ->add('endOfLateShift')
            ->add('typeOfDay.name', null, ['label' => 'Type of day'])
            ->add('typeOfPresentism.name', null, ['label' => 'Type of presentism'])
            ->add('isWithSertificate')
            ;
    }
}

