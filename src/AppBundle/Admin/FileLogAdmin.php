<?php 
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class FileLogAdmin extends AbstractAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'edit']);
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('timeOfStarting', 'doctrine_orm_datetime_range', array('field_type'=>'sonata_type_datetime_range_picker'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('logId')
            ->add('type.name', null, ['label' => 'Type of import'])
            ->add('timeOfStarting', null, ['label' => 'Start time'])
            ->add('timeOfSaving', null, ['label' => 'Finish time'])
            ->add('isAllOk', null, ['label' => 'Is all ok?']);
    }
}

