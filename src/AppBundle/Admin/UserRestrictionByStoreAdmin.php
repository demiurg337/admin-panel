<?php 
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Store;
use AppBundle\Entity\User;
class UserRestrictionByStoreAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'login',
                'label' => 'Administrator'
            ])
            ->add('store', EntityType::class, [
                'class' => Store::class,
                'choice_label' => 'externalStoreId',
                'label' => 'Need to restrict by store'
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('user.login');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('user.login', null, ['label' => 'Adminstrator'])
            ->add('store.externalStoreId', null, ['label' => 'Store'])
            ->add('_action', null, [
                'actions' => [
                    'delete' => [],
                ]
            ]);
    }
}

