<?php 
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Store;

class StoreCodeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('store', EntityType::class, [
                'class' => Store::class,
                'choice_label' => 'externalStoreId',
            ])
            ->add('additionalId');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('additionalId');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('store.externalStoreId', null, ['label' => 'Store'])
            ->add('additionalId', null, ['label' => 'Code of store'])
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => [],
                ]
            ]);
    }
}

