<?php
namespace Helper\Export;

class productParcelsInfo extends ExportType
{
    private $model;
    private $dateFrom;
    private $dateTo;
    private $lastProcessed;

    protected $exportName = 'productParcelsInfo';
    protected $fieldsNames = array(
        'internal ID',
        'external ID',
        'order ID',
        'ID on service if exists'
    );

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;

        $this->dateFrom = date('Y-m-d H:i:s', strtotime($data['from']));
        $this->dateTo = date('Y-m-d H:i:s', strtotime($data['to']));

        $this->model = $CORE->get('DeliveryInfoContainer')->builder;
    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getAllDeliveryInfo($this->dateFrom, $this->dateTo);

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }


    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $row[] = $dataRow->internal_parcel_id;
            $row[] = '#'.$dataRow->external_parcel_id;
            $row[] = $dataRow->orderId;
            $row[] = $dataRow->parcel_id_shipnow;

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}