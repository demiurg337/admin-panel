<?php
namespace Helper\Export;

class Products extends ExportType
{
    private $model;
    private $supplierModel;
    private $lastProcessed;
    private $isSuperAdmin;
    private $exportType;
    private $step = 2000;
    private $start = 0;


    protected $exportName = 'products';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        ini_set('memory_limit','1G');
        set_time_limit(3600);

        global $CORE;
        $this->isSuperAdmin = $CORE->get('Security')->isHavePermissions('SuperAdministrator');
        $this->exportType = $data['exportType'];
        $this->model = $CORE->get('ProductContainer')->builder;
        $this->supplierModel = $CORE->get('SupplierContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();

    }


    public function getFieldsValues()
    {
        $data = $this->getData();

        if($data === null){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
        $fieldsNames = array();

        $fieldsNames[] = 'ID';
        $fieldsNames[] = 'TITLE';
        $fieldsNames[] = 'Description';
        $fieldsNames[] = 'SKU';

        if ((int) COUNTRY_ID === 1) {
            $fieldsNames[] = 'SHIPNOW SKU';
        }

        if($this->isSuperAdmin){
            $fieldsNames[] = 'Purchase price';
        }

        $fieldsNames[] = 'Price';
        $fieldsNames[] = 'Discount';
        $fieldsNames[] = 'Sex';
        $fieldsNames[] = 'Stock';
        $fieldsNames[] = 'All quantity';
        $fieldsNames[] = 'Link';
        $fieldsNames[] = 'Category ID(s)';
        $fieldsNames[] = 'Category Name(s)';
        $fieldsNames[] = 'Brand';
        $fieldsNames[] = 'Stock type';
        $fieldsNames[] = 'Size grid ID';
        $fieldsNames[] = 'Need update';
        $fieldsNames[] = 'Suppliers';
        $fieldsNames[] = 'Name of brands';
        $fieldsNames[] = 'Estación';
        $fieldsNames[] = 'Sync with ShipNow?';


        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {


            $suppliers = $this->supplierModel->getProductSuppliers($dataRow->id);

            foreach ($suppliers as &$supplier) {
                $supplier = $supplier->name;
            }
            $row = Array();

            $row[] = $dataRow->id;
            $row[] = strip_tags($dataRow->name);
            $row[] = strip_tags($dataRow->description);
            $row[] = $dataRow->sku ;

            if ((int) COUNTRY_ID === 1) {
                $row[] = $dataRow->shipNowProductSkus ;
            }

            if ($this->isSuperAdmin) {
                $row[] = $dataRow->buy_price;
            }
            $row[] = $dataRow->price ;
            $row[] = $dataRow->discount;
            $row[] = $dataRow->sex;
            
            $stockDescription = $dataRow->stock;
            $stockDescription =  self::decorateProductColor($stockDescription, true);
            $stockDescription =  self::decorateProductSize($stockDescription, true);

            $stockDescription =  str_replace(';',' | ', $stockDescription);
            $stockDescription =  str_replace(',',' ', $stockDescription);

            $row[] = $stockDescription;
            $row[] = $dataRow->allQuantity;
            $row[] = $dataRow->img;
            $row[] = $dataRow->categoryIds;
            $row[] = $dataRow->categoryNames;
            $row[] = $dataRow->brand;
            $row[] = $dataRow->stock_type;
            $row[] = $dataRow->size_grid_id;
            $row[] = $dataRow->namesOfCampaigns;
            $row[] = 0;
            $row[] = implode(',', $suppliers);
            $row[] = $dataRow->seasonTitle;
            $row[] = ($dataRow->isNotSyncStockWithShipNow) ? 'no':'yes'; // Sync with ShipNow?

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

    private function getData(){


        if($this->lastProcessed){
            return null;
        }


        $data = $this->model->getProductsForCSV($this->start, $this->step, $this->exportType);

        if(!$data){
            return null;
        }

        if(count($data) < $this->step){
            $this->lastProcessed = true;
        }else{
            $this->start = $this->start + $this->step;
        }

        return $data;
    }

}
