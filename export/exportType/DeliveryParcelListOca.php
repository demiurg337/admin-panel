<?php
namespace Helper\Export;

class DeliveryParcelListOca extends ExportType
{
    private $model;
    private $lastProcessed;
    private $listId;

    protected $firstRow = '';
    protected $exportName = 'retiro_en_sucursal_oca';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;

        $this->model = $CORE->get('DeliveryInfoContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();
        $this->listId = $data['listId'];

    }

    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getParselsOfLists(array(
            'listId' => $this->listId,
        ));

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
         $fieldsNames = array();

         $fieldsNames[] = 'NAME SURNAME';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'SURCURSAL';
         $fieldsNames[] = 'PHONE';
         $fieldsNames[] = 'EMAIL';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'INTNAL PARCEL ID';
         $fieldsNames[] = 'Guia operativa';
         $fieldsNames[] = 'Is sended (yes/no)';

        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $sendInfo = json_decode($dataRow->send_info_json, true);

            $row[] = $sendInfo['name'] . ' ' . $sendInfo['surname'];
            $row[] = '0';
            $row[] = $dataRow->surcursalCode;
            $row[] = $sendInfo['phone'];
            $row[] = self::hiddenEmail($sendInfo['email']) ;
            $row[] = '1';
            $row[] = '0.499';
            $row[] = '5';
            $row[] = '4';
            $row[] = '1';
            $row[] = '0';
            $row[] = $dataRow->internal_parcel_id;
            $row[] = '96811';
            $row[] = ((int) $dataRow->was_sended === 1 ? 'YES' : 'NO');

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}