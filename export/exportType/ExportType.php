<?php
namespace Helper\Export;

Abstract class ExportType {

    protected $firstRow = '';

    abstract function getFieldsValues();

    public function getFirstRow(){
        return $this->firstRow;
    }

    public function getFieldsNames()
    {
        return $this->fieldsNames;

    }

    public function getExportName(){
        return $this->exportName;
    }

    public function getExportFormat(){
        return $this->exportFormat;
    }

    public static function hiddenEmail($emailSource){
        $email = $emailSource;

        $strLen =strlen($emailSource);

        if ($strLen>12) {
            $first=substr($emailSource, 0, 6);
            $last=substr($emailSource, $strLen - 6, 6);
            $stars='';
            for ($i=0; $i < $strLen-12; $i++) {
                $stars.='*';
            }
            $email=$first.$stars.$last;
        }

        return $email;
    }

    public static function decorateProductColor($str, $isInGroup = false){

        if((int) COUNTRY_ID === 1){

            if($isInGroup) {
                if ($str === null || trim($str) === '') {
                    return ',No color,0;';
                }

                $result = explode(';', $str);

                foreach($result as &$one){
                    if(trim($one) === '') {
                        $one = '';
                        continue;
                    }

                    $res = explode(',',$one);
                    if(isset($res[1]) && trim($res[1]) == ''){
                        $res[1] = 'No color';
                        $one = implode(',',$res);
                    }

                }

                $str  = implode(';',$result);
            }else if(trim($str) == ''){
                $str = 'No color';
            }
        }
        return $str;
    }

    public static function decorateProductSize($str, $isInGroup = false){

        if((int) COUNTRY_ID === 1){
            if($isInGroup) {

                $result = explode(';', $str);

                foreach($result as &$one){
                    if(trim($one) === '') {
                        $one = '';
                        continue;
                    }

                    $res = explode(',',$one);
                    if(isset($res[0]) && trim($res[0]) == ''){
                        $res[0] = 'UNICO';
                        $one = implode(',',$res);
                    }

                }

                $str  = implode(';',$result);
            }else if(trim($str) == ''){
                $str = 'UNICO';
            }
        }
        return $str;
    }
}
