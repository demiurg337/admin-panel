<?php

namespace Helper\Export;

/**
* @todo need to change name of export
*/
class ExportOfConvertedFileStores extends ExportType
{
    private $data;

    protected $firstRow = '';
    protected $exportName = 'offline_stores';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){
        $this->fieldsNames = $data[0];
        array_shift($data);
        $this->data = $data;
    }

    private $isFirst = false;
    public function getFieldsValues()
    {
        if (!$this->isFirst) {
            $this->isFirst = true;
            return $this->data;
        }

        return null;
    }
}
