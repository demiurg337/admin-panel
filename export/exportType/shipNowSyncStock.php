<?php
namespace Helper\Export;

class shipNowSyncStock extends ExportType
{
    private $model;
    private $lastProcessed;
    private $date;


    protected $exportName = 'shipNowSyncStock';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        ini_set('memory_limit','1G');
        set_time_limit(3600);

        global $CORE;

        if (isset($data['date']) ) {
            $this->date = $data['date'];
        } else {
            $this->date = date('Y-m-d', time());
        }

        $this->model = $CORE->logger()->builder;;
        $this->fieldsNames = $this->setFieldsNames();

    }


    public function getFieldsValues()
    {
        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getShipNowSyncStockLog($this->date);

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
        $fieldsNames = array();

        $fieldsNames[] = 'SKU ShipNow';
        $fieldsNames[] = 'En Mano';
        $fieldsNames[] = 'Comprometido';
        $fieldsNames[] = 'Pendientes';
        $fieldsNames[] = 'Cambios Pending';
        $fieldsNames[] = 'Stock Actualizado';
        $fieldsNames[] = 'Producto Activo en CP';
        $fieldsNames[] = 'date';
        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $one) {

            if (isset($one['syncInfo'])) {
                foreach ($one['syncInfo'] as $item){

                    $shipNowInfo = $item['shipNowInfo'];
                    $stockInfo = $item['stockInfo'];

                    $row = Array();
                    $row[] = $shipNowInfo['external_reference'];
                    $row[] = $shipNowInfo['stock']['on_hand'];
                    $row[] = $shipNowInfo['stock']['committed'];
                    $row[] = $stockInfo['quantityPending'];
                    $row[] = $stockInfo['quantityExchange'];
                    $row[] = $stockInfo['newStock'];
                    $row[] = ((int)$item['activeCampaigns'] === 1) ? 'yes':'no' ;
                    $row[] = date('Y-m-d H:i:s',$one['time']) ;

                    $fieldsValues[] = $row;
                }
            }
        }
        return $fieldsValues;
    }

}
