<?php
namespace Helper\Export;

class DeliveryParcelListHome extends ExportType
{
    private $model;
    private $lastProcessed;
    private $listId;

    protected $firstRow = '';
    protected $exportName = 'parcel_list';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;

        if( (int) $data['deliveryTypeId'] === 1){

            $this->exportName =  'envío_a_domicilio_oca';

        }else if( (int) $data['deliveryTypeId'] === 4){

            $this->exportName =  'envío_Etrans';

        }

        $this->model = $CORE->get('DeliveryInfoContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();
        $this->listId = $data['listId'];

    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getParselsOfLists(array(
            'listId' => $this->listId,
        ));


        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
         $fieldsNames = array();

         $fieldsNames[] = 'NAME SURNAME';
         $fieldsNames[] = 'fixed';
         $fieldsNames[] = 'ADDRESS';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'SPACE';
         $fieldsNames[] = 'SPACE';
         $fieldsNames[] = 'ZIP';
         $fieldsNames[] = 'CITY';
         $fieldsNames[] = 'STATE';
         $fieldsNames[] = 'PHONE';
         $fieldsNames[] = 'EMAIL';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'INTERNAL PARCEL ID';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Is sended (yes/no)';
        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $sendInfo = json_decode($dataRow->send_info_json, true);

            $row[] = $sendInfo['name'] . ' ' . $sendInfo['surname'];
            $row[] = '0';
            $row[] =  $dataRow->address;
            $row[] = '0';
            $row[] = '';
            $row[] = '';
            $row[] = $sendInfo['zip'];
            $row[] = $sendInfo['city'];
            $row[] = $sendInfo['state_name'];
            $row[] = $sendInfo['phone'];
            $row[] = self::hiddenEmail($sendInfo['email']);
            $row[] = 'BILLINGHURST';
            $row[] = '2476';
            $row[] = 'PB';
            $row[] = 'A';
            $row[] = '48072094';
            $row[] = '1425';
            $row[] = 'CABA';
            $row[] = 'Buenos Aires';
            $row[] = 'Logistica CP';
            $row[] = '0.49';
            $row[] = '5';
            $row[] = '4';
            $row[] = '1';
            $row[] = '0';
            $row[] = $dataRow->internal_parcel_id;
            $row[] = '96810';
            $row[] = 'logistica@clubpoint.com';
            $row[] = '1';
            $row[] = '3';
            $row[] = ((int) $dataRow->was_sended === 1 ? 'YES' : 'NO');

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}