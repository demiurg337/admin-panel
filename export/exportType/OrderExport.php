<?php
namespace Helper\Export;

class OrderExport extends ExportType
{
    private $model;
    private $paymentModel;
    private $specialDate;
    private $isSuperAdmin;
    private $dateFrom;
    private $dateTo;
    private $statuses = array();
    private $maxOrderId;
    private $fromId = 0;
    private $toId = 0;
    private $step = 5000;
    private $lastProcessed;
    private $imgPath;

    protected $firstRow = 'sep=';
    protected $exportName = 'order';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;


        $this->dateFrom = date("Y-m-d H:i:s", strtotime($data['from_date']));
        $this->dateTo = date("Y-m-d H:i:s", strtotime($data['to_date']));
        $this->parameters = array();


        if (isset($data['order_statuses'])) {
            $this->statuses['order_statuses'] = $data['order_statuses'];
        }

        if (isset($data['product_statuses'])) {
            $this->statuses['product_statuses'] = $data['product_statuses'];
        }

        if (isset($data['order_labels'])) {
            $this->parameters['order_labels'] = $data['order_labels'];
        }

        $this->model = $CORE->get('OrderContainer')->builder;
        $this->paymentModel = $CORE->get('Payments')->builder;
        $this->specialDate = $CORE->get('SpecialDatesContainer')->builder;
        $this->isSuperAdmin = $CORE->get('Security')->isHavePermissions('SuperAdministrator');

        $this->maxOrderId = $this->model->GetOrdersMaxId();

        $this->fieldsNames = $this->setFieldsNames();
        $this->imgPath = 'http://'.$_SERVER['SERVER_NAME'].'/data/img/products/big/';

    }

    public function getFieldsValues()
    {
        $data = $this->getData();

        if($data === null){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }

    private function setFieldsNames(){
        $fieldsNames = array();
        $fieldsNames[] = 'ORDER ID AND SKU AND COLOR AND SIZE';
        $fieldsNames[] = 'PRODUCT ORDER ID';
        $fieldsNames[] = 'PARCEL ID_ORDER ID';
        $fieldsNames[] = 'ORDER ID';
        $fieldsNames[] = 'COMMENT';
        $fieldsNames[] = 'USER ID';
        $fieldsNames[] = 'NAME SURNAME';
        $fieldsNames[] = 'EMAIL';
        $fieldsNames[] = 'PAYMENT METHOD';
        $fieldsNames[] = 'PRICE * QUANTITY';

        if ($this->isSuperAdmin) {
            $fieldsNames[] = 'PURCHASE PRICE';
        }

        $fieldsNames[] ='TOTAL PRICE';
        $fieldsNames[] ='DISCOUNT';
        $fieldsNames[] = 'BANK_DISCOUNT';
        $fieldsNames[] = 'DELIVERY PRICE';
        $fieldsNames[] = 'QUANTITY';
        $fieldsNames[] = 'DELIVERY TYPE';
        $fieldsNames[] = 'DELIVERY DATES';
        $fieldsNames[] = 'BONUSES USED';
        $fieldsNames[] = 'ORDER STATUS';
        $fieldsNames[] = 'PRODUCT STATUS NAME';
        $fieldsNames[] = 'ORDER DATE';
        $fieldsNames[] = 'PRODUCT ID';
        $fieldsNames[] = 'SKU';

        $fieldsNames[] = 'Company action SKU';
        $fieldsNames[] = 'PRODUCT NAME';
        $fieldsNames[] = 'PRODUCT COLOR';
        $fieldsNames[] = 'PRODUCT SIZE';
        $fieldsNames[] = 'PRODUCT CATEGORIES IDs';
        $fieldsNames[] = 'PRODUCT CATEGORIES NAMES';
        $fieldsNames[] = 'ADDRESS';
        $fieldsNames[] = 'STATE';
        $fieldsNames[] = 'CITY';

        if ((int) COUNTRY_ID === 1) {
            $fieldsNames[] = 'SURCURSAL';
        }

        $fieldsNames[] = 'ZIP';
        $fieldsNames[] = 'PHONE';
        $fieldsNames[] = 'IMAGE LINK';
        $fieldsNames[] = 'Order data id';
        $fieldsNames[] = 'DOCUMENT EXPORT DATE';
        $fieldsNames[] = 'Stock type';
        $fieldsNames[] = 'Campaign end date';
        $fieldsNames[] = 'Brand name';
        $fieldsNames[] = 'Labels of order';
        $fieldsNames[] = 'PAYMENT DATE';
        $fieldsNames[] = 'PS METHOD';
        $fieldsNames[] = 'PRODUCT SUPPLIER';

        return $fieldsNames;
    }

    private function prepareFieldsValues($orders)
    {

        $nowTime = new \DateTime();

        $fieldsValues = array();

        foreach ($orders as $u) {

            $row = Array();
            $deliveryDateRange = ' - ';

            if ($u['delivery_date'] != '0000-00-00 00:00:00') {

                $deliveryDateRange = date('d/m/Y', strtotime($u['delivery_date'])) . ' - ';

                if ($u['delivery_end_date']) {
                    $deliveryDateRange .= date('d/m/Y', strtotime($u['delivery_end_date']));
                }
            }

            if ($u['name'] != '' OR $u['surname'] != '') {
                $nameSurname = $u['name'] . ' ' . $u['surname'];
            } else {
                $nameSurname = 'New user, didn\'t fill';
            }

            $address = $u['address'];
            $state = $u['state'];
            $city = $u['city'];
            $zip = $u['zip'];
            $phone = $u['phone'];
            $delivery_price = $u['delivery_price'];

            $priceQuantity = (int)($u['discount'] * (int)$u['quantity']);

            if ((int)$u['return_status_id'] === 2) {
                $u['status'] = 'Devoluvcion del producto';
            } elseif ((int)$u['return_status_id'] === 3) {
                $u['status'] = 'Devoluvcion del producto';
            } elseif ((int)$u['return_status_id'] === 4) {
                $u['status'] = 'Devolucion del import';
            }


            $parcelIdOrderId = '';
            if ((int)$u['parcel_id'] > 0) {
                $parcelIdOrderId = $u['parcel_id'] . '_' . $u['order_id'];
            }


            $row[] = $u['order_id'] . '_' . $u['scu'] . '_' . $u['product_color'] . '_' . $u['product_size'];
            $row[] = $u['order_id'] . '_' . $u['orderDataId'];
            $row[] = $parcelIdOrderId;
            $row[] = $u['order_id'];
            $row[] = $u['comment'];
            $row[] = $u['user_id'];
            $row[] = $nameSurname;
            $row[] = self::hiddenEmail($u['email']);;

            $psType = $u['payment_method'];
            $psDate = $u['payment_date'];
            $psMethod = "no info";

            $paymentInf = $this->paymentModel->getPaymentData($u['order_id']);
            $request = @json_decode($paymentInf->request);

            if (is_object($paymentInf)) {
                if ((int)$paymentInf->payment_id === 1) {
                    //Mercago
                    $psMethod = @$request->collection->payment_type;
                    $psDate = @$request->collection->money_release_date;
                } else if ((int)$paymentInf->payment_id == 16) {
                    //Khipu - not have any data about payment method
                } else if ((int)$paymentInf->payment_id == 20) {
                    // TodoPago - we have data, but do not have infoabout payment
                    if (isset($request->requestPayment)) {
                        $psDate = @$request->requestPayment->Payload->Answer->DATETIME;
                    }
                }
            }

            $row[] = $psType;
            $row[] = $priceQuantity;

            if ($this->isSuperAdmin) {
                $row[] = $u['purchase_price'];
            }

            $row[] = $u['overallPrice'];
            $row[] = $u['discount'];
            $row[] = $u['banco_discount'];
            $row[] = $delivery_price;
            $row[] = $u['quantity'];
            $row[] = $u['delivery_name'];
            $row[] = $deliveryDateRange;
            $row[] = $u['rewards'];
            $row[] = $u['status'];
            $row[] = $u['productStatusName'];
            $row[] = $u['order_date'];
            $row[] = $u['product_id'];
            $row[] = $u['scu'];

            $row[] = $u['companyActionSku'];
            $row[] = $u['product_name'];
            $row[] = self::decorateProductColor($u['product_color']);
            $row[] = self::decorateProductSize($u['product_size']);
            $row[] = $u['productCategoriesIds'];
            $row[] = $u['productCategoriesNames'];
            $row[] = $address;
            $row[] = $state;
            $row[] = $city;

            if ((int) COUNTRY_ID === 1) {
                $row[] = ((int) $u['curier_type'] === 2) ? '' : $u['surcursal_code'];
            }

            $row[] = $zip;
            $row[] = $phone;
            $row[] = $this->imgPath . $u['productImage'];
            $row[] = $u['orderDataId'];
            $row[] = $nowTime->format('d/m/Y H:i:s');
            $row[] = $u['stockTypeName'];
            $row[] = (($u['campaignEndDate']) ? date('d/m/Y H:i:s', strtotime($u['campaignEndDate'])) : '');
            $row[] = $u['campaignName'];
            $row[] = $u['labelsOfOrder'];
            $row[] = $psDate;
            $row[] = $psMethod;
            $row[] = $u['product_supplier'];

            $fieldsValues[] = $row;

        }

        return $fieldsValues;
    }

    private function getData(){

        $orders = null;

        while(true){

            if ($this->lastProcessed)
            {
                return null;
            }

            if ($this->toId + $this->step > $this->maxOrderId)
            {
                $this->lastProcessed = true;
            }

            $this->toId = $this->fromId + $this->step;

            $orders = $this->model->getNextOrders(
                $this->fromId,
                $this->toId,
                $this->statuses,
                $this->dateFrom,
                $this->dateTo,
                COUNTRY_ID,
                false,
                $this->parameters
            );

            $this->fromId += $this->step;
            $this->fromId++;


            if(count($orders) > 0){
                break;
            }
        }

        return $orders;
    }
}
