<?php
namespace Helper\Export;

class Credits extends ExportType
{
    private $model;
    private $dateFrom;
    private $dateTo;
    private $lastProcessed;

    protected $exportName = 'credits';
    protected $fieldsNames = array(
        'OrderID',
        'UserID',
        'NAME SURNAME',
        'Date',
        'Bonus',
        'Reason',
        'Comment',
    );

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;

        $this->dateFrom = date("Y-m-d H:i:s", strtotime($data['from_date']));
        $this->dateTo = date("Y-m-d H:i:s", strtotime($data['to_date']));

        $this->model = $CORE->get('OrderContainer')->builder;
    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getCreditsHistoryChanges($this->dateFrom, $this->dateTo);

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }


    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $row[] = $dataRow->id;
            $row[] = $dataRow->user_id;
            $row[] = $dataRow->name.' '.$dataRow->surname;
            $row[] = $dataRow->date;
            $row[] = $dataRow->rewards;
            $row[] = $dataRow->reason;
            $row[] = $dataRow->comment;

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}