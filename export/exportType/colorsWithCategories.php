<?php
namespace Helper\Export;

class colorsWithCategories extends ExportType
{
    private $model;
    private $lastProcessed;

    protected $exportName = 'Colors_With_Categories';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;

        $this->model = $CORE->get('Categories')->builder;;
        $this->fieldsNames = $this->setFieldsNames();
    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getAllColorsForExport();

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
        $fieldsNames = array();

        $fieldsNames[] = 'ID';
        $fieldsNames[] = 'TITLE';
        $fieldsNames[] = 'CATEGORIES_IDS';
        $fieldsNames[] = 'CATEGORIES_NAMES';

        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();
            $row[] = $dataRow->color_id;
            $row[] = $dataRow->title;
            $row[] = $dataRow->categoriesIds;
            $row[] = $dataRow->categoriesNames ;

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}
