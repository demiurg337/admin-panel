<?php
namespace Helper\Export;

class CourierNewChile extends ExportType
{
    private $model;
    private $dateFrom;
    private $dateTo;
    private $lastProcessed;

    protected $firstRow = 'sep=';
    protected $exportName = 'courier_info';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;


        $this->dateFrom = date("Y-m-d H:i:s", strtotime($data['courier-from_date']));
        $this->dateTo = date("Y-m-d H:i:s", strtotime($data['courier-to_date']));

        $this->model = $CORE->get('OrderContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();

    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getCouriersExcelExportInfo($this->dateFrom, $this->dateTo);

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }


    private function setFieldsNames(){
         $fieldsNames = array();

         $fieldsNames[] = 'CODIGO AGENCIA DEST.';
         $fieldsNames[] = 'RUT DESTINATARIO';
         $fieldsNames[] = 'DIGITO VERIFICADOR (DEL RUT)';
         $fieldsNames[] = 'NOMBRE DEST. O RAZÓN SOCIAL';
         $fieldsNames[] = 'A. PATERNO';
         $fieldsNames[] = 'A. MATERNO';
         $fieldsNames[] = 'DIRECCIÓN DEST.';
         $fieldsNames[] = 'NÚMERO DIRECCIÓN DEST.';
         $fieldsNames[] = 'NÚMERO DEPTO.';
         $fieldsNames[] = 'CÓDIGO COMUNA DEST.';
         $fieldsNames[] = 'TELÉFONO DEST.';
         $fieldsNames[] = 'E-MAIL DEST.';
         $fieldsNames[] = 'NOMBRE CONTACTO DEST.';
         $fieldsNames[] = 'TIPO DE ENTREGA (1)AGEN (2)DOM';
         $fieldsNames[] = 'TIPO DE PAGO (2)CTA. CTE. (3)POR PAGAR';
         $fieldsNames[] = 'NÚMERO CTA. CTE.';
         $fieldsNames[] = 'DÍGITO VERIFICADOR CTA. CTE.';
         $fieldsNames[] = 'NÚMERO CENTRO DE COSTO';
         $fieldsNames[] = '$ VALOR DECLARADO';
         $fieldsNames[] = 'CONTENIDO';
         $fieldsNames[] = 'CANTIDAD BULTOS';
         $fieldsNames[] = 'CANTIDAD SOBRES';
         $fieldsNames[] = 'KILOS';
         $fieldsNames[] = 'TIPO SERVICIO (0)NORM (1)EXPR';
         $fieldsNames[] = 'TIPO DOC. 1 (26)FAC (27)GUÍA (28)BOL';
         $fieldsNames[] = 'NÚMERO DOC. 1';
         $fieldsNames[] = 'TIPO DOC. 2 (26)FAC (27)GUÍA (28)BOL';
         $fieldsNames[] = 'NÚMERO DOC. 2';
         $fieldsNames[] = 'TIPO DOC. 3 (26)FAC (27)GUÍA (28)BOL';
         $fieldsNames[] = 'NÚMERO DOC 3';
         $fieldsNames[] = 'TIPO DOC. 4 (26)FAC (27)GUÍA (28)BOL';
         $fieldsNames[] = 'NÚMERO DOC. 4';
         $fieldsNames[] = 'TIPO DOC. 5 (26)FAC (27)GUÍA (28)BOL';
         $fieldsNames[] = 'NÚMERO DOC 5';

        return $fieldsNames;
    }


    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = $dataRow->name . ' ' . $dataRow->surname;
            $row[] = '';
            $row[] = '';
            $row[] =  $dataRow->address;
            $row[] = '1';
            $row[] = '';
            $row[] = $dataRow->city;
            $row[] = $dataRow->phone;
            $row[] = '';
            $row[] = $dataRow->name . ' ' . $dataRow->surname;
            $row[] = '2';
            $row[] = '2';
            $row[] = '19468-9';
            $row[] = '9';
            $row[] = '0';
            $row[] = $dataRow->sum;
            $row[] = 'ROPA';
            $row[] = '';
            $row[] = '';
            $row[] = '1';
            $row[] = '0';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}