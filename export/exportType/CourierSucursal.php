<?php
namespace Helper\Export;

class CourierSucursal extends ExportType
{
    private $model;
    private $dateFrom;
    private $dateTo;
    private $lastProcessed;

    protected $exportName = 'courier_export_formato_inclusion_sucursal';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;

        if (isset($data['courier-from_date']) && isset($data['courier-to_date'])){
            $this->dateFrom = date("Y-m-d H:i:s", strtotime($data['courier-from_date']));
            $this->dateTo = date("Y-m-d H:i:s", strtotime($data['courier-to_date']));
        }else {
            die('Not valid params "from date" and "to date"');
        }

        $this->model = $CORE->get('OrderContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();

    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getCouriersExcelExportInfo($this->dateFrom, $this->dateTo);

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
         $fieldsNames = array();

         $fieldsNames[] = 'Destinatario - Apellido';
         $fieldsNames[] = 'Destinatario - Nombre';
         $fieldsNames[] = 'Destino- Sucursal';
         $fieldsNames[] = 'Destinatario - Teléfono';
         $fieldsNames[] = 'Destinatario - Email';
         $fieldsNames[] = 'Paquete - Cantidad';
         $fieldsNames[] = 'Paquete - Peso [Kg]';
         $fieldsNames[] = 'Paquete - Alto [cm]';
         $fieldsNames[] = 'Paquete - Largo [cm]';
         $fieldsNames[] = 'Paquete - Ancho [cm]';
         $fieldsNames[] = 'Valor Asegurado';
         $fieldsNames[] = 'Número de remito';
         $fieldsNames[] = 'Número de Operativa';

        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $row[] = $dataRow->surname;
            $row[] = $dataRow->name;
            $row[] = $dataRow->surcursal;
            $row[] = $dataRow->phone;
            $row[] =  'Contactar por telefono';
            $row[] = '0.499';
            $row[] = '5';
            $row[] = '4';
            $row[] = '1';
            $row[] = '0';
            $row[] = '0';
            $row[] = $dataRow->id;
            $row[] = '76848';

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}