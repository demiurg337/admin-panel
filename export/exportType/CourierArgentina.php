<?php
namespace Helper\Export;

class CourierArgentina extends ExportType
{
    private $model;
    private $dateFrom;
    private $dateTo;
    private $lastProcessed;

    protected $firstRow = 'sep=';
    protected $exportName = 'courier_info';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;


        $this->dateFrom = date("Y-m-d H:i:s", strtotime($data['courier-from_date']));
        $this->dateTo = date("Y-m-d H:i:s", strtotime($data['courier-to_date']));

        $this->model = $CORE->get('OrderContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();

    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getCouriersExcelExportInfo($this->dateFrom, $this->dateTo);

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
         $fieldsNames = array();

         $fieldsNames[] = 'NAME SURNAME';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'ADDRESS';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'SPACE';
         $fieldsNames[] = 'SPACE';
         $fieldsNames[] = 'ZIP';
         $fieldsNames[] = 'CITY';
         $fieldsNames[] = 'STATE';
         $fieldsNames[] = 'PHONE';
         $fieldsNames[] = 'EMAIL';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'ORDER ID';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';

        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $row[] = $dataRow->name . ' ' . $dataRow->surname;
            $row[] = '0';
            $row[] = $dataRow->address;
            $row[] = '0';
            $row[] = '';
            $row[] = '';
            $row[] = $dataRow->zip;
            $row[] = $dataRow->city;
            $row[] = $dataRow->state;
            $row[] = $dataRow->phone;
            $row[] =  self::hiddenEmail($dataRow->email);
            $row[] =  'BILLINGHURST';
            $row[] = '2476';
            $row[] = 'PB';
            $row[] = 'A';
            $row[] = '48072094';
            $row[] = '1425';
            $row[] = 'CABA';
            $row[] = 'Buenos Aires';
            $row[] = 'Logística CP';
            $row[] = '0.499';
            $row[] = '5';
            $row[] = '4';
            $row[] = '1';
            $row[] = '0';
            $row[] = $dataRow->id;
            $row[] = '76848';
            $row[] = 'logistica@clubpoint.com';
            $row[] = '1';
            $row[] = '3';

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}