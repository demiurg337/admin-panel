<?php
namespace Helper\Export;

class MarketingExport extends ExportType
{
    private $model;
    private $paymentModel;
    private $specialDate;
    private $isSuperAdmin;
    private $dateFrom;
    private $dateTo;
    private $statuses = array();
    private $maxOrderId;
    private $fromId = 0;
    private $toId = 0;
    private $step = 5000;
    private $lastProcessed;
    private $imgPath;

    protected $firstRow = 'sep=';
    protected $exportName = 'marketing';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;

        $confirmStatusId = 3;

        $this->statuses = array(
            0 => $confirmStatusId
        ) ;


        $this->dateFrom = date("Y-m-d H:i:s", strtotime($data['from_date']));
        $this->dateTo = date("Y-m-d H:i:s", strtotime($data['to_date']));

        $this->model = $CORE->get('OrderContainer')->builder;
        $this->paymentModel = $CORE->get('Payments')->builder;
        $this->specialDate = $CORE->get('SpecialDatesContainer')->builder;
        $this->isSuperAdmin = $CORE->get('Security')->isHavePermissions('SuperAdministrator');

        $this->maxOrderId = $this->model->GetOrdersMaxId();

        $this->fieldsNames = $this->setFieldsNames();
        $this->imgPath = 'http://'.$_SERVER['SERVER_NAME'].'/data/img/products/big/';

    }

    public function getFieldsValues()
    {
        $data = $this->getData();

        if($data === null){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;

    }


    private function setFieldsNames(){
        $fieldsNames = array();
        $fieldsNames[] = 'ORDER ID AND SKU AND COLOR AND SIZE';
        $fieldsNames[] = 'PRODUCT ORDER ID';
        $fieldsNames[] = 'ORDER ID';
        $fieldsNames[] = 'COMMENT';
        $fieldsNames[] = 'USER ID';
        $fieldsNames[] = 'NAME SURNAME';
        $fieldsNames[] = 'EMAIL';
        $fieldsNames[] = 'REFERRER';
        $fieldsNames[] = 'DATA REGISTER';
        $fieldsNames[] = 'PAYMENT METHOD';
        $fieldsNames[] = 'PRICE * QUANTITY';

        if ($this->isSuperAdmin) {
            $fieldsNames[] = 'PURCHASE PRICE';
        }

        $fieldsNames[] ='TOTAL PRICE';
        $fieldsNames[] ='DISCOUNT';
        $fieldsNames[] = 'BANK_DISCOUNT';
        $fieldsNames[] = 'DELIVERY PRICE';
        $fieldsNames[] = 'QUANTITY';
        $fieldsNames[] = 'DELIVERY TYPE';
        $fieldsNames[] = 'DELIVERY DATES';
        $fieldsNames[] = 'BONUSES USED';
        $fieldsNames[] = 'ORDER STATUS';
        $fieldsNames[] = 'PRODUCT STATUS NAME';
        $fieldsNames[] = 'ORDER DATE';
        $fieldsNames[] = 'PRODUCT ID';
        $fieldsNames[] = 'SKU';

        if ((int) COUNTRY_ID === 1) {
            $fieldsNames[] = 'SHIPNOW SKU';
        }

        $fieldsNames[] = 'Company action SKU';
        $fieldsNames[] = 'PRODUCT NAME';
        $fieldsNames[] = 'PRODUCT COLOR';
        $fieldsNames[] = 'PRODUCT SIZE';
        $fieldsNames[] = 'PRODUCT CATEGORIES IDs';
        $fieldsNames[] = 'PRODUCT CATEGORIES NAMES';
        $fieldsNames[] = 'ADDRESS';
        $fieldsNames[] = 'STATE';
        $fieldsNames[] = 'CITY';

        if ((int) COUNTRY_ID === 1) {
            $fieldsNames[] = 'SURCURSAL';
        }

        $fieldsNames[] = 'ZIP';
        $fieldsNames[] = 'PHONE';
        $fieldsNames[] = 'IMAGE LINK';
        $fieldsNames[] = 'Order data id';
        $fieldsNames[] = 'DOCUMENT EXPORT DATE';
        $fieldsNames[] = 'Stock type';
        $fieldsNames[] = 'Campaign end date';
        $fieldsNames[] = 'Brand name';
        $fieldsNames[] = 'PAYMENT DATE';
        $fieldsNames[] = 'PS METHOD';
        $fieldsNames[] = 'PRODUCT SUPPLIER';

        return $fieldsNames;
    }

    private function prepareFieldsValues($orders)
    {

        $nowTime = new \DateTime();

        $fieldsValues = array();


        foreach ($orders as $u) {

            $row = Array();
            $deliveryDateRange = ' - ';

            if ($u['delivery_date'] != '0000-00-00 00:00:00') {

                $deliveryDateRange = date('d/m/Y', strtotime($u['delivery_date'])) . ' - ';

                if ($u['delivery_end_date']) {
                    $deliveryDateRange .= date('d/m/Y', strtotime($u['delivery_end_date']));
                }
            }

            $isFree = $this->specialDate->isOrderShippingDate($u['order_date']);
            $surcursal = '';
            switch ($u['curier_type']) {
                case 2 :
                    $deliveryType = 'Retiro en Sucursal OCA';
                    $surcursal = $u['surcursal_code'];
                    break;

                case 3 :
                    $deliveryType = 'Retiro en oficina';
                    break;

                default :
                    if (!empty($u['deliveryTypeEtrans'])) {
                        $deliveryType = 'Envio a Domicilio Etrans';
                    } else {
                        $deliveryType = 'Envio a Domicilio OCA';
                    }


            }


            if ($u['name'] != '' OR $u['surname'] != '') {
                $nameSurname = $u['name'] . ' ' . $u['surname'];
            } elseif ($u['p_name'] != '') {
                $nameSurname = $u['p_name'] . ' ' . $u['p_surname'];
            } else {
                $nameSurname = 'New user, didn\'t fill';
            }

            $address = $u['address'];
            $state = $u['state'];
            $city = $u['city'];
            $zip = $u['zip'];
            $phone = $u['phone'];

            if (!($u['shipping_rut'] === null)) {
                $delivery_price = 'LaTercera member - free shipping';
            } else {
                if ($isFree) {
                    $delivery_price = 'Free shipping day';
                } else {
                    $delivery_price = $u['delivery_price'];
                }
            }

            $priceQuantity = (int)($u['discount'] * (int)$u['quantity']);

            if ((int)$u['return_status_id'] === 2) {
                $u['status'] = 'Devoluvcion del producto';
            } elseif ((int)$u['return_status_id'] === 3) {
                $u['status'] = 'Devoluvcion del producto';
            } elseif ((int)$u['return_status_id'] === 4) {
                $u['status'] = 'Devolucion del import';
            }

            $row[] = $u['order_id'] . '_' . $u['scu'] . '_' . $u['product_color'] . '_' . $u['product_size'];
            $row[] = $u['order_id'] . '_' . $u['orderDataId'];
            $row[] = $u['order_id'];
            $row[] = $u['comment'];
            $row[] = $u['user_id'];
            $row[] = $nameSurname;
            $row[] = self::hiddenEmail($u['email']);

            $referer  = '-';
            if($u['referer'] != ''){
                $referer = $u['referer'];
            }elseif($u['p_referer'] != ''){
                $referer = $u['p_referer'];
            }

            $row[] = $referer;
            $row[] = date('d.m.Y H:i:s', strtotime($u['p_dataRegister']));

            $psType = $u['payment_method'];
            $psDate = $u['payment_date'];
            $psMethod = "no info";
            $paymentInf = $this->paymentModel->getPaymentData($u['order_id']);
            $request = @json_decode($paymentInf->request);

            if (is_object($paymentInf)) {
                if ((int)$paymentInf->payment_id === 1) {
                    //Mercago
                    $psMethod = @$request->collection->payment_type;
                    $psDate = @$request->collection->money_release_date;
                } else if ((int)$paymentInf->payment_id == 16) {
                    //Khipu - not have any data about payment method
                } else if ((int)$paymentInf->payment_id == 20) {
                    // TodoPago - we have data, but do not have infoabout payment
                    if (isset($request->requestPayment)) {
                        $psDate = @$request->requestPayment->Payload->Answer->DATETIME;
                    }
                }
            }

            $row[] = $psType;
            $row[] = $priceQuantity;

            if ($this->isSuperAdmin) {
                $row[] = $u['purchase_price'];
            }

            $row[] = $u['overallPrice'];
            $row[] = $u['discount'];
            $row[] = $u['banco_discount'];
            $row[] = $delivery_price;
            $row[] = $u['quantity'];
            $row[] = $deliveryType;
            $row[] = $deliveryDateRange;
            $row[] = $u['rewards'];
            $row[] = $u['status'];
            $row[] = $u['productStatusName'];
            $row[] = $u['order_date'];
            $row[] = $u['product_id'];
            $row[] = $u['scu'];

            if ((int) COUNTRY_ID === 1) {
                $row[] = $u['shipNowScu'];
            }

            $row[] = $u['companyActionSku'];
            $row[] = $u['product_name'];
            $row[] = self::decorateProductColor($u['product_color']);
            $row[] = self::decorateProductSize($u['product_size']);
            $row[] = $u['productCategoriesIds'];
            $row[] = $u['productCategoriesNames'];
            $row[] = $address;
            $row[] = $state;
            $row[] = $city;

            if ((int) COUNTRY_ID == 1) {
                $row[] = $surcursal;
            }

            $row[] = $zip;
            $row[] = $phone;
            $row[] = $this->imgPath . $u['productImage'];
            $row[] = $u['orderDataId'];
            $row[] = $nowTime->format('d/m/Y H:i:s');
            $row[] = $u['stockTypeName'];
            $row[] = (($u['campaignEndDate']) ? date('d/m/Y H:i:s', strtotime($u['campaignEndDate'])) : '');
            $row[] = $u['campaignName'];
            $row[] = $psDate;
            $row[] = $psMethod;
            $row[] = $u['product_supplier'];

            $fieldsValues[] = $row;

        }

     
        return $fieldsValues;
    }

    private function getData(){

        $orders = null;

        while(true){

            if ($this->lastProcessed)
            {
                return null;
            }

            if ($this->toId + $this->step > $this->maxOrderId)
            {
                $this->lastProcessed = true;
            }

            $this->toId = $this->fromId + $this->step;

            $orders = $this->model->getNextOrders(
                $this->fromId,
                $this->toId,
                $this->statuses,
                $this->dateFrom,
                $this->dateTo,
                COUNTRY_ID,
                true
            );

            $this->fromId += $this->step;
            $this->fromId++;


            if(count($orders) > 0){
                break;
            }
        }
        return $orders;

    }

}
