<?php
namespace Helper\Export;

class DeliveryParcelListCorreos extends ExportType
{
    private $model;
    private $lastProcessed;
    private $listId;

    protected $firstRow = '';
    protected $exportName = 'courier_info';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;

        $this->model = $CORE->get('DeliveryInfoContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();
        $this->listId = $data['listId'];

    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getParselsOfLists(array(
            'listId' => $this->listId,
        ));

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
         $fieldsNames = array();

         $fieldsNames[] = 'NAME SURNAME';
         $fieldsNames[] = 'ADDRESS';
         $fieldsNames[] = 'COMUNA';
         $fieldsNames[] = 'CITY';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'INTERNAL PARCEL ID';
         $fieldsNames[] = 'PRODUCT PRICE';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Space';
         $fieldsNames[] = 'Space';
         $fieldsNames[] = 'PHONE';

        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $sendInfo = json_decode($dataRow->send_info_json, true);

            $row[] = $sendInfo['name'] . ' ' . $sendInfo['surname'];
            $row[] =  $sendInfo['address'];
            $row[] =  $sendInfo['city'];
            $row[] = 'CHILE';
            $row[] = '24';
            $row[] = 'P';
            $row[] = '1';
            $row[] = $dataRow->internal_parcel_id ;
            $row[] = 'Productos Temporada';
            $row[] = $dataRow->price_all_products;
            $row[] = '1';
            $row[] = '1';
            $row[] = ' ';
            $row[] = ' ';
            $row[] = $sendInfo['phone'];

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}