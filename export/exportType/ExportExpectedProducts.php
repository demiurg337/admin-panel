<?php

namespace Helper\Export;

class ExportExpectedProducts extends ExportType
{
    private $data;

    protected $firstRow = '';
    protected $exportName = 'expected_products';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    private $generator;
    private $from;
    private $to;

    public function __construct($data){
        $this->generator = $data['generator'];
        $this->from = $data['from'];
        $this->to = $data['to'];

        $captions = ['Articulo', 'Color', 'Talle'];
        $data = $this->generator->getInfoForProductsWhichWillNeed($this->from, $this->to);
        $el = reset($data);

        if (isset($el['stock'])) {
            foreach ($el['stock'] as $key => $val) {
                $captions[] = 'Repo ' . $key;
                $captions[] = 'Stock ' . $key;
                $captions[] = 'Pedido ' . $key;
            }
        }

        $this->fieldsNames = $captions;

    }

    private $isFirst = false;
    public function getFieldsValues()
    {
        if (!$this->isFirst) {
            $this->isFirst = true;
            return $this->prepareData($this->generator->getInfoForProductsWhichWillNeed($this->from, $this->to));
        }

        return null;
    }


    private function prepareData($info)
    {
        $prepareInfo = array();
        foreach ($info as $key => $row) {
            $row = (object) $row;
            $rowFile = array();
            $rowFile[] = $row->code; 
            $rowFile[] = $row->color;
            $rowFile[] = $row->size;

            foreach ($row->stock as $key => $val) {
                $rowFile[] = $row->need[$key];
                $rowFile[] = $val;
                $rowFile[] = $row->ask[$key];
            }

            $prepareInfo[] = $rowFile;
        }

        return $prepareInfo;
    }
}
