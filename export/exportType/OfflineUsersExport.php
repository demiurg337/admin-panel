<?php

namespace Helper\Export;

class OfflineUsersExport extends ExportType
{
    private $dateFrom;
    private $dateTo;

    protected $firstRow = '';
    protected $exportName = 'office_products';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        if (!isset($data['from']) || !isset($data['to']) ) {
            throw new \Exception('Wrong parameters');
        }

        $this->dateFrom = $data['from'];
        $this->dateTo = $data['to'];
        $this->fieldsNames = $this->setFieldsNames();
    }

    private $isFirst = false;
    public function getFieldsValues()
    {
        global $CORE;
        $userBuilder = $CORE->get('UsersContainer')->builder;
        $from = new \DateTime($this->dateFrom);
        $to = new \DateTime($this->dateTo);
        $users = $userBuilder->getUsersFromOfflineStore($from, $to);


        if (!$this->isFirst) {
            $this->isFirst = true;
            return $this->prepareData($users);
        }

        return null;
    }


    private function prepareData($info)
    {
        $prepareInfo = array();
        foreach ($info as $row) {
           $rowFile = array();
           $rowFile[] = $row->code;
           $rowFile[] = $row->id;
           $rowFile[] = $row->name;
           $rowFile[] = $row->surname;
           $rowFile[] = $row->phone;
           $rowFile[] = $row->date;

           $prepareInfo[] = $rowFile;
        }

        return $prepareInfo;
    }

    private function setFieldsNames()
    {
        $fieldsNames = array();

        $fieldsNames[] = 'code';
        $fieldsNames[] = 'userId';
        $fieldsNames[] = 'name';
        $fieldsNames[] = 'surname';
        $fieldsNames[] = 'phone';
        $fieldsNames[] = 'date of registration';

        return $fieldsNames;
    }
}
