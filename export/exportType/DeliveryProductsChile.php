<?php
namespace Helper\Export;

class DeliveryProductsChile extends ExportType
{
    private $model;
    private $lastProcessed;
    private $from;
    private $to;

    protected $exportName = 'deliveryProductsChile';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;

        $this->from = date('Y-m-d H:i:s',strtotime($data['from']));
        $this->to = date('Y-m-d H:i:s',strtotime($data['to']));

        $this->model = $CORE->get('DeliveryInfoContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();

    }

    private function setFieldsNames(){
        $fieldsNames = array();

        $fieldsNames[] = 'NOMBRE_CONS';
        $fieldsNames[] = 'APELLIDO1_CONS';
        $fieldsNames[] = 'APELLIDO2_CONS';
        $fieldsNames[] = 'CONTACTO_CONS';
        $fieldsNames[] = 'DIRECCION_CONS';
        $fieldsNames[] = 'NUMERO_VIAL_CONS';
        $fieldsNames[] = 'POBLACIO_CON';
        $fieldsNames[] = 'TELEFONO_CONS';
        $fieldsNames[] = 'NIF_CONS';
        $fieldsNames[] = 'TIPO_ENTREGA';
        $fieldsNames[] = 'TIPO_PAGO';
        $fieldsNames[] = 'TIPO_SERVICIO';
        $fieldsNames[] = 'CUENTA_CORRIENTE';
        $fieldsNames[] = 'DCONTROL_CC';
        $fieldsNames[] = 'CCOSTO_CC';
        $fieldsNames[] = 'CONTENIDO';
        $fieldsNames[] = 'VALOR_DECLARADO';
        $fieldsNames[] = 'REFERENCIA';
        $fieldsNames[] = 'REFERENCIA_ART';
        $fieldsNames[] = 'CANTIDAD';
        $fieldsNames[] = 'LOTE';
        $fieldsNames[] = 'OBSERVACIONES';
        $fieldsNames[] = 'AGENCIA';
        $fieldsNames[] = 'VALORES_DOCUMENTOS';
        $fieldsNames[] = 'CLAVE_MOVIMIENTO';

        return $fieldsNames;

    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getWillSendOrdersForPeriod($this->from, $this->to);

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {

            $row = Array();

            $sendInfo = json_decode($dataRow->send_info_json, true);

            $size = self::decorateProductSize($dataRow->size);
            $color = self::decorateProductColor($dataRow->color);
            $delimiter = ($size == '' || $color == '') ? '': '-';


            $row[] = self::decorateFieldValue(self::cleanUserDatafromSpeciaSymbols($sendInfo['name']));     // A
            $row[] = self::decorateFieldValue(self::cleanUserDatafromSpeciaSymbols($sendInfo['surname']));  // B
            $row[] = $dataRow->orderDataId;                   // C
            $row[] = self::decorateFieldValue(self::cleanUserDatafromSpeciaSymbols($sendInfo['name']));     // D
            $row[] = self::decorateFieldValue(self::cleanUserDatafromSpeciaSymbols($sendInfo['address']));  // E
            $row[] = '0';                   // F
            $row[] = self::decorateFieldValue(self::cleanUserDatafromSpeciaSymbols($sendInfo['location'])); // G
            $row[] = $sendInfo['phone'];    // H
            $row[] = '76154342-3';     // I
            $row[] = '2';                   // J  2 -  option is “home delivery”
            $row[] = '2';                   // K  2 - because that option is “current account”
            $row[] = '0';                   // L  0 - because that option is “normal services”
            $row[] = '19468';                   // M  ? - here will be Club Point account id for courier service
            $row[] = '9';                   // N  ? - It is also related to account, for example for 53565-3 id we set in N 3
            $row[] = '0';                   // O  ? - (It is also related to CP account ID for courier service
            $row[] = self::decorateFieldValue(self::cleanUserDatafromSpeciaSymbols($dataRow->categories));      // P  categores of product
            $row[] = $dataRow->price;           // Q  price of product
            $row[] = $dataRow->order_id . '_' .$dataRow->orderDataId;     // R  orderId_orderDataId
            $row[] = $dataRow->product_id; // S product_id
            $row[] = $dataRow->quantity;        // T  quantity
            $row[] = $color . $delimiter .$size;                    // U
            $row[] = '2';                   // V  (clarify with client + starken)
            $row[] = '';                    // W   leave empty
            $row[] = '42';                  // X   always 42
            $row[] = '';                  // Y   always 42

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

    private static function decorateFieldValue($str){

       return mb_strtoupper($str);
    }

    private static function cleanUserDatafromSpeciaSymbols($str){

        $srcArray = array(
            '¡','¿','á','í','é','ó','ú','ñ','ü','Á','É','Ó','Ú','Ñ','Í','Ü'
        );

        $replaceArray = array(
            '!','?','a','i','e','o','u','n','u','A','E','O','U','N','I','U'
        );

        return str_replace($srcArray, $replaceArray, $str);
    }
}
