<?php
namespace Helper\Export;

class CourierPlazasArgentina extends ExportType
{
    private $model;
    private $dateFrom;
    private $dateTo;
    private $lastProcessed;

    protected $firstRow = 'sep=';
    protected $exportName = 'courier_felipe_plazas';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;


        $this->dateFrom = date("Y-m-d H:i:s", strtotime($data['courier-from_date']));
        $this->dateTo = date("Y-m-d H:i:s", strtotime($data['courier-to_date']));

        $this->model = $CORE->get('OrderContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();

    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getCouriersExcelExportInfo($this->dateFrom, $this->dateTo);

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
         $fieldsNames = array();

         $fieldsNames[] = 'NAME SURNAME';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'SURCURSAL';
         $fieldsNames[] = 'PHONE';
         $fieldsNames[] = 'EMAIL';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'ORDER ID';

        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $row[] = $dataRow->name . ' ' . $dataRow->surname;
            $row[] = '0';
            $row[] = $dataRow->surcursal_code;
            $row[] = $dataRow->phone;
            $row[] =  self::hiddenEmail($dataRow->email);
            $row[] =  '1';
            $row[] = '0.499';
            $row[] = '5';
            $row[] = '4';
            $row[] = '1';
            $row[] = '0';
            $row[] = $dataRow->id;

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}