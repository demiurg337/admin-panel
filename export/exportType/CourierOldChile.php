<?php
namespace Helper\Export;

class CourierOldChile extends ExportType
{
    private $model;
    private $dateFrom;
    private $dateTo;
    private $lastProcessed;

    protected $firstRow = 'sep=';
    protected $exportName = 'courier_info';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;


        $this->dateFrom = date("Y-m-d H:i:s", strtotime($data['courier-from_date']));
        $this->dateTo = date("Y-m-d H:i:s", strtotime($data['courier-to_date']));

        $this->model = $CORE->get('OrderContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();

    }


    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getCouriersExcelExportInfo($this->dateFrom, $this->dateTo);

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }



    private function setFieldsNames(){
         $fieldsNames = array();

         $fieldsNames[] = 'NAME SURNAME';
         $fieldsNames[] = 'ADDRESS';
         $fieldsNames[] = 'CITY';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'CLUBPOINT & Order ID';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'PRODUCT PRICE';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Fixed';
         $fieldsNames[] = 'Space';
         $fieldsNames[] = 'Space';
         $fieldsNames[] = 'PHONE';

        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $row[] = $dataRow->name . ' ' . $dataRow->surname;
            $row[] =  $dataRow->address;
            $row[] = 'CHILE';
            $row[] = '24';
            $row[] = 'P';
            $row[] = '1';
            $row[] = 'CLUBPOINT' . $dataRow->id ;
            $row[] = 'Productos Temporada';
            $row[] = 'Capital Federal';
            $row[] = $dataRow->sum;
            $row[] = '1';
            $row[] = '1';
            $row[] = ' ';
            $row[] = ' ';
            $row[] = $dataRow->phone;

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}