<?php

namespace Helper\Export;

class ExportForReports extends ExportType
{
    private $sellers;
    private $stores;
    private $timeRange;

    protected $firstRow = '';
    protected $exportName = 'report';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($sellers, $stores, $timeRange){
        $this->sellers = $sellers;
        $this->stores = $stores;
        $this->timeRange = $timeRange;
        $this->fieldsNames = $this->setFieldsNames();
    }

    private $isFirst = false;
    public function getFieldsValues()
    {
        global $CORE;
        if (!$this->isFirst) {
            $this->isFirst = true;
            return $this->prepareData($this->sellers, $this->stores);
        }

        return null;
    }


    private function prepareData($sellers, $stores)
    {
        $prepareInfo = array();
        foreach ($sellers as $key => $row) {
           
           $row = (object) $row;

           $rowFile = array();
           $rowFile[] = $row->seller;
           $rowFile[] = $row->quantity;
           $rowFile[] = round($row->price, 2);
           $rowFile[] = $row->quantityOfOrders;
           $rowFile[] = $row->price_div_quantityOfOrders;
           $rowFile[] = $row->quantity_div_quantityOfOrders;

           $prepareInfo[] = $rowFile;
        }
        
        $prepareInfo[] = array();
        $prepareInfo[] = array();
        $prepareInfo[] = array();
        $rowFile = $this->setFieldsNames();
        $rowFile[0] = 'Store ID';
        $prepareInfo[] = $rowFile;

        foreach ($stores as $key => $row) {
           
           $row = (object) $row;

           $rowFile = array();
           $rowFile[] = $row->storeCode;
           $rowFile[] = '';
           $rowFile[] = '';
           $rowFile[] = '';
           $rowFile[] = $row->price_div_quantityOfOrders;
           $rowFile[] = $row->quantity_div_quantityOfOrders;

           $prepareInfo[] = $rowFile;
        }

        $timeStr = $this->timeRange['from']->format('m.d H:i') . ' al ' . $this->timeRange['to']->format('m.d H:i');
        $prepareInfo[] = array();
        $prepareInfo[] = array('informe hecho del ' . $timeStr);
        return $prepareInfo;
    }

    private function setFieldsNames()
    {
        $fieldsNames = array();

        $fieldsNames[] = 'Vendedor';
        $fieldsNames[] = 'Unidades';
        $fieldsNames[] = 'Monto';
        $fieldsNames[] = 'Comprobantes';
        $fieldsNames[] = 'Monto/Comprobantes';
        $fieldsNames[] = 'Unidades/Comprobantes';

        return $fieldsNames;
    }
}
