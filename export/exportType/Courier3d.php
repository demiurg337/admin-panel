<?php
namespace Helper\Export;

class Courier3d extends ExportType
{
    private $model;
    private $dateFrom;
    private $dateTo;
    private $lastProcessed;

    protected $firstRow = 'sep=';
    protected $exportName = '3d_courier_export';
    protected $fieldsNames = array();

    public $exportFormat = 'csv';

    public function __construct($data){

        global $CORE;


        $this->dateFrom = date("Y-m-d H:i:s", strtotime($data['courier-from_date']));
        $this->dateTo = date("Y-m-d H:i:s", strtotime($data['courier-to_date']));

        $this->model = $CORE->get('OrderContainer')->builder;
        $this->fieldsNames = $this->setFieldsNames();

    }

    public function getFieldsValues()
    {

        if($this->lastProcessed){
            return null;
        }
        $this->lastProcessed = true;

        $data = $this->model->getCouriersExcelExportInfo($this->dateFrom, $this->dateTo);

        if(!$data){
            return null;
        }

        $fieldsValues = $this->prepareFieldsValues($data);

        unset($data);

        return $fieldsValues;
    }


    private function setFieldsNames(){
        $fieldsNames = array();

         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'ADDRESS';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'PHONE';
         $fieldsNames[] = 'ZIP';
         $fieldsNames[] = 'CITY';
         $fieldsNames[] = 'STATE';
         $fieldsNames[] = 'NAME SURNAME';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'ORDER ID';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'EMAIL';
         $fieldsNames[] = 'FIXED';
         $fieldsNames[] = 'FIXED';

        return $fieldsNames;
    }

    private function prepareFieldsValues($data)
    {

        $fieldsValues = array();

        foreach ($data as $dataRow) {
            $row = Array();

            $row[] = 'Logistica inversa';
            $row[] = '0';
            $row[] = 'BILLINGHURST';
            $row[] = '2476';
            $row[] = 'PB';
            $row[] = 'A';
            $row[] = '1425';
            $row[] = 'Capital Federal';
            $row[] = 'Capital Federal';
            $row[] = '48072094';
            $row[] = 'logitsticainversa@clubpoint.com';
            $row[] = $dataRow->address;
            $row[] = '0';
            $row[] = '0';
            $row[] = '0';
            $row[] = $dataRow->phone;
            $row[] = $dataRow->zip;
            $row[] = $dataRow->city;
            $row[] = $dataRow->state;
            $row[] = $dataRow->name . ' ' . $dataRow->surname;
            $row[] = '0.499';
            $row[] = '5';
            $row[] = '4';
            $row[] = '1';
            $row[] = '0';
            $row[] = $dataRow->id;
            $row[] = '76850';
            $row[] = self::hiddenEmail($dataRow->email);
            $row[] = '1';
            $row[] = '1';

            $fieldsValues[] = $row;
        }

        return $fieldsValues;
    }

}