<?php
namespace Helper\Export;

class ExportCsv implements ExportFormat
{
    private $delimiter;
    private $country;


    private $delimitersTypes  = array(
        1=>',',
        2=>';',
    );

    public function __construct($delimiterId){
        $this->country = COUNTRY;


        if(isset($this->delimitersTypes[$delimiterId])){

            $this->delimiter = $this->delimitersTypes[$delimiterId];

        }else{

            $this->delimiter = $this->delimitersTypes[1];

        }


    }
    public function getExportFile(ExportType $data){

        $name = $data->getExportName();

        $fileName = $name.'_'.$this->country.'_'.date('d_m_Y_H_i_s', time()).'.csv';

        header('Content-Type: application/vnd.ms-excel');
        header('Connection: Keep-Alive');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');

        $csvHandler = fopen('php://output', 'w');
        $str = '';

        $firstRow = $data->getFirstRow();

        if($firstRow != ''){
            $str .= $firstRow . $this->delimiter . "\n";
        }

        $fieldsNames = $data->getFieldsNames();
        if(count($fieldsNames) > 0){
            $str .= implode( $this->delimiter, $fieldsNames ) . "\n";

            fwrite( $csvHandler, $str);
        }



        $prepareFields = function($value){
            return '"'. $this->escapedString($value) . '"';
        };

        while(($fieldsValues = $data->getFieldsValues()) !== null){

            $str = '';

            foreach ($fieldsValues as $row){

                $mapRow = array_map( $prepareFields, $row);
                $str .= implode($this->delimiter, $mapRow) . "\n";

            }

            if($str != ''){
                fwrite($csvHandler, $str);
            }
        }

        fclose($csvHandler);
    }

    public static function escapedString($str){
        $search = Array(
            '"',',',';','\n'
        );
        $replace = Array(
            "'",' ',' ',' '
        );

        return str_replace($search , $replace, $str);
    }
}
