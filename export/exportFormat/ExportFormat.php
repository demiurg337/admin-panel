<?php


namespace Helper\Export;


interface ExportFormat
{
    public function getExportFile(ExportType $data);
}