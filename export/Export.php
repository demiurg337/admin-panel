<?php

namespace Helper\Export;


class Export
{
    private $data;
    private $format;

    public function __construct($data, $typeExport, $delimiterId)
    {
        if (!defined('COUNTRY')) {
            define('COUNTRY', 'Argentina');
        }

        switch ($typeExport) {
            case 'ExportForReports':
                $this->data = new ExportForReports(
                    $data['sellers'], 
                    $data['stores'], 
                    $data['timeRange']
                );
                break;
            case 'order':
                $this->data = new OrderExport($data);
                break;
            case 'marketing':
                $this->data = new MarketingExport($data);
                break;
            case 'courier3d':
                $this->data = new Courier3d($data);
                break;
            case 'expectedProducts':
                $this->data = new ExportExpectedProducts($data);
                break;
            case 'convertedFileFromOfflineStores':
                $this->data = new ExportOfConvertedFileStores($data);
                break;
            case 'courierOldChile':
                $this->data = new CourierOldChile($data);
                break;
            case 'courierNewChile':
                $this->data = new CourierNewChile($data);
                break;
            case 'offlineStoreUsers':
                $this->data = new OfflineUsersExport($data);
                break;
            case 'courierArgentina':
                $this->data = new CourierArgentina($data);
                break;
            case 'courierPlazasArgentina':
                $this->data = new CourierPlazasArgentina($data);
                break;
            case 'courierSucursal':
                $this->data = new CourierSucursal($data);
                break;
            case 'credits':
                $this->data = new Credits($data);
                break;
            case 'products':
                $this->data = new Products($data);
                break;
            case 'deliveryProductsChile':
                $this->data = new deliveryProductsChile($data);
                break;
            case 'deliveryCourierExport':
                if((int) COUNTRY_ID === 1 ){
                    if(isset($data['deliveryTypeId']) && (int) $data['deliveryTypeId'] > 0 ){
                        $deliveryTypeId = (int) $data['deliveryTypeId'];

                        if($deliveryTypeId === 1 || $deliveryTypeId === 4 ){
                            $this->data = new DeliveryParcelListHome($data);
                        }else if($deliveryTypeId === 2){
                            $this->data = new DeliveryParcelListOca($data);
                        }
                    }

                }else if( (int) COUNTRY_ID === 2){
                    $this->data = new DeliveryParcelListCorreos($data);
                }
                break;
            case 'colorsWithCategories':
                $this->data = new colorsWithCategories($data);
                break;
            case 'productParcelsInfo':
            $this->data = new productParcelsInfo($data);
                break;

            case 'shipNowSyncStock':
                if((int) COUNTRY_ID === 1 ) {
                    $this->data = new shipNowSyncStock($data);
                }
                break;
            default:
                die('bad params');
        }


        switch ($this->data->getExportFormat()) {
            case 'csv':

                $this->format = new ExportCsv($delimiterId);
                break;

            default:
                die('bad params');
        }
    }

    public function getExport()
    {
        $this->format->getExportFile($this->data);
    }

}
